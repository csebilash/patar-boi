var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , crypto = require('crypto')
  ,uuid = require('node-uuid')
  
  , authTypes = [ 'facebook']
  
  
  var count=0;
  var UserSchema = mongoose.Schema({
            
            salt: {type: String, required: true, default: uuid.v1},
            name: String,
            email: String,
            activation:String,
            type: String,
            password: String,
            articleNumber: {type: Number,default: 0},
            amaderkotha: String,
            submitarticle: String,
            donation: String,
            guideAuthor: String,
            donors:[{
              name:String,
              donor_ID:String,
              dateDonation:{type:Date,default:Date.now},
              amount:{type:Number, default:0},
              bookDistributionID:String,
            }],
            creditForCompany:String,
            resetPassId:String,
            textRequestSubmitArtWork:{type:String,default:'<p>Dear sir,</p><p>Please submit art work for the article </p>'},
            textReminderArtWork:{type:String,default:'<p>Dear sir,</p><p>Please submit art work for the article </p>'},
            textRemindParticipate:{type:String,default:'<p>Dear sir,</p><p>Please parcipate for a  bid for the article </p>'},
            textRemindReviewSubmit:{type:String,default:'<p>Dear sir,</p><p>Please review for the article'},
            textemailRejectedArticle:{type:String,default:'<p>Dear sir,</p><p>Your article has been rejected.'},
            textAckRejectAuthour:{type:String,default:'<p>Dear sir,</p><p>Your article has been rejected.'},
            textThankSubmittingArt:{type:String,default:'<p>Dear sir,</p><p>Thank you for submitting art work for the article.<br></p>'},
            textThankParticipateBid:{type:String,default:'<p>Dear sir,</p><p>Thank you for Participate in bid<br></p>'},
            textackAuthour:{type:String,default:'<p>Dear sir,</p><p>Your article has been accepted<br></p>'},
            textThank_review_article:{type:String,default:'<p>Dear sir,</p><p>Thank you for reviewing the article</p>'},
            textThank_submit_article:{type:String,default:'<p>Dear sir,</p><p>Thank you for submitting the article</p>'},
            textourAuthor:{type:String,default:'<p>Our Author,</p>'},
            textOurreviewer:{type:String,default:'Our Reviewer'},
            textOurArtist:{type:String,default:'Our Artist'},
            textOurDonor:{type:String,default:'Our donors'},
            textcongrtzMessageArtist:{type:String,default:'<p>Dear sir,</p><p>Congraduation!! Your article has been completed successfully <br></p><p>Regards</p>Patar boi'},
            textcongrtzMessageAuthor:{type:String,default:'<p>Dear sir,</p><p>Congraduation!! Your article has been completed successfully <br></p><p>Regards</p>Patar boi'},
            textInviteArtist:{type:String,default:'<p>Dear sir,</p><p>You are invited to join patarboi as a Artist.</p><p>Regards</p>Patar boi'},
            textInviteReviewer:{type:String,default:'<p>Dear sir,</p><p>You are invited to join patarboi as a Reviewer.</p><p>Thanks</p>Patar boi'},
            textInviteUser:{type:String,default:'<p>Dear sir,</p><p>You are invited to join patarboi as a user.</p><p>Thanks</p><p>Patar boi<br></p>'},
            bookprice: {type: Number,default: 0},
            shippingprice: {type: Number,default: 0},

            userType: {type: String,default: 0},
            reviewerType: {type: String,default: 0},
            profilePicture: {type: String, default: null},
            userSubtype: [{

              subtype: {type: String,default: 0},

            }],
            age: {type: String,default: 0},
            gender: {type: String,default: 0},
            country: {type: String,default: ''},
            preferredname:{type: String,default: ''},
            biography:{type: String,default: ''},
            facebook: {},
            facebookAccessToken: {},
            pendingArticleCount: {type: Number,default: 0},
            acceptedArticleCount: {type: Number,default: 0},
            rejectedArticleCount: {type: Number,default: 0},
            
            latestArticleCount: {type: Number,default: 0},
            latestArticle:[],
            editedArticle:[],
            latestArticleAuthor:[],
            biddingRequestedArticle:[],
            acceptedArtist:[{
              articleId:String,

            }],

            writeArticle: [{
                  productionRef:{type: String,default: null},
                  produceBy:{type: String,default: null},
                  collectedBy:{type: String,default: null},
                  productionDate:Date,
                  paidDate:Date,
                  productionVolume:{type: Number,default: 0},
                  productionCost:{type: Number,default: 0},
                  amountPaid:{type: Number,default: 0},
                  distributionRef:{type: String,default: null},
                  distributedTo:{type: String,default: null},
                  distributionBy:{type: String,default: null},
                  distributionDate:Date,
                  collectedDate:Date,
                  distributionVolume:{type: Number,default: 0},
                  distributionRevenue:{type: Number,default: 0},
                  amountCollected:{type: Number,default: 0},
                  no_page:{type: Number,default: 0},
                  completeDate:Date,
                  homePic:String,
                  default_image:String,
                  noOfreviewer: {type: Number,default: 0},
                  noOfArtist: {type: Number,default: 0},
                  noOfReviewer:{type: String,default: null},
                  sound:String,
                  rejectMessage:String,
                  bookTitle:String, //new
                  artistImagePage1:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage2:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage3:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage4:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage5:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage6:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage7:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage8:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage9:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage10:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage11:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage12:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage13:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage14:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage15:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],
                  artistImagePage16:[{
                    img:String,
                    artist:String,
                    name:String,
                    rating:{type: Number,default: 0},
                  }],

                  selectedartistImagePage1:String,
                  selectedartistImagePage2:String,
                  selectedartistImagePage3:String,
                  selectedartistImagePage4:String,
                  selectedartistImagePage5:String,
                  selectedartistImagePage6:String,
                  selectedartistImagePage7:String,
                  selectedartistImagePage8:String,
                  selectedartistImagePage9:String,
                  selectedartistImagePage10:String,
                  selectedartistImagePage11:String,
                  selectedartistImagePage12:String,
                  selectedartistImagePage13:String,
                  selectedartistImagePage14:String,
                  selectedartistImagePage15:String,
                  selectedartistImagePage16:String,

                  selectedartistPage1:String,
                  selectedartistPage2:String,
                  selectedartistPage3:String,
                  selectedartistPage4:String,
                  selectedartistPage5:String,
                  selectedartistPage6:String,
                  selectedartistPage7:String,
                  selectedartistPage8:String,
                  selectedartistPage9:String,
                  selectedartistPage10:String,
                  selectedartistPage11:String,
                  selectedartistPage12:String,
                  selectedartistPage13:String,
                  selectedartistPage14:String,
                  selectedartistPage15:String,
                  selectedartistPage16:String,
                  bookTitleSound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  articleId: String,
                  userId: String,
                  NickName: String,

                  title: String,
                  title1Sound:[{
                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article: String,
                  topSubText1Sound:[{
                      word:String,
                      sound:{type: String,default: null},
                  }],
                  credit: String,
                  bottomText: String,
                  topBottomText1Sound:[{
                      word:String,
                      sound:{type: String,default: null}, 
                    }],

                  cetagory: [],
                  label: String,
                  articleImage: {type: String, default: null},
                  status: String,
                  created: {type: Date, default: Date.now},
                  artistImage1: {type: String, default: null},
                  artistImage2: {type: String, default: null},
                  artistImage3: {type: String, default: null},
                  artistImage4: {type: String, default: null},
                  artistImage5: {type: String, default: null},
                  artistImage6: {type: String, default: null},
                  artistImage7: {type: String, default: null},
                  artistImage8: {type: String, default: null},
                  artistImage9: {type: String, default: null},
                  artistImage10: {type: String, default: null},
                  artistImage11: {type: String, default: null},
                  artistImage12: {type: String, default: null},
                  artistImage13: {type: String, default: null},
                  artistImage14: {type: String, default: null},
                  artistImage15: {type: String, default: null},
                  artistImage16: {type: String, default: null},
                  artistId: {type: String, default: null},
                  artistName: {type: String, default: null},
                  artistArticleId:String,
                  rating:{type: Number,default: 0},
                  ratedUser: {type: Number,default: null},
                  tags:[],
                  read: {type: Number,default: null},
                  
                  topFontcolor: String,
                  topTextalign: String,
                  topBackgroundcolor: String,
                  topTexttransparency: String,
                  pdfArticle:{type: String, default: null},
                  pptArticle:{type: String, default: null},
                  letterArticle:{type: String, default: null},
                  imagePosition: String,
                  articleImage_comment:[{
                    comment:String,
                    commenttator:String,
                    commentCreated: {type: Date, default: Date.now},
                  }],
                  article_reviewer:[{
                    articleReviewerId:String,
                     articleRevieweremail:String,
                     dueDate:String,
                     status:{type: String, default: 'Request waiting'},
                  }],
                  reviewSubmittedDate:Date,
                  articleReviewer_comment:[{
                    condition:String,
                    comment:String,
                    commenttator:String,
                    rating:{type: Number,default: 0},
                    comment1:String,
                    comment2:String,
                    comment3:String,
                    comment4:String,
                    comment5:String,
                    comment6:String,
                    comment7:String,
                    comment8:String,
                    comment9:String,
                    comment10:String,
                    comment11:String,
                    comment12:String,
                    comment13:String,
                    comment14:String,
                    comment15:String,
                    comment16:String,
                    commentCreated: {type: Date, default: Date.now},
                  }],
                  article_artist:[{
                   articleArtistemail:String,
                   status:{type: String, default: 'Request waiting'},
                  }],

                   bidding_Article:[{
                    artistId: String,
                    artistEmail:String,
                    bidding_status: String,  
                    no_of_image:{type: Number,default: 0},
                    cost:{type: Number,default: 0},
                    status:{type: String,default: ''},
                    days:{type: Date, default: Date.now},
                    paid:{type: Number,default: 0},
                    completeDate: Date,
                    pageSubmitted:{type:Number,default:0},
                    standardPage:{type:Number,default:0},
                  }],
                  bottomTextcolor: String,
                  bottomTextalign: String,
                  bottomBackgroundcolor: String,
                  bottomTransparency: String,
                  startDate:Date,
                  endDate:String,
                  //2nd page:
                  articleTitle2: String,
                  articleTitle2Sound:[{

                      word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article2: String,
                  article2Sound:[{
                      word:String,
                      sound:{type: String,default: null}, 

                  }],
                  img2: {type: String, default: null},
                  source2: String,
                  bottomText2: String,
                  
                  bottomText2Sound:[{
                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //3rd page:
                  articleTitle3: String,
                  articleTitle3Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  article3: String,
                  article3Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  img3: {type: String, default: null},
                  source3: String,
                  bottomText3: String,
                  bottomText3Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //4th page:----------------------------------------
                  articleTitle4: String,
                  articleTitle4Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article4: String,
                  article4Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img4: {type: String, default: null},
                  source4: String,
                  bottomText4: String,
                  bottomText4Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                 // ---------------Mustakim start------------

                  //5th page:
                  articleTitle5: String,
                  articleTitle5Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article5: String,
                  article5Sound:[{
                      word:String,
                      sound:{type: String,default: null},                  
                  }],
                  img5: {type: String, default: null},
                  source5: String,
                  bottomText5: String,
                  bottomText5Sound:[{
                      word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //6th page:
                  articleTitle6: String,
                  articleTitle6Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article6: String,
                  article6Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img6: {type: String, default:null},
                  source6: String,
                  bottomText6: String,
                  bottomText6Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //7th page:------------------------------------------------
                  articleTitle7: String,
                  articleTitle7Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article7: String,
                  article7Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img7: {type: String, default:null},
                  source7: String,
                  bottomText7: String,
                  bottomText7Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],



                  //8th page:---------------------------------------------------
                  articleTitle8: String,
                  articleTitle8Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article8: String,
                  article8Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img8: {type: String, default:null},
                  source8: String,
                  bottomText8: String,
                  bottomText8Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  editcontent:{type: String,default: ''},
                  uploadPdf:{type: String,default: ''},
                  uploadPpt:{type: String,default: ''},
                  uploadLetter:{type: String,default: ''},
                  artwork:{type: String,default: ''},
                  addsound:{type: String,default: ''},


                  //9th page:---------------------------------------------------
                  articleTitle9: String,
                  articleTitle9Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article9: String,
                  article9Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img9: {type: String, default:null},
                  source9: String,
                  bottomText9: String,
                  bottomText9Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //10th page:-----------------------------------------------------
                  articleTitle10: String,
                  articleTitle10Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article10: String,
                  article10Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img10: {type: String, default:null},
                  source10: String,
                  bottomText10: String,
                  bottomText10Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //11th page:------------------------------------------------------
                  articleTitle11: String,
                  articleTitle11Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article11: String,
                  article11Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img11: {type: String, default:null},
                  source11: String,
                  bottomText11: String,
                  bottomText11Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //12th page:-------------------------------------------------------
                  articleTitle12: String,
                  articleTitle12Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article12: String,
                  article12Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img12: {type: String, default:null},
                  source12: String,
                  bottomText12: String,
                  bottomText12Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //13th page:---------------------------------------------------------
                  articleTitle13: String,
                  articleTitle13Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article13: String,
                  article13Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img13: {type: String, default:null},
                  source13: String,
                  bottomText13: String,
                  bottomText13Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //14th page:--------------------------------------------------------------
                  articleTitle14: String,
                  articleTitle14Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article14: String,
                  article14Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img14: {type: String, default:null},
                  source14: String,
                  bottomText14: String,
                  bottomText14Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //15th page:---------------------------------------------------------------
                  articleTitle15: String,
                  articleTitle15Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article15: String,
                  article15Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img15: {type: String, default:null},
                  source15: String,
                  bottomText15: String,
                  bottomText15Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],

                  //16th page:----------------------------------------------------------------
                  articleTitle16: String,
                  articleTitle16Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article16: String,
                  article16Sound: [{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],
                  img16: {type: String, default:null},
                  source16: String,
                  bottomText16: String, 
                  bottomText16Sound:[{

                       word:String,
                      sound:{type: String,default: null}, 
                  }],


                   morepage: String,
                  //----------------Mustakim End---------              


          }],
           

            reviewer: [{
              dueDate:String,
              reviewer_status:String,
              reviewSubmittedDate:Date,
              sound:String,
              articleId:String,
              status: String,
              
               bookTitle:String, //new
                  bookTitleSound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],



                  title: String,
                  title1Sound:[{
                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article: String,
                  topSubText1Sound:[{
                      word:String,
                      sound:{type: String,default: null},
                  }],
                  credit: String,
                  bottomText: String,
                  topBottomText1Sound:[{
                      word:String,
                      sound:{type: String,default: null}, 
                    }],
              articleImage: {type: String, default: null},
              
              articleAthor:{type: String,default: null},
              reviewid: {type: String,default: null},
              writeArticleId:{type: String,default: null},

                     //2nd page:-----------------------------------
                  articleTitle2: String,
                  articleTitle2Sound:[{

                      word:String,
                      sound:{type: String,default: null}, 
                  }],
                  article2: String,
                  article2Sound:[{
                      word:String,
                      sound:{type: String,default: null}, 

                  }],
                  img2: {type: String, default: null},
                  source2: String,
                  bottomText2: String,
                  
                  bottomText2Sound:[{
                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //3rd page:------------------------------------
                  articleTitle3: String,
                  articleTitle3Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  article3: String,
                  article3Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  img3: {type: String, default: null},
                  source3: String,
                  bottomText3: String,
                  bottomText3Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //4th page:----------------------------------------
                  articleTitle4: String,
                  articleTitle4Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article4: String,
                  article4Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img4: {type: String, default: null},
                  source4: String,
                  bottomText4: String,
                  bottomText4Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //5th page:----------------------------------------
                  articleTitle5: String,
                  articleTitle5Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article5: String,
                  article5Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img5: {type: String, default: null},
                  source5: String,
                  bottomText5: String,
                  bottomText5Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //6th page:----------------------------------------
                  articleTitle6: String,
                  articleTitle6Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article6: String,
                  article6Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img6: {type: String, default: null},
                  source6: String,
                  bottomText6: String,
                  bottomText6Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //7th page:----------------------------------------
                  articleTitle7: String,
                  articleTitle7Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article7: String,
                  article7Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img7: {type: String, default: null},
                  source7: String,
                  bottomText7: String,
                  bottomText7Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //8th page:----------------------------------------
                  articleTitle8: String,
                  articleTitle8Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article8: String,
                  article8Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img8: {type: String, default: null},
                  source8: String,
                  bottomText8: String,
                  bottomText8Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //9th page:----------------------------------------
                  articleTitle9: String,
                  articleTitle9Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article9: String,
                  article9Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img9: {type: String, default: null},
                  source9: String,
                  bottomText9: String,
                  bottomText9Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //10th page:----------------------------------------
                  articleTitle10: String,
                  articleTitle10Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article10: String,
                  article10Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img10: {type: String, default: null},
                  source10: String,
                  bottomText10: String,
                  bottomText10Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //11th page:----------------------------------------
                  articleTitle11: String,
                  articleTitle11Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article11: String,
                  article11Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img11: {type: String, default: null},
                  source11: String,
                  bottomText11: String,
                  bottomText11Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  //12th page:----------------------------------------
                  articleTitle12: String,
                  articleTitle12Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article12: String,
                  article12Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img12: {type: String, default: null},
                  source12: String,
                  bottomText12: String,
                  bottomText12Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  //13th page:----------------------------------------
                  articleTitle13: String,
                  articleTitle13Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article13: String,
                  article13Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img13: {type: String, default: null},
                  source13: String,
                  bottomText13: String,
                  bottomText13Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //14th page:----------------------------------------
                  articleTitle14: String,
                  articleTitle14Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article14: String,
                  article14Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img14: {type: String, default: null},
                  source14: String,
                  bottomText14: String,
                  bottomText14Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //14th page:----------------------------------------
                  articleTitle14: String,
                  articleTitle14Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article14: String,
                  article14Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img14: {type: String, default: null},
                  source14: String,
                  bottomText14: String,
                  bottomText14Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //15th page:----------------------------------------
                  articleTitle15: String,
                  articleTitle15Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article15: String,
                  article15Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img15: {type: String, default: null},
                  source15: String,
                  bottomText15: String,
                  bottomText15Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

                  //16th page:----------------------------------------
                  articleTitle16: String,
                  articleTitle16Sound: [{

                      word:String,
                      sound:{type: String,default: null},
                  }],
                  article16: String,
                  article16Sound:[{
                      word:String,
                      sound:{type: String,default: null},

                  }],
                  img16: {type: String, default: null},
                  source16: String,
                  bottomText16: String,
                  bottomText16Sound:[{

                      word:String,
                      sound:{type: String,default: null},
                  }],

            }],
            
            reviewArticle: [{


                  bookTitle:String, //new
                  articleId: {type: String,default: 0},
                  writeArticleId:{type: String,default: 0},
                  userId: {type: String,default: 0},
                  title: {type: String,default: 0},
                  article: {type: String,default: 0},
                  credit: {type: String,default: 0},
                  bottomText: {type: String,default: 0},
                  articleImage: {type: String, default: null},
                  status: {type: String,default: 0},
                  reviewer_1: {type: String,default: 0},
                  reviewer_2: {type: String,default: 0},
                  reviewer_3: {type: String,default: 0},
 
                  //2nd page:
                  articleTitle2: String,
                  article2: String,
                  img2: {type: String, default: null},
                  source2: String,
                  bottomText2: String,

                  //3rd page:
                  articleTitle3: String,
                  article3: String,
                  img3: {type: String, default: null},
                  source3: String,
                  bottomText3: String,

                  //4th page:
                  articleTitle4: String,
                  article4: String,
                  img4: {type: String, default: null},
                  source4: String,
                  bottomText4: String,

              }],
              // for assign artist & artist
             articleArtist: [{

                  articleId: {type: String,default: 0},
                  writeArticleId:{type: String,default: 0},
                  title: {type: String,default: 0},
                  article: {type: String,default: 0},
                  credit: {type: String,default: 0},
                  bottomText: {type: String,default: 0},
                  status: {type: String,default: 0},
                  artist_1: {type: String,default: 0},
                  articleImage: {type: String, default: null},

              }],
              selectedArtistId:[{
                id:{type: String,default: null},
                startDate:Date,
                endDate:String,
              }],
              selected_ArtistId:[{
                id:{type: String,default: null},
                startDate:Date,
                endDate:String,
              }],
              selected_Artist:[],
              selectedArtist:[{

                  no_page:{type: Number,default: 0},
                  noOfreviewer: {type: Number,default: 0},
                  noOfArtist: {type: Number,default: 0},
                  noOfReviewer:{type: String,default: null},
                  sound:String,
                  bookTitle:String, //new
                  articleId: String,
                  userId: String,
                  title: String,
                  article: String,
                  credit: String,
                  bottomText: String,
                  cetagory: String,
                  label: String,
                  articleImage: {type: String, default: null},
                  status: String,
                  created: {type: Date, default: Date.now},
                  artistImage: {type: String, default: null},
                  artistImage2: {type: String, default: null},
                  artistImage3: {type: String, default: null},
                  artistImage4: {type: String, default: null},
                  artistId:String,
                  artistArticleId:String,
                  rating:{type: Number,default: 0},
                  ratedUser: {type: Number,default: null},
                  tags:[],
                  read: {type: Number,default: null},
                  
                  topFontcolor: String,
                  topTextalign: String,
                  topBackgroundcolor: String,
                  topTexttransparency: String,
                  
                  imagePosition: String,

                  bottomTextcolor: String,
                  bottomTextalign: String,
                  bottomBackgroundcolor: String,
                  bottomTransparency: String,

                  //2nd page:
                  articleTitle2: String,
                  article2: String,
                  img2: {type: String, default: null},
                  source2: String,
                  bottomText2: String,

                  //3rd page:
                  articleTitle3: String,
                  article3: String,
                  img3: {type: String, default: null},
                  source3: String,
                  bottomText3: String,


                  //4th page:----------------------------------------
                  articleTitle4: String,
                  article4: String,
                  img4: {type: String, default: null},
                  source4: String,
                  bottomText4: String,


                 // ---------------Mustakim start------------

                  //5th page:
                  articleTitle5: String,
                  article5: String,
                  img5: {type: String, default: null},
                  source5: String,
                  bottomText5: String,


                  //6th page:
                  articleTitle6: String,
                  article6: String,
                  img6: {type: String, default:null},
                  source6: String,
                  bottomText6: String,

                  //7th page:------------------------------------------------
                  articleTitle7: String,
                  article7: String,
                  img7: {type: String, default:null},
                  source7: String,
                  bottomText7: String,


                  //8th page:---------------------------------------------------
                  articleTitle8: String,
                  article8: String,
                  img8: {type: String, default:null},
                  source8: String,
                  bottomText8: String,

                  //9th page:---------------------------------------------------
                  articleTitle9: String,
                  article9: String,
                  img9: {type: String, default:null},
                  source9: String,
                  bottomText9: String,


                  //10th page:-----------------------------------------------------
                  articleTitle10: String,
                  article10: String,
                  img10: {type: String, default:null},
                  source10: String,
                  bottomText10: String,


                  //11th page:------------------------------------------------------
                  articleTitle11: String,
                  article11: String,
                  img11: {type: String, default:null},
                  source11: String,
                  bottomText11: String,

                  //12th page:-------------------------------------------------------
                  articleTitle12: String,
                  article12: String,
                  img12: {type: String, default:null},
                  source12: String,
                  bottomText12: String,

                  //13th page:---------------------------------------------------------
                  articleTitle13: String,
                  article13: String,
                  img13: {type: String, default:null},
                  source13: String,
                  bottomText13: String,

                  //14th page:--------------------------------------------------------------
                  articleTitle14: String,
                  article14: String,
                  img14: {type: String, default:null},
                  source14: String,
                  bottomText14: String,


                  //15th page:---------------------------------------------------------------
                  articleTitle15: String,
                  article15: String,
                  img15: {type: String, default:null},
                  source15: String,
                  bottomText15: String,


                  //16th page:----------------------------------------------------------------
                  articleTitle16: String,
                  article16: String,
                  img16: {type: String, default:null},
                  source16: String,
                  bottomText16: String, 


                  newimage1:{type: String, default:null},
                  newimage2:{type: String, default:null},
                  newimage3:{type: String, default:null},
                  newimage4:{type: String, default:null},

                  morepage: String,
                  //----------------Mustakim End---------              


              }],
              //------------ End assign artist -------------


              acceptArticle:[{
                  title: {type: String,default: 0},
                  article: {type: String,default: 0},
                  status: {type: String,default: 0},

              }]



    })
    
var hash = function(passwd, salt) {
  
return crypto.createHmac('sha256', salt).update(passwd).digest('hex');
};


UserSchema.methods.setPassword = function(passwordString) {
    
    this.password = hash(passwordString, this.salt);
};
UserSchema.methods.isValidPassword = function(passwordString) {
    return this.password === hash(passwordString, this.salt);
};

 mongoose.model('User',UserSchema);