var mongoose = require('mongoose')
  , User = mongoose.model('User')
  , fs = require('fs')
 // , im = require('imagemagick')
  , gm = require('gm')
  ,password_generator=require('password-generator')
  , mandrill = require('node-mandrill')('dlYF7-zjxHaIBVixKuq7XQ')
  , _ = require("underscore")

var x=0;
var lastArticleCount=0;
exports.authCallback = function (req, res, next) {

//console.log("already used: "+req.user.alreadyUsed);
// if(req.user.alreadyUsed==1){
//    res.redirect('/signup#signupError');
// }
// else{

  req.user.userType='Author';
 
  req.user.save(function(err){
    if(err)
      console.log('Error');
    else
      console.log('Done');
  })
  res.redirect('/writeNewArticle')
  //}




}
exports.commentSubmit=function(req,res){
  var userId=req.user._id;
  var comment=req.body.comment;
  User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
      for(var i=0;i<user.length;i++)
         {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                if(user[i].writeArticle[j]._id==req.body.id)
                {
                    user[i].writeArticle[j].articleImage_comment.push({comment:comment,commenttator:userId});
                }
            }
            user[i].save(function(err){
            if(err)
              console.log('Error');
            else
              console.log('Done');
          })
            // res.redirect('/article');
            res.send("done")
        }
    }
  })
}


exports.writeNewArticle=function(req,res)
{
   
    User.findOne({_id:req.user._id}).exec(function(err,user){
    if(err){
      console.log(err);
    }
    else if(user){
       res.render('writeNewArticle', {
       title: 0,
       user:user,
   
  })
    }

    })
   
}
exports.viewProfile=function(req,res){
   userId=req.param('articleUser');
  
  User.findOne({_id:userId}).exec(function(err,user){
    if(err)
      console.log(err);
    else{
      
      res.render('viewProfile',{
        user:user,
        currentUser:req.user,
      })
    }

  })
}
exports.updateUserProfile=function(req,res){
  userId=req.user._id;//req.params.userId;
  
  User.findOne({_id:userId}).exec(function(err,user){
    if(err)
      console.log(err);
    else{
      
      res.render('updateProfile',{
        user:user,
        currentUser:req.user,
      })
    }

  })
  
}

exports.manageamaderkotha=function(req,res)
{
    
    req.user.amaderkotha=req.body.text;

    req.user.save(function(err){
      if(err)
        console.log('Error');
      else
        console.log('Save data');
    })
    res.send('Done');


}

exports.manageWriteArticle=function(req,res)
{
    req.user.submitarticle=req.body.text;

    req.user.save(function(err){
      if(err)
        console.log('Error');
      else
        console.log('Save data');
    })
    res.send('Done')


}
exports.manageguideAuthor=function(req,res)
{
    req.user.guideAuthor=req.body.text;
    req.user.save(function(err){
      if(err)
        console.log('Error');
      else
        console.log('Save data');
    })
    res.send('Done')


}
exports.manageDonation=function(req,res)
{
    req.user.donation=req.body.text;

    req.user.save(function(err){
      if(err)
        console.log('Error');
      else
        console.log('Save data');
    })
    res.send('Done')


}

exports.signin = function (req, res) {
  res.render('signIn', {
    title: 'Signin',
    message: req.flash('error')
  })
}
exports.congrtzAuthor=function(req,res)
{
  req.user.textcongrtzMessageAuthor= req.body.message;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
    {
      console.log("Save")
    }
  })
     mandrill('/messages/send', {


          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Congratulation",
              "html": "" + req.body.message,
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else {
            console.log(response);

          }
            
      });
    res.send('DOne');
}
exports.congrtzArtist=function(req,res)
{
  req.user.textcongrtzMessageArtist= req.body.message;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
    {
      console.log("Save")
    }
  })
     mandrill('/messages/send', {


          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Congratulation",
              "html": "" + req.body.message,
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else {
            console.log(response);

          }
            
      });
    res.send('DOne');
}
exports.areYouArtist=function(req,res)
{
      mandrill('/messages/send', {


          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Interested for artist !!",
              "html": "Message:" + req.body.message,
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else {
            console.log(response);

          }
            
      });
    res.send('DOne');


}


exports.acceptArtistArticle=function(req,res)
{
      for(var i=0;i<req.user.selectedArtist.length;i++)
      {
          if(req.user.selectedArtist[i]._id == req.body.index)
          {
              req.user.selectedArtist[i].status ="Accepted";             
          }
      }
       req.user.save(function(err){
            if(err)
                console.log('Error');
            // else
            //     res.send('Done');
        });
      User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
             {
              for(var j=0;j<user[i].writeArticle.length;j++)
              {
                  if(user[i].writeArticle[j]._id==req.body.index)
                  {

                    for(var k=0;k<user[i].writeArticle[j].article_artist.length;k++)
                    {
                        if(user[i].writeArticle[j].article_artist[k].articleArtistemail==req.user.email)
                        {
                          user[i].writeArticle[j].article_artist[k].status='Bidding in progress';
                            user[i].save(function(err){
                              if(err)
                                      console.log('Error');
                                  else
                                      res.send('Done');
                              });
                        }
                    }
                  }
              }
             }  
        }
      })

      

}
exports.skipArtist=function(req,res){
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
     for(var i=0;i<user.length;i++)
     {
      for(var j=0;j<user[i].writeArticle.length;j++)
      {
          if(user[i].writeArticle[j]._id==req.param('articleId'))
          {
              user[i].writeArticle[j].status='Received Author’s Response';
              user[i].save(function(err){
              if(err)
                console.log('Error');
              else
                {
                   res.redirect("/assignartist");
                }
            });
          }
      }
     }
    }
  })
}

exports.previouslyworkedartist=function(req,res){
  var acceptedArticleForBid=[];
  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            { 
                for(var k=0;k<req.user.acceptedArtist.length;k++)
                {
                    if(user[i].writeArticle[j]._id==req.user.acceptedArtist[k].articleId&&user[i].writeArticle[j].status=='Received Author’s Response')
                    {
                      acceptedArticleForBid[count]=user[i].writeArticle[j];
                      count++;
                    }
                }
            }
        }
        res.render('previouslyworkedartist',{
          acceptedArticleForBid:acceptedArticleForBid,
          user:req.user,
        })
    }
  })
}
exports.currentlyworkingartist=function(req,res)
{
      res.render('currentlyworkingartist',{
        user:req.user,
      })

}
exports.waiting_for_acceptence=function(req,res)
{
      res.render('waiting_acceptence',{
        user:req.user,
      })

}
exports.open_for_bid=function(req,res){
  var article=[];
  var articleId=[];
  var author=[];
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
      for(var i=0;i<user.length;i++)
      {
        (function(i){
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                article.push(user[i].writeArticle[j]);
                articleId.push(user[i].writeArticle[j]._id);
                author.push(user[i])
            }
        })(i)
         if(i==user.length-1)
         {
            res.render('open_for_bid',{
              user:req.user,
              article:article,
              articleId:articleId,
              author:author,
            })
         } 
      }
    }
  })
  
  
}
exports.open_for_rebid=function(req,res){
  var article=[];
  var articleId=[];
  var author=[];
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
      for(var i=0;i<user.length;i++)
      {
        (function(i){
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                article.push(user[i].writeArticle[j]);
                articleId.push(user[i].writeArticle[j]._id);
                author.push(user[i])
            }
        })(i)
         if(i==user.length-1)
         {
            res.render('open_for_rebid',{
              user:req.user,
              article:article,
              articleId:articleId,
              author:author,
            })
         } 
      }
    }
  })
  
  
}
exports.contact=function(req,res)
{
      mandrill('/messages/send', {


          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Contact Information",
              "html": + "Message:" + req.body.message,
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else 
            console.log(response);
      });
    res.send('DOne');    
}
exports.suggestion=function(req,res)
{
    mandrill('/messages/send', {
          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Suggestion for Patarboi !!",
              "html": "<p>Message:</p>" + req.body.suggestion,
            },
      }, function(error, response)
      {

          if (error) console.log( JSON.stringify(error) );

          //everything's good, lets see what mandrill said
          else
          {
              console.log(response);
              res.send('DOne');
          } 

            
      });
    
}

exports.amaderkotha=function(req,res)
{


  var amaderkotha;



      User.find().exec(function(err,user){

        if(err)
          console.log('Error');
        else
        {

          for(var i=0;i<user.length;i++)
          {

              if(user[i].userType == 'admin')
              {
                  amaderkotha=user[i].amaderkotha;
                  res.render('amaderkotha',{
                        data:0,
                        amaderkotha:amaderkotha,

                     }); 

              }

          }
        }
      });
}

exports.managewebsite=function(req,res)
{
  var check=0;
  var reviewer=[];
  var artist=[];
  var author=[];
  var completeArticleArtist=[];
  var requestAuthorArticle=[];
  var requestAuthor=[];
  var thankforArtWork=[];
  var thankforArtWorkArtist=[];
  var article_user=[];
  var reminderArtistArticle=[];
  var submittingArticle=[];
  var submittingAuthor=[];
  var rejectedAuthor=[];
  var rejectedArticle=[];
  var credit=[];
  var allDonors=[];
  var admin;
  User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.length;i++)
        {
            if(user[i].reviewerType=='Reviewer'||user[i].reviewer.length>0)
              reviewer.push(user[i]);
        }
        for(var i=0;i<user.length;i++)
        {
            if(user[i].userType=='Author')
              author.push(user[i]);
        }
        for(var i=0;i<user.length;i++)
        {
            if(user[i].userType=='admin')
              credit=user[i].creditForCompany;
            allDonors=user[i].donors;
            admin=user[i];
        }
        for(var i=0;i<user.length;i++)
        {
          for(var j=0;j<user[i].userSubtype.length;j++)
          {
               if(user[i].userSubtype[j].subtype=='Artists'||user[i].selectedArtist.length>0)
                artist.push(user[i]);
          }
        }
        for(var i=0;i<user.length;i++)
        {
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
               if(user[i].writeArticle[j].status=='complete')
               {
                  for(var k=0;k<user[i].writeArticle[j].bidding_Article.length;k++)
                  {
                      for(var l=0;l<completeArticleArtist.length;l++)
                      {
                          if(user[i].writeArticle[j].bidding_Article[k].artistEmail==completeArticleArtist[l])
                            check=1;
                      }
                      if(check==0)
                      completeArticleArtist.push(user[i].writeArticle[j].bidding_Article[k].artistEmail)
                  }
               }
          }
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
               if(user[i].writeArticle[j].selectedartistImagePage1||user[i].writeArticle[j].selectedartistImagePage2||user[i].writeArticle[j].selectedartistImagePage3||user[i].writeArticle[j].selectedartistImagePage4)
               {
                  if(user[i].writeArticle[j].status=='Request Authors Approval')
                  {
                      requestAuthorArticle.push(user[i].writeArticle[j]);
                      requestAuthor.push(user[i]);
                  }
               }
          }
           for(var i=0;i<user.length;i++)
            {
              for(var j=0;j<user[i].writeArticle.length;j++)
              {
                   if(user[i].writeArticle[j].status== 'Bidding Proccessing')
                   {
                      for(var k=0;k<user[i].writeArticle[j].bidding_Article.length;k++)
                      {
                        if(user[i].writeArticle[j].bidding_Article[k].pageSubmitted!=0)
                        {
                            thankforArtWorkArtist.push(user[i].writeArticle[j].bidding_Article[k].artistEmail);
                            thankforArtWork.push(user[i].writeArticle[j]);
                        }
                          
                      }
                   }
                    
              }
            }
             for(var i=0;i<user.length;i++)
              {
                  if(user[i].userType == 'Author')
                  {
                      article_user.push(user[i]);
                  }
              }
              for(var i=0;i<user.length;i++)
              {
                 for(var j=0;j<user[i].writeArticle.length;j++)
                  {
                    // if(user[i].writeArticle[j].status=='Request Artist')
                     {
                        reminderArtistArticle.push(user[i].writeArticle[j]);
                     }
                  }
              } 
              for(var i=0;i<user.length;i++)
              {
                 for(var j=0;j<user[i].writeArticle.length;j++)
                  {
                     if(user[i].writeArticle[j].status=='waiting')
                     {
                        submittingArticle.push(user[i].writeArticle[j]);
                        submittingAuthor.push(user[i]);
                     }
                  }
              }    
              for(var i=0;i<user.length;i++)
              {
                 for(var j=0;j<user[i].writeArticle.length;j++)
                  {
                     if(user[i].writeArticle[j].status=='Rejected')
                     {
                        rejectedArticle.push(user[i].writeArticle[j]);
                        rejectedAuthor.push(user[i]);
                     }
                  }
              }    
        }
        res.render('managewebsite',{
          reviewer:reviewer,
          artist:artist,
          author:author,
          completeArticleArtist:completeArticleArtist,
          requestAuthorArticle:requestAuthorArticle,
          requestAuthor:requestAuthor,
          thankforArtWork:thankforArtWork,
          thankforArtWorkArtist:thankforArtWorkArtist,
          article_user:article_user,
          reminderArtistArticle:reminderArtistArticle,
          submittingArticle:submittingArticle,
          submittingAuthor:submittingAuthor,
          rejectedArticle:rejectedArticle,
          rejectedAuthor:rejectedAuthor,
          credit:credit,
          allDonors:allDonors,
          admin:admin,
        });
    }
  })
}
exports.donation=function(req,res)
{
      var donation;



          User.find().exec(function(err,user){
            if(err)
              console.log('Error');
            else
            {

              for(var i=0;i<user.length;i++)
              {

                  if(user[i].userType == 'admin')
                  {


                      console.log('Test data');
                      console.log(user[i].donation);
                      donation=user[i].donation;
                      res.render('donation',{
                            data:0,
                            donation:donation,

                         }); 

                  }

              }
            }
          });


}

exports.adminManageWebsite=function(req,res)
{
    User.find().exec(function(err,user){
        if(err)
          console.log('Error');
        else
        {
            for(var i=0;i<user.length;i++)
            {
                if(user[i].userType == 'admin')
                    {
                      user[i].amaderkotha=req.body.amaderkotha;
                      user[i].submitarticle=req.body.writearticle;
                      user[i].donation=req.body.donation;
                      user[i].save(function(err){

                            if(err)
                              console.log('Error');
                            else
                              {
                                 res.send("Done");
                              }
                          });
                    }

            }




        }

    });

}

exports.searchArticleByArticle=function(req,res){
  var author=[];
  var article=[];
   var article=[];
   var biddingAllArtist;
   var biddingArtist;
  var c=0;
  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
      for(var i=0;i<user.length;i++)
      {
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
              if(user[i].writeArticle[j].bookTitle==req.body.searchByArticle)
              {
                    author[c]=user[i];
                    article[c]=user[i].writeArticle[j];
                    biddingAllArtist=user[i].writeArticle[j].bidding_Article;
                    for(var k=0;k<user[i].writeArticle[j].bidding_Article.length;k++){
                      if(user[i].writeArticle[j].bidding_Article[k].status=='Accepted')
                      {
                           biddingArtist=user[i].writeArticle[j].bidding_Article[k];
                      }
                    }
                    c++;
              }
          }
      }
     // console.log( biddingArtist)
      res.render('managecostArticle',{
        author:author,
        article:article,
        biddingAllArtist:biddingAllArtist,
        biddingArtist:biddingArtist,
      })
    }
  })
}

exports.admindistributionDetails=function(req,res)
{
   User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                if(user[i].writeArticle[j]._id==req.body.articleId)
                {
                  user[i].writeArticle[j].distributionRef=req.body.disRef;
                  user[i].writeArticle[j].distributedTo=req.body.distributedTo;
                  user[i].writeArticle[j].distributionBy=req.body.distributionBy;
                  user[i].writeArticle[j].distributionDate=req.body.disDatepicker1;
                  user[i].writeArticle[j].collectedDate=req.body.disDatepicker2;
                  user[i].writeArticle[j].distributionVolume=req.body.distributionVolume;
                  user[i].writeArticle[j].distributionRevenue=req.body.distributionRevenue;
                  user[i].writeArticle[j].amountCollected=req.body.amountCollected;
                  user[i].save(function(err){
                    if(err)
                      console.log(err)
                    else
                    {
                        res.jsonp("send")
                    }
                  })
                }
            }
        }
    }
   })
    
}
exports.productionDetails=function(req,res)
{
   User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                if(user[i].writeArticle[j]._id==req.body.articleId)
                {
                  user[i].writeArticle[j].productionRef=req.body.ref;
                  user[i].writeArticle[j].produceBy=req.body.producedBy;
                  user[i].writeArticle[j].collectedBy=req.body.collectedBy;
                  user[i].writeArticle[j].productionDate=req.body.datepicker1;
                  user[i].writeArticle[j].paidDate=req.body.datepicker2;
                  user[i].writeArticle[j].productionVolume=req.body.productionVolume;
                  user[i].writeArticle[j].productionCost=req.body.productionCost;
                  user[i].writeArticle[j].amountPaid=req.body.amountPaid;
                  user[i].save(function(err){
                    if(err)
                      console.log(err)
                    else
                    {
                        res.jsonp("send")
                    }
                  })
                }
            }
        }
    }
   })
    
}
exports.searchArticleProductionCost=function(req,res){
  //console.log(req.body);
  var author=[];
  var article=[];
  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                if(user[i].writeArticle[j].bookTitle==req.body.searchByArticle)
                { 
                    author[count]=user[i];
                    article[count]=user[i].writeArticle[j];
                    count++;
                }
            }
        }
        res.render('searchArticleProductionCost',{
          author:author,
          article:article,
        })
    }
  })
}
exports.managecost=function(req,res)
{
    var artist=[];
    var articleTitle=[];
    var count=0;
    var c=0;
    User.find().exec(function(err,user){
      if(err)
        console.log(err);
      else
      {
          for(var i=0;i<user.length;i++)
          {
           for(var j=0;j<user[i].userSubtype.length;j++)
           {
              if(user[i].userSubtype[j].subtype=='Artists'||user[i].selectedArtist.length!=0)
              {
                  artist[count]=user[i].name;
                  count++;
              }
           }
           for(var j=0;j<user[i].writeArticle.length;j++)
           {
              articleTitle[c]=user[i].writeArticle[j].bookTitle;
              c++;
           }
          }
           res.render('managecost',{
            artist:artist,
            articleTitle:articleTitle,
           }); 
      }
    })
   

}

exports.adminManagecost=function(req,res)
{
   console.log(req.body);
    User.find().exec(function(err,user){
        if(err)
          console.log('Error');
        else
        {
            //console.log('Find');
            for(var i=0;i<user.length;i++)
            {
                if(user[i].userType == 'admin')
                    {

                      console.log('Admin');
                      user[i].bookprice=req.body.bookprice;
                      user[i].shippingprice=req.body.shippingprice;

                      user[i].save(function(err){

                          if(err)
                            console.log('Error');
                          else
                           res.render('managecost');

                      });

                    }

            }




        }

    });
}

exports.adminpassword=function(req,res)
{
    console.log(req.body);
    res.send('Done');

}
exports.donateAmount=function(req,res)
{
    User.findOne({userType:'admin'}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
        user.donors.push({donor_ID:req.user._id,name:req.user.name,bookDistributionID:req.body.articleId,amount:req.body.amount});
        user.save(function(err){
          if(err)
            console.log(err)
          else
            res.send("save")
        })
      }
    })
}
exports.donors=function(req,res)
{ var article=[];
  var check;
  User.findOne({userType:'admin'}).exec(function(err,admin){
      if(err)
        console.log(err)
      else
      {
         User.find().exec(function(err,user){
          if(err)
            console.log(err);
          else
          {
              for(var i=0;i<user.length;i++)
              {
                (function(i){
                    for(var j=0;j<user[i].writeArticle.length;j++)
                      {
                        check=0;
                        for(var k=0;k<admin.donors.length;k++)
                        {
                          (function(k){

                              if(admin.donors[k].donor_ID==req.user._id&&admin.donors[k].bookDistributionID==user[i].writeArticle[j]._id)
                              {
                                check=1;
                                console.log("admin.donors[k].donor_ID")
                              }
                          })(k);
                            if(k==admin.donors.length-1&&check==0)
                              article.push(user[i].writeArticle[j]);
                        }
                          
                      }
                })(i);
                 if(i==user.length-1)
                 {
                    res.render('donor',{
                    user: req.user,
                    article:article,
                   });
                } 
              }
          }
        })
      }
    })
 
   
      

}
exports.guideline=function(req,res)
{ 
  User.findOne({userType:'admin'}).exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
      res.render('guideline',{
        admin:user,
      });
    }
  })
    

}
exports.viewdonation=function(req,res)
{
  res.render('viewdonation');
}
exports.viewdistribution=function(req,res)
{
  var articleTitle=[];
  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
         for(var i=0;i<user.length;i++)
         {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                articleTitle[count]=user[i].writeArticle[j].bookTitle;
                count++;
            }
         }
          res.render('viewdistribution',{
            articleTitle:articleTitle,
          });
    }
  })
 
}

exports.reviewarticle=function(req,res){
   console.log(' Review article');
   console.log(req.user.name);
   var user_name=req.user.name;
   var user=req.user;
   res.render('reviewarticle',{

        name: user_name,
        user:user

        });  
}
/*
exports.tagsearch=function(req,res)
{

      var author=[];
      var counter=0;
      var tag =req.body.tagInput;
      User.find().exec(function(err,user)
      {
        

        if(err)
            console.log('Error');
        else
        {
            

            for(var i=0;i<user.length;i++)
            {
                if(user[i].userType == 'Author')
                {
                    author[counter]=user[i];
                    counter++;
                }

            }            

          res.render('index', {
          
            
            user:author, 
            tag:tag,
            
          })


        }


      });

}  */
exports.showAuhtor=function(req,res)
{
    console.log('Author page');
    var name=[];
    var email=[];
    var articleNo=[];
    var count=0;
    User.find().exec(function(err,user)
    {
        
        if(err)
          console.log('Error');
        else
        {
            
            console.log(user.length);
            for(var i=0;i<user.length;i++)
            {
                  if(user[i].userType == 'Author')
                  {
                      name[count]=user[i].name;
                      email[count]=user[i].email;
                      articleNo[count]=user[i].writeArticle.length;
                      count++;
                    //console.log('author ami');
                  }

            }
            console.log(name);
            console.log(articleNo);
             res.render('showAuhtor',{
                  no:count,
                  name:name,
                  email:email,
                  articleNo:articleNo

                }); 

        }


    });




}

exports.signup = function (req, res) {
  console.log('Test signup');
    res.render('signUp');
}
exports.biddingArticle=function(req,res){
    var article=[];
    var author=[];
    var count=0;
    User.find().exec(function(err,user){
      if(err)
        console.log(err);
      else
      {
        for(var i=0;i<user.length;i++)
        {
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
            console.log('user[i].writeArticle[j].bidding_Article.length')
              if(user[i].writeArticle[j].bidding_Article.length!=0)
              {   console.log('user[i].writeArticle[j].bidding_Article.lengthdddf')
                 
                  author[count]=user[i];
                  article[count]=user[i].writeArticle[j];
                  count++;
              }
          }
        }
        console.log(article)
        res.render('biddingArticle',{
          article:article,
          author:author,
        })
      }
    })
}
exports.acceptBidding=function(req,res){
      console.log(req.param('artist'));
      console.log(req.param('articleId'));
      var author;
      User.findOne({_id:req.param('artist')}).exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            user.acceptedArtist.push({articleId:req.param('articleId')});
            user.save(function(err){
              if(err)
              console.log(err);
            else
            {
                 User.find().exec(function(err,user1){
                    if(err)
                      console.log(err)
                    else
                    {
                        for(var i=0;i<user1.length;i++)
                        {
                            for(var j=0;j<user1[i].writeArticle.length;j++)
                            {
                                if(user1[i].writeArticle[j]._id==req.param('articleId'))
                                {
                                    for(var k=0;k<user1[i].writeArticle[j].bidding_Article.length;k++)
                                    {
                                        if(user1[i].writeArticle[j].bidding_Article[k].artistId==req.param('artist'))
                                        {
                                          author=user1[i]._id;
                                            user1[i].writeArticle[j].bidding_Article[k].status='Accepted';
                                        }
                                        // else
                                        // {
                                        //     user1[i].writeArticle[j].bidding_Article[k].status='Rejected';
                                        // }
                                          
                                    }
                                }
                            }
                            user1[i].save(function(err){
                                if(err)
                                  console.log("abc "+err); 
                            })

                            
                        }
                         res.redirect('/viewSingleBiddingArticle?author='+author+'&articleID='+req.param('articleId'));
                    }
                  }) 
            }
          })
        }
      })
      
}
exports.viewSingleBiddingArticle=function(req,res){
  var author=req.param('author');
  var articleid=req.param('articleID');
  var biddingDetais;
  var article;
  User.findOne({_id:req.param('author')}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==req.param('articleID'))
            {   article=user.writeArticle[i];
                biddingDetais=user.writeArticle[i].bidding_Article;
            }
        }
        res.render('viewSingleBiddingArticle',{
          biddingDetais:biddingDetais,
          article:article,
        })
    }
  })
}
exports.admin=function(req,res){
   
    var waiting=0;
    var notreviewed=0;
    var accepted=0;
    var imagerequest=0;
    var complete=0;
    var requestReviewer=0;
    var AuthorsApproval=0;
    var reviewcomplete=0;
    var biddingProcess=0;
    var ApprovedbyAuthor=0;
    User.find().exec(function(err,user)
    {
        
        if(err)
          console.log('Error');
        else
        {
            

            for(var i=0;i<user.length;i++)
            {
                for(j=0;j<user[i].writeArticle.length;j++)
                {

                  if(user[i].writeArticle[j].status == 'waiting')
                  {

                      waiting++;
                  }
                  if(user[i].writeArticle[j].status == 'Request Reviewer')
                  {

                      requestReviewer++;
                  }
                  if(user[i].writeArticle[j].status== 'Accepted')
                  {
                    accepted++;

                  }
                  if(user[i].writeArticle[j].status== 'Request Artist')
                  {

                      imagerequest++;
                  }
                  if(user[i].writeArticle[j].status== 'complete')
                  {
                    complete++;
                  }
                  if(user[i].writeArticle[j].status== 'Review complete'||user[i].writeArticle[j].status=='With Condition')
                  {
                    reviewcomplete++;
                  }

                  if(user[i].writeArticle[j].status== 'Bidding Proccessing'||user[i].writeArticle[j].status== 'Request Authors Approval'||user[i].writeArticle[j].status== 'Received Author’s Response')
                  {

                    AuthorsApproval++;
                  }
                  if(user[i].writeArticle[j].status== 'Bidding in progress')
                  {

                    biddingProcess++;
                  }
                  if(user[i].writeArticle[j].status== 'Received Author’s Response')
                  {
                      ApprovedbyAuthor++;
                  }
                }

            } 

            // for (var i=0;i<user.length;i++)
            // {
            //     for(j=0;j<user[i].reviewArticle.length;j++)
            //     {

            //       if(user[i].reviewArticle[j].status == 'Not Reviewed')
            //       {

            //           notreviewed++;
            //       }
            //     }

            // }  




            //console.log(author);        
          if(req.user.userType=='admin')
          {
              res.render('admin',{
              waiting: waiting,
              notreviewed:notreviewed,
              accepted:accepted,
              reviewcomplete:reviewcomplete,
              imagerequest:imagerequest,
              complete:complete,
              AuthorsApproval:AuthorsApproval,
              requestReviewer:requestReviewer,
              biddingProcess:biddingProcess,
              ApprovedbyAuthor:ApprovedbyAuthor

            });
          }
          else{
            res.redirect('/');
          }
           

        }


    });

 

}
exports.artistArticleForbid=function(req,res){
   articleId=req.param('articleId');

     var acceptedArticleForBid;

      var count=0;
      User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            {
                for(var j=0;j<user[i].writeArticle.length;j++)
                { 
                   
                     // console.log(user[i].writeArticle[j]._id+ "  "+req.user.acceptedArtist[k].articleId)
                        if(user[i].writeArticle[j]._id==articleId)
                        {
                          acceptedArticleForBid=user[i].writeArticle[j];
                         
                        }
              
                }
            }
            res.render('artistArticleForbid',{
              article:acceptedArticleForBid
            })
        }
      })
 
}
exports.submitArtistImage=function(req,res){
     console.log(req.body); 
     var image1=[];
     image1=req.files.img;

     var articleid=req.body.index;
     // console.log(image1.length);
     // console.log(req.files.img);
     if(req.files.img){
     if(req.files.img.length>=2){
      var img_path=[];
      for(var i=0;i<req.files.img.length;i++)
      {
        var image=req.files.img[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path=[];
       var image=req.files.img;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path[0]='/article_image/'+image.name;
        })(image)
    }

    }
    if(req.files.img2){
      if(req.files.img2.length>=2){
      var img_path2=[];
      for(var i=0;i<req.files.img2.length;i++)
      {
        var image=req.files.img2[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path2[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path2=[];
       var image=req.files.img2;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path2[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img3){
      if(req.files.img3.length>=2){
      var img_path3=[];
      for(var i=0;i<req.files.img3.length;i++)
      {
        var image=req.files.img3[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path3[i]='/article_image/'+image.name;
        })(image)

      }
    }
     else 
    {
      var img_path3=[];
       var image=req.files.img3;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path3[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img4){
     if(req.files.img4.length>=2){
      var img_path4=[];
      for(var i=0;i<req.files.img4.length;i++)
      {
        var image=req.files.img4[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path4[i]='/article_image/'+image.name;
        })(image)

      }
    }
     else 
    {
      var img_path4=[];
       var image=req.files.img4;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path4[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img5){
      if(req.files.img5.length>=2){
      var img_path5=[];
      for(var i=0;i<req.files.img5.length;i++)
      {
        var image=req.files.img5[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path5[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path5=[];
       var image=req.files.img5;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path5[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img6){
 if(req.files.img6.length>=2){
      var img_path6=[];
      for(var i=0;i<req.files.img6.length;i++)
      {
        var image=req.files.img6[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path6[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path6=[];
       var image=req.files.img6;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path6[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img7){
     if(req.files.img7.length>=2){
      var img_path7=[];
      for(var i=0;i<req.files.img7.length;i++)
      {
        var image=req.files.img7[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path7[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path7=[];
       var image=req.files.img7;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path7[0]='/article_image/'+image.name;
        })(image)
    }
  }
if(req.files.img8){
     if(req.files.img8.length>=2){
      var img_path8=[];
      for(var i=0;i<req.files.img8.length;i++)
      {
        var image=req.files.img8[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path8[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path8=[];
       var image=req.files.img8;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path8[0]='/article_image/'+image.name;
        })(image)
    }
  }
if(req.files.img9){
     if(req.files.img9.length>=2){
      var img_path9=[];
      for(var i=0;i<req.files.img9.length;i++)
      {
        var image=req.files.img9[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path9[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path9=[];
       var image=req.files.img9;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path9[0]='/article_image/'+image.name;
        })(image)
    }
  }
   if(req.files.img10){
     if(req.files.img10.length>=2){
      var img_path10=[];
      for(var i=0;i<req.files.img10.length;i++)
      {
        var image=req.files.img10[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path10[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else
    {
      var img_path10=[];
       var image=req.files.img10;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path10[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img11){
     if(req.files.img11.length>=2){
      var img_path11=[];
      for(var i=0;i<req.files.img11.length;i++)
      {
        var image=req.files.img11[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path11[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path11=[];
       var image=req.files.img11;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path11[0]='/article_image/'+image.name;
        })(image)
    }
  }
if(req.files.img12){
     if(req.files.img12.length>=2){
      var img_path12=[];
      for(var i=0;i<req.files.img12.length;i++)
      {
        var image=req.files.img12[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path12[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path12=[];
       var image=req.files.img12;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path12[0]='/article_image/'+image.name;
        })(image)
    }
}

if(req.files.img13){
     if(req.files.img13.length>=2){
      var img_path13=[];
      for(var i=0;i<req.files.img13.length;i++)
      {
        var image=req.files.img13[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path13[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path13=[];
       var image=req.files.img13;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path13[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img14){
     if(req.files.img14.length>=2){
      var img_path14=[];
      for(var i=0;i<req.files.img14.length;i++)
      {
        var image=req.files.img14[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path14[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path14=[];
       var image=req.files.img14;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path14[0]='/article_image/'+image.name;
        })(image)
    }
}
if(req.files.img15){
     if(req.files.img15.length>=2){
      var img_path15=[];
      for(var i=0;i<req.files.img15.length;i++)
      {
        var image=req.files.img15[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path15[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path15=[];
       var image=req.files.img15;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path15[0]='/article_image/'+image.name;
        })(image)
    }
  }
  if(req.files.img16){
     if(req.files.img16.length>=2){
      var img_path16=[];
      for(var i=0;i<req.files.img16.length;i++)
      {
        var image=req.files.img16[i];
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path16[i]='/article_image/'+image.name;
        })(image)

      }
    }
    else 
    {
      var img_path16=[];
       var image=req.files.img16;
        (function(image){
          fs.readFile(image.path, function (err, data) {
               fs.writeFile('public/article_image/'+image.name,data, function (err) {
                   if (err) 
                    console.log(err)
                  else{
                    console.log('Done success');
                  }

               });
          });
          img_path16[0]='/article_image/'+image.name;
        })(image)
    }
  }
  var noPage=0;
  var dateNow = new Date();
    User.find().exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.length;i++)
          {
              for(var j=0;j<user[i].writeArticle.length;j++)
              {
                  if(user[i].writeArticle[j]._id==articleid)
                  {   
                    // if(img_path&&img_path[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path2&&img_path2[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path3&&img_path3[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path4&&img_path4[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path5&&img_path5[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path6&&img_path6[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path7&&img_path7[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path8&&img_path8[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path9&&img_path9[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path10&&img_path10[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path11&&img_path11[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path12&&img_path12[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path13&&img_path13[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path14&&img_path14[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path15&&img_path15[0]!='/article_image/')
                    //   noPage=noPage+1;
                    // if(img_path16&&img_path16[0]!='/article_image/')
                    //   noPage=noPage+1;

                    //if(img_path)
                  //     user[i].writeArticle[j].artistImagePage1=img_path;
                  //     if(img_path2)
                  //     user[i].writeArticle[j].artistImagePage2=img_path2;
                  //     if(img_path3)
                  //     user[i].writeArticle[j].artistImagePage3=img_path3;
                  //     if(img_path4)
                  //     user[i].writeArticle[j].artistImagePage4=img_path4;
                  //     if(img_path5)
                  //     user[i].writeArticle[j].artistImagePage5=img_path5;
                  //     if(img_path6)
                  //     user[i].writeArticle[j].artistImagePage6=img_path6;
                  //     if(img_path7)
                  //     user[i].writeArticle[j].artistImagePage7=img_path7;
                  //     if(img_path8)
                  //     user[i].writeArticle[j].artistImagePage8=img_path8;
                  //     if(img_path9)
                  //     user[i].writeArticle[j].artistImagePage9=img_path9;
                  //     if(img_path10)
                  //     user[i].writeArticle[j].artistImagePage10=img_path10;
                  //     if(img_path11)
                  //     user[i].writeArticle[j].artistImagePage11=img_path11;
                  //     if(img_path12)
                  //     user[i].writeArticle[j].artistImagePage12=img_path12;
                  //     if(img_path13)
                  //     user[i].writeArticle[j].artistImagePage13=img_path13;
                  //     if(img_path14)
                  //     user[i].writeArticle[j].artistImagePage14=img_path14;
                  //     if(img_path15)
                  //     user[i].writeArticle[j].artistImagePage15=img_path15;
                  //     if(img_path16)
                  //     user[i].writeArticle[j].artistImagePage16=img_path16;

                        user[i].writeArticle[j].status='Bidding Proccessing';
                        for(var k=0;k< user[i].writeArticle[j].bidding_Article.length;k++)
                        {
                            if(user[i].writeArticle[j].bidding_Article[k].artistId==req.user._id)
                            {
                              console.log("rumi "+k)
                             // user[i].writeArticle[j].bidding_Article[k].paid='Yes';
                             if(img_path&&img_path!='/article_image/'){
                              noPage=noPage+1;
                              console.log("dmfdf")
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage1.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage1[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage1[m].img=img_path[0];
                                     if(user[i].writeArticle[j].artistImagePage1[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage1.length-1)
                                      user[i].writeArticle[j].artistImagePage1.push({img:img_path[0],rating:req.body.ratingScore_1,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage1.length==0)
                                     user[i].writeArticle[j].artistImagePage1.push({img:img_path[0],rating:req.body.ratingScore_1,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path2&&img_path2!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage2.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage2[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage2[m].img=img_path2[0];
                                     if(user[i].writeArticle[j].artistImagePage2[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage2.length-1)
                                      user[i].writeArticle[j].artistImagePage2.push({img:img_path2[0],rating:req.body.ratingScore_2,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage2.length==0)
                                     user[i].writeArticle[j].artistImagePage2.push({img:img_path2[0],rating:req.body.ratingScore_2,artist:req.user._id,name:req.user.name});
                             }
                              if(img_path3&&img_path3!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage3.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage3[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage3[m].img=img_path3[0];
                                     if(user[i].writeArticle[j].artistImagePage3[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage3.length-1)
                                      user[i].writeArticle[j].artistImagePage3.push({img:img_path3[0],rating:req.body.ratingScore_3,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage3.length==0)
                                     user[i].writeArticle[j].artistImagePage3.push({img:img_path3[0],rating:req.body.ratingScore_3,artist:req.user._id,name:req.user.name});
                             }

                             if(img_path4&&img_path4!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage4.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage4[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage4[m].img=img_path4[0];
                                     if(user[i].writeArticle[j].artistImagePage4[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage4.length-1)
                                      user[i].writeArticle[j].artistImagePage4.push({img:img_path4[0],rating:req.body.ratingScore_4,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage4.length==0)
                                     user[i].writeArticle[j].artistImagePage4.push({img:img_path4[0],rating:req.body.ratingScore_4,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path5&&img_path5!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage5.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage5[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage5[m].img=img_path5[0];
                                     if(user[i].writeArticle[j].artistImagePage5[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage5.length-1)
                                      user[i].writeArticle[j].artistImagePage5.push({img:img_path5[0],rating:req.body.ratingScore_5,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage5.length==0)
                                     user[i].writeArticle[j].artistImagePage5.push({img:img_path5[0],rating:req.body.ratingScore_5,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path6&&img_path6!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage6.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage6[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage6[m].img=img_path6[0];
                                     if(user[i].writeArticle[j].artistImagePage6[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage6.length-1)
                                      user[i].writeArticle[j].artistImagePage6.push({img:img_path6[0],rating:req.body.ratingScore_6,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage6.length==0)
                                     user[i].writeArticle[j].artistImagePage6.push({img:img_path6[0],rating:req.body.ratingScore_6,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path7&&img_path7!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage7.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage7[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage7[m].img=img_path7[0];
                                     if(user[i].writeArticle[j].artistImagePage7[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage7.length-1)
                                      user[i].writeArticle[j].artistImagePage7.push({img:img_path7[0],rating:req.body.ratingScore_7,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage7.length==0)
                                     user[i].writeArticle[j].artistImagePage7.push({img:img_path7[0],rating:req.body.ratingScore_7,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path8&&img_path8!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage8.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage8[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage8[m].img=img_path8[0];
                                     if(user[i].writeArticle[j].artistImagePage8[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage8.length-1)
                                      user[i].writeArticle[j].artistImagePage8.push({img:img_path8[0],rating:req.body.ratingScore_8,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage8.length==0)
                                     user[i].writeArticle[j].artistImagePage8.push({img:img_path8[0],rating:req.body.ratingScore_8,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path9&&img_path9!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage9.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage9[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage9[m].img=img_path9[0];
                                     if(user[i].writeArticle[j].artistImagePage9[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage9.length-1)
                                      user[i].writeArticle[j].artistImagePage9.push({img:img_path9[0],rating:req.body.ratingScore_9,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage9.length==0)
                                     user[i].writeArticle[j].artistImagePage9.push({img:img_path9[0],rating:req.body.ratingScore_9,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path10&&img_path10!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage10.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage10[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage10[m].img=img_path10[0];
                                     if(user[i].writeArticle[j].artistImagePage10[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage10.length-1)
                                      user[i].writeArticle[j].artistImagePage10.push({img:img_path10[0],rating:req.body.ratingScore_10,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage10.length==0)
                                     user[i].writeArticle[j].artistImagePage10.push({img:img_path10[0],rating:req.body.ratingScore_10,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path11&&img_path11!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage11.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage11[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage11[m].img=img_path11[0];
                                     if(user[i].writeArticle[j].artistImagePage11[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage11.length-1)
                                      user[i].writeArticle[j].artistImagePage11.push({img:img_path11[0],rating:req.body.ratingScore_11,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage11.length==0)
                                     user[i].writeArticle[j].artistImagePage11.push({img:img_path11[0],rating:req.body.ratingScore_11,artist:req.user._id,name:req.user.name});

                             }
                             if(img_path12&&img_path12!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage12.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage12[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage12[m].img=img_path12[0];
                                     if(user[i].writeArticle[j].artistImagePage12[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage12.length-1)
                                      user[i].writeArticle[j].artistImagePage12.push({img:img_path12[0],rating:req.body.ratingScore_12,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage12.length==0)
                                     user[i].writeArticle[j].artistImagePage12.push({img:img_path12[0],rating:req.body.ratingScore_12,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path13&&img_path13!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage13.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage13[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage13[m].img=img_path13[0];
                                     if(user[i].writeArticle[j].artistImagePage13[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage13.length-1)
                                      user[i].writeArticle[j].artistImagePage13.push({img:img_path13[0],rating:req.body.ratingScore_13,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage13.length==0)
                                     user[i].writeArticle[j].artistImagePage13.push({img:img_path13[0],rating:req.body.ratingScore_13,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path14&&img_path14!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage14.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage14[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage14[m].img=img_path14[0];
                                     if(user[i].writeArticle[j].artistImagePage14[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage14.length-1)
                                      user[i].writeArticle[j].artistImagePage14.push({img:img_path14[0],rating:req.body.ratingScore_14,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage14.length==0)
                                     user[i].writeArticle[j].artistImagePage14.push({img:img_path14[0],rating:req.body.ratingScore_14,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path15&&img_path15!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage15.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage15[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage15[m].img=img_path15[0];
                                     if(user[i].writeArticle[j].artistImagePage15[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage15.length-1)
                                      user[i].writeArticle[j].artistImagePage15.push({img:img_path15[0],rating:req.body.ratingScore_15,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage15.length==0)
                                     user[i].writeArticle[j].artistImagePage15.push({img:img_path15[0],rating:req.body.ratingScore_15,artist:req.user._id,name:req.user.name});
                             }
                             if(img_path16&&img_path16!='/article_image/'){
                              noPage=noPage+1;
                                for(var m=0;m< user[i].writeArticle[j].artistImagePage16.length;m++)
                                  {
                                    if(user[i].writeArticle[j].artistImagePage16[m].artist==req.user._id)
                                      user[i].writeArticle[j].artistImagePage16[m].img=img_path16[0];
                                     if(user[i].writeArticle[j].artistImagePage16[m].artist!=req.user._id&&m==user[i].writeArticle[j].artistImagePage16.length-1)
                                      user[i].writeArticle[j].artistImagePage16.push({img:img_path16[0],rating:req.body.ratingScore_16,artist:req.user._id,name:req.user.name});
                                  }
                                  if(user[i].writeArticle[j].artistImagePage16.length==0)
                                     user[i].writeArticle[j].artistImagePage16.push({img:img_path16[0],rating:req.body.ratingScore_16,artist:req.user._id,name:req.user.name});
                             }
                              // if(img_path)
                              //   user[i].writeArticle[j].artistImagePage1.push({img:img_path[0],artist:req.user._id});
                                // if(img_path2)
                                // user[i].writeArticle[j].artistImagePage2.push({img:img_path2[0],artist:req.user._id});
                                // if(img_path3)
                                // user[i].writeArticle[j].artistImagePage3.push({img:img_path3[0],artist:req.user._id});
                                // if(img_path4)
                                // user[i].writeArticle[j].artistImagePage4.push({img:img_path4[0],artist:req.user._id});
                                // if(img_path5)
                                // user[i].writeArticle[j].artistImagePage5.push({img:img_path5[0],artist:req.user._id});
                                // if(img_path6)
                                // user[i].writeArticle[j].artistImagePage6.push({img:img_path6[0],artist:req.user._id});
                                // if(img_path7)
                                // user[i].writeArticle[j].artistImagePage7.push({img:img_path7[0],artist:req.user._id});
                                // if(img_path8)
                                // user[i].writeArticle[j].artistImagePage8.push({img:img_path8[0],artist:req.user._id});
                                // if(img_path9)
                                // user[i].writeArticle[j].artistImagePage9.push({img:img_path9[0],artist:req.user._id});
                                // if(img_path10)
                                // user[i].writeArticle[j].artistImagePage10.push({img:img_path10[0],artist:req.user._id});
                                // if(img_path11)
                                // user[i].writeArticle[j].artistImagePage11.push({img:img_path11[0],artist:req.user._id});
                                // if(img_path12)
                                // user[i].writeArticle[j].artistImagePage12.push({img:img_path12[0],artist:req.user._id});
                                // if(img_path13)
                                // user[i].writeArticle[j].artistImagePage13.push({img:img_path13[0],artist:req.user._id});
                                // if(img_path14)
                                // user[i].writeArticle[j].artistImagePage14.push({img:img_path14[0],artist:req.user._id});
                                // if(img_path15)
                                // user[i].writeArticle[j].artistImagePage15.push({img:img_path15[0],artist:req.user._id});
                                // if(img_path16)
                                // user[i].writeArticle[j].artistImagePage16.push({img:img_path16[0],artist:req.user._id});
  // console.log("rumi "+user[i].writeArticle[j].artistImagePage1[k])
                                 user[i].writeArticle[j].bidding_Article[k].completeDate=dateNow;
                                  user[i].writeArticle[j].bidding_Article[k].pageSubmitted=noPage;
                               user[i].save(function(err){
                                  if(err)
                                    console.log("err 2");
                                  else{
                                    User.findOne({_id:req.user._id}).exec(function(err,requser){
                                      if(err)
                                        console.log(err)
                                      else{
                                         for(var i=0;i<requser.selectedArtist.length;i++)
                                            {
                                              if(requser.selectedArtist[i]._id == req.body.index)
                                              {
                                                requser.selectedArtist[i].status='Finish';
                                                requser.save(function(err){
                                                  if(err)
                                                    console.log(err);
                                                })

                                              }

                                            }
                                        }
                                      })
                                                            
                                    }
                                     
                                })
                            }
                        }
                  }
              }
             
               
            }
          res.redirect('/userArticle');  
      }
    })
    //  if(req.files.imagefiles.length!=0){
    //   var img_path=[];
    //   console.log(req.files.imagefiles.length);
    //   for(var i=0;i<req.files.imagefiles.length;i++)
    //   { console.log(req.files.imagefiles[i].name)
    //     var image=req.files.imagefiles[i];
    //     (function(image){
    //       fs.readFile(image.path, function (err, data) {
    //            //console.log(name);
    //             fs.writeFile('public/article_image/'+image.name,data, function (err) {
    //               if (err) 
    //                 console.log('Error');
    //               else
    //               {
    //                 console.log('Done success');
    //               }
                    
    //             });
    //           });
    //           img_path[i]='/article_image/'+image.name;
    //     })(image);
    //   }
    // }
    
}
exports.editcontents=function(req,res){
  var author=req.param('id');
  var articleId=req.param('articleid');
  var article;
  User.findOne({_id:author}).exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==articleId)
            {
                article=user.writeArticle[i];
            }
        }
    }
    res.render('editcontents',{
      article:article,
      author:user,
    });
  })
    
}
exports.selectionArtWorkSubmit=function(req,res)
{
    console.log(req.body)

       User.findOne({_id:req.body.userId}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==req.body.articleID)
              {   user.writeArticle[i].artwork='Done';
                  if(req.body.checkbox_11)
                  {
                    user.writeArticle[i].articleImage=user.writeArticle[i].artistImagePage1[0].img;
                  }
                  else if(req.body.checkbox_12)
                  {
                    user.writeArticle[i].articleImage=user.writeArticle[i].artistImagePage1[1].img;
                  }
                  else if(req.body.checkbox_13)
                  {
                    user.writeArticle[i].articleImage=user.writeArticle[i].artistImagePage1[2].img;
                  }
                  else if(req.body.checkbox_14)
                  {
                    user.writeArticle[i].articleImage=user.writeArticle[i].articleImage;
                  }

                   if(req.body.checkbox_21)
                  {
                    user.writeArticle[i].img2=user.writeArticle[i].artistImagePage2[0].img;
                  }
                  else if(req.body.checkbox_22)
                  {
                    user.writeArticle[i].img2=user.writeArticle[i].artistImagePage2[1].img;
                  }
                  else if(req.body.checkbox_23)
                  {
                    user.writeArticle[i].img2=user.writeArticle[i].artistImagePage2[2].img;
                  }
                  else if(req.body.checkbox_24)
                  {
                    user.writeArticle[i].img2=user.writeArticle[i].img2;
                  }
                   if(req.body.checkbox_31)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].artistImagePage3[0].img;
                  }
                  else if(req.body.checkbox_32)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].artistImagePage3[1].img;
                  }
                  else if(req.body.checkbox_33)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].artistImagePage3[2].img;
                  }
                  else if(req.body.checkbox_34)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].img3;
                  }

                   if(req.body.checkbox_41)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].artistImagePage4[0].img;
                  }
                  else if(req.body.checkbox_42)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].artistImagePage4[1].img;
                  }
                  else if(req.body.checkbox_43)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].artistImagePage4[2].img;
                  }
                  else if(req.body.checkbox_44)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].img4;
                  }

                   if(req.body.checkbox_51)
                  {
                    user.writeArticle[i].img5=user.writeArticle[i].artistImagePage5[0].img;
                  }
                  else if(req.body.checkbox_52)
                  {
                    user.writeArticle[i].img5=user.writeArticle[i].artistImagePage5[1].img;
                  }
                  else if(req.body.checkbox_53)
                  {
                    user.writeArticle[i].img5=user.writeArticle[i].artistImagePage5[2].img;
                  }
                  else if(req.body.checkbox_54)
                  {
                    user.writeArticle[i].img5=user.writeArticle[i].img5;
                  }

                   if(req.body.checkbox_61)
                  {
                    user.writeArticle[i].img6=user.writeArticle[i].artistImagePage6[0].img;
                  }
                  else if(req.body.checkbox_62)
                  {
                    user.writeArticle[i].img6=user.writeArticle[i].artistImagePage6[1].img;
                  }
                  else if(req.body.checkbox_63)
                  {
                    user.writeArticle[i].img6=user.writeArticle[i].artistImagePage6[2].img;
                  }

                  else if(req.body.checkbox_64)
                  {
                    user.writeArticle[i].img6=user.writeArticle[i].img6;
                  }

                   if(req.body.checkbox_71)
                  {
                    user.writeArticle[i].img7=user.writeArticle[i].artistImagePage7[0].img;
                  }
                  else if(req.body.checkbox_72)
                  {
                    user.writeArticle[i].img7=user.writeArticle[i].artistImagePage7[1].img;
                  }
                  else if(req.body.checkbox_73)
                  {
                    user.writeArticle[i].img7=user.writeArticle[i].artistImagePage7[2].img;
                  }
                  else if(req.body.checkbox_74)
                  {
                    user.writeArticle[i].img7=user.writeArticle[i].img7;
                  }

                   if(req.body.checkbox_81)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].artistImagePage8[0].img;
                  }
                  else if(req.body.checkbox_82)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].artistImagePage8[1].img;
                  }
                  else if(req.body.checkbox_83)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].artistImagePage8[2].img;
                  }
                  else if(req.body.checkbox_84)
                  {
                    user.writeArticle[i].img4=user.writeArticle[i].img4;
                  }

                   if(req.body.checkbox_91)
                  {
                    user.writeArticle[i].img9=user.writeArticle[i].artistImagePage9[0].img;
                  }
                  else if(req.body.checkbox_92)
                  {
                    user.writeArticle[i].img9=user.writeArticle[i].artistImagePage9[1].img;
                  }
                  else if(req.body.checkbox_93)
                  {
                    user.writeArticle[i].img9=user.writeArticle[i].artistImagePage9[2].img;
                  }
                  else if(req.body.checkbox_94)
                  {
                    user.writeArticle[i].img9=user.writeArticle[i].img9;
                  }

                   if(req.body.checkbox_101)
                  {
                    user.writeArticle[i].img10=user.writeArticle[i].artistImagePage10[0].img;
                  }
                  else if(req.body.checkbox_102)
                  {
                    user.writeArticle[i].img10=user.writeArticle[i].artistImagePage10[1].img;
                  }
                  else if(req.body.checkbox_103)
                  {
                    user.writeArticle[i].img10=user.writeArticle[i].artistImagePage10[2].img;
                  }
                  else if(req.body.checkbox_104)
                  {
                    user.writeArticle[i].img10=user.writeArticle[i].img10;
                  }

                   if(req.body.checkbox_111)
                  {
                    user.writeArticle[i].img11=user.writeArticle[i].artistImagePage11[0].img;
                  }
                  else if(req.body.checkbox_112)
                  {
                    user.writeArticle[i].img11=user.writeArticle[i].artistImagePage11[1].img;
                  }
                  else if(req.body.checkbox_113)
                  {
                    user.writeArticle[i].img11=user.writeArticle[i].artistImagePage11[2].img;
                  }
                  else if(req.body.checkbox_114)
                  {
                    user.writeArticle[i].img11=user.writeArticle[i].img11;
                  }

                   if(req.body.checkbox_121)
                  {
                    user.writeArticle[i].img12=user.writeArticle[i].artistImagePage12[0].img;
                  }
                  else if(req.body.checkbox_122)
                  {
                    user.writeArticle[i].img12=user.writeArticle[i].artistImagePage12[1].img;
                  }
                  else if(req.body.checkbox_123)
                  {
                    user.writeArticle[i].img12=user.writeArticle[i].artistImagePage12[2].img;
                  }
                  else if(req.body.checkbox_124)
                  {
                    user.writeArticle[i].img12=user.writeArticle[i].img12;
                  }

                   if(req.body.checkbox_131)
                  {
                    user.writeArticle[i].img13=user.writeArticle[i].artistImagePage13[0].img;
                  }
                  else if(req.body.checkbox_132)
                  {
                    user.writeArticle[i].img13=user.writeArticle[i].artistImagePage13[1].img;
                  }
                  else if(req.body.checkbox_133)
                  {
                    user.writeArticle[i].img13=user.writeArticle[i].artistImagePage13[2].img;
                  }
                  else if(req.body.checkbox_134)
                  {
                    user.writeArticle[i].img13=user.writeArticle[i].img13;
                  }

                   if(req.body.checkbox_141)
                  {
                    user.writeArticle[i].img14=user.writeArticle[i].artistImagePage14[0].img;
                  }
                  else if(req.body.checkbox_142)
                  {
                    user.writeArticle[i].img14=user.writeArticle[i].artistImagePage14[1].img;
                  }
                  else if(req.body.checkbox_143)
                  {
                    user.writeArticle[i].img14=user.writeArticle[i].artistImagePage14[2].img;
                  }
                  else if(req.body.checkbox_144)
                  {
                    user.writeArticle[i].img14=user.writeArticle[i].img14;
                  }

                   if(req.body.checkbox_151)
                  {
                    user.writeArticle[i].img15=user.writeArticle[i].artistImagePage15[0].img;
                  }
                  else if(req.body.checkbox_152)
                  {
                    user.writeArticle[i].img15=user.writeArticle[i].artistImagePage15[1].img;
                  }
                  else if(req.body.checkbox_153)
                  {
                    user.writeArticle[i].img15=user.writeArticle[i].artistImagePage15[2].img;
                  }
                  else if(req.body.checkbox_154)
                  {
                    user.writeArticle[i].img15=user.writeArticle[i].img15;
                  }
                   if(req.body.checkbox_161)
                  {
                    user.writeArticle[i].img16=user.writeArticle[i].artistImagePage16[0].img;
                  }
                  else if(req.body.checkbox_162)
                  {
                    user.writeArticle[i].img16=user.writeArticle[i].artistImagePage16[1].img;
                  }
                  else if(req.body.checkbox_163)
                  {
                    user.writeArticle[i].img16=user.writeArticle[i].artistImagePage16[2].img;
                  }
                  else if(req.body.checkbox_164)
                  {
                    user.writeArticle[i].img16=user.writeArticle[i].img16;
                    //selectedartistImagePage16
                  }
              }
          }
          user.save(function(err){
              if(err)
                console.log(err);
              else
                res.redirect('/uploadFiles');
          })
      }
    })
}
exports.selectArtWork=function(req,res){

  // var userId=req.param('author');
  // var articleId=req.param('article');

  var articleId=req.param('article');
  var author=req.param('author');
  var article;
  User.findOne({_id:author}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==articleId)
            {
              article=user.writeArticle[i];
              res.render('selectArtWork',{
                article:article,
                author:author
              })
            }
        }
    }
  })
}
exports.editcontentsubmit=function(req,res){
   console.log(req.body);
  // console.log("********************");
  // console.log(req.files);
  var author=req.body.author;
  var articleId=req.body.articleId;
  var article;
  User.findOne({_id:author}).exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==articleId)
            {
              article=user.writeArticle[i];
              if(req.body.titleofBook&&req.body.titleofBook!='')
                     user.writeArticle[i].bookTitle=req.body.titleofBook;

              
              if(req.body.articleTitle&&req.body.articleTitle!='')
                     user.writeArticle[i].title=req.body.articleTitle;
                   
              if(req.body.article&&req.body.article!='')
                     user.writeArticle[i].article=req.body.article;

               if(req.body.source&&req.body.source!='')
                     user.writeArticle[i].credit=req.body.source;

              if(req.body.bottomText&&req.body.bottomText!='')
                     user.writeArticle[i].bottomText=req.body.bottomText;

               
              if(req.body.articleTitle2&&req.body.articleTitle2!='')
                     user.writeArticle[i].articleTitle2=req.body.articleTitle2;  
              
              if(req.body.article2&&req.body.article2!='')
                     user.writeArticle[i].article2=req.body.article2; 

              if(req.body.source2&&req.body.source2!='')
                     user.writeArticle[i].source2=req.body.source2; 

              if(req.body.bottomText2&&req.body.bottomText2!='')
                     user.writeArticle[i].bottomText2=req.body.bottomText2;

              if(req.body.articleTitle3&&req.body.articleTitle3!='')
                     user.writeArticle[i].articleTitle3=req.body.articleTitle3;  
              
              if(req.body.article3&&req.body.article3!='')
                     user.writeArticle[i].article3=req.body.article3; 

              if(req.body.source3&&req.body.source3!='')
                     user.writeArticle[i].source3=req.body.source3; 

              if(req.body.bottomText3&&req.body.bottomText3!='')
                     user.writeArticle[i].bottomText3=req.body.bottomText3;


              if(req.body.articleTitle4&&req.body.articleTitle4!='')
                     user.writeArticle[i].articleTitle4=req.body.articleTitle4;  
              
              if(req.body.article4&&req.body.article4!='')
                     user.writeArticle[i].article4=req.body.article4; 

              if(req.body.source4&&req.body.source4!='')
                     user.writeArticle[i].source4=req.body.source4; 

              if(req.body.bottomText4&&req.body.bottomText4!='')
                     user.writeArticle[i].bottomText4=req.body.bottomText4;

              if(req.body.articleTitle5&&req.body.articleTitle5!='')
                     user.writeArticle[i].articleTitle5=req.body.articleTitle5;  
              
              if(req.body.article5&&req.body.article5!='')
                     user.writeArticle[i].article5=req.body.article5; 

              if(req.body.source5&&req.body.source5!='')
                     user.writeArticle[i].source5=req.body.source5; 

              if(req.body.bottomText5&&req.body.bottomText5!='')
                     user.writeArticle[i].bottomText5=req.body.bottomText5;


              if(req.body.articleTitle6&&req.body.articleTitle6!='')
                     user.writeArticle[i].articleTitle6=req.body.articleTitle6;  
              
              if(req.body.article6&&req.body.article6!='')
                     user.writeArticle[i].article6=req.body.article6; 

              if(req.body.source6&&req.body.source6!='')
                     user.writeArticle[i].source6=req.body.source6; 

              if(req.body.bottomText6&&req.body.bottomText6!='')
                     user.writeArticle[i].bottomText6=req.body.bottomText6;


              if(req.body.articleTitle7&&req.body.articleTitle7!='')
                     user.writeArticle[i].articleTitle7=req.body.articleTitle7;  
              
              if(req.body.article7&&req.body.article7!='')
                     user.writeArticle[i].article7=req.body.article7; 

              if(req.body.source7&&req.body.source7!='')
                     user.writeArticle[i].source7=req.body.source7; 

              if(req.body.bottomText7&&req.body.bottomText7!='')
                     user.writeArticle[i].bottomText7=req.body.bottomText7;


              if(req.body.articleTitle8&&req.body.articleTitle8!='')
                     user.writeArticle[i].articleTitle8=req.body.articleTitle8;  
              
              if(req.body.article8&&req.body.article8!='')
                     user.writeArticle[i].article8=req.body.article8; 

              if(req.body.source8&&req.body.source8!='')
                     user.writeArticle[i].source8=req.body.source8; 

              if(req.body.bottomText8&&req.body.bottomText8!='')
                     user.writeArticle[i].bottomText8=req.body.bottomText8;


              if(req.body.articleTitle9&&req.body.articleTitle9!='')
                     user.writeArticle[i].articleTitle9=req.body.articleTitle9;  
              
              if(req.body.article9&&req.body.article9!='')
                     user.writeArticle[i].article9=req.body.article9; 

              if(req.body.source9&&req.body.source9!='')
                     user.writeArticle[i].source9=req.body.source9; 

              if(req.body.bottomText9&&req.body.bottomText9!='')
                     user.writeArticle[i].bottomText9=req.body.bottomText9;


              if(req.body.articleTitle10&&req.body.articleTitle10!='')
                     user.writeArticle[i].articleTitle10=req.body.articleTitle10;  
              
              if(req.body.article10&&req.body.article10!='')
                     user.writeArticle[i].article10=req.body.article10; 

              if(req.body.source10&&req.body.source10!='')
                     user.writeArticle[i].source10=req.body.source10; 

              if(req.body.bottomText10&&req.body.bottomText10!='')
                     user.writeArticle[i].bottomText10=req.body.bottomText10;

              if(req.body.articleTitle11&&req.body.articleTitle11!='')
                     user.writeArticle[i].articleTitle11=req.body.articleTitle11;  
              
              if(req.body.article11&&req.body.article11!='')
                     user.writeArticle[i].article11=req.body.article11; 

              if(req.body.source11&&req.body.source11!='')
                     user.writeArticle[i].source11=req.body.source11; 

              if(req.body.bottomText11&&req.body.bottomText11!='')
                     user.writeArticle[i].bottomText11=req.body.bottomText11;


              if(req.body.articleTitle12&&req.body.articleTitle12!='')
                     user.writeArticle[i].articleTitle12=req.body.articleTitle12;  
              
              if(req.body.article12&&req.body.article12!='')
                     user.writeArticle[i].article12=req.body.article12; 

              if(req.body.source12&&req.body.source12!='')
                     user.writeArticle[i].source12=req.body.source12; 

              if(req.body.bottomText12&&req.body.bottomText12!='')
                     user.writeArticle[i].bottomText12=req.body.bottomText12;

              if(req.body.articleTitle13&&req.body.articleTitle13!='')
                     user.writeArticle[i].articleTitle13=req.body.articleTitle13;  
              
              if(req.body.article13&&req.body.article13!='')
                     user.writeArticle[i].article13=req.body.article13; 

              if(req.body.source13&&req.body.source13!='')
                     user.writeArticle[i].source13=req.body.source13; 

              if(req.body.bottomText13&&req.body.bottomText13!='')
                     user.writeArticle[i].bottomText13=req.body.bottomText13;

              if(req.body.articleTitle14&&req.body.articleTitle14!='')
                     user.writeArticle[i].articleTitle14=req.body.articleTitle14;  
              
              if(req.body.article14&&req.body.article14!='')
                     user.writeArticle[i].article14=req.body.article14; 

              if(req.body.source14&&req.body.source14!='')
                     user.writeArticle[i].source14=req.body.source14; 

              if(req.body.bottomText14&&req.body.bottomText14!='')
                     user.writeArticle[i].bottomText14=req.body.bottomText14;


              if(req.body.articleTitle15&&req.body.articleTitle15!='')
                     user.writeArticle[i].articleTitle15=req.body.articleTitle15;  
              
              if(req.body.article15&&req.body.article15!='')
                     user.writeArticle[i].article15=req.body.article15; 

              if(req.body.source15&&req.body.source15!='')
                     user.writeArticle[i].source15=req.body.source15; 

              if(req.body.bottomText15&&req.body.bottomText15!='')
                     user.writeArticle[i].bottomText15=req.body.bottomText15;


              if(req.body.articleTitle16&&req.body.articleTitle16!='')
                     user.writeArticle[i].articleTitle16=req.body.articleTitle16;  
              
              if(req.body.article16&&req.body.article16!='')
                     user.writeArticle[i].article16=req.body.article16; 

              if(req.body.source16&&req.body.source16!='')
                     user.writeArticle[i].source16=req.body.source16; 

              if(req.body.bottomText16&&req.body.bottomText16!='')
                     user.writeArticle[i].bottomText16=req.body.bottomText16;
              user.writeArticle[i].editcontent='Done';

               if(req.files.img.name != "")
                {
                    fs.readFile(req.files.img.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].articleImage='/article_image/'+req.files.img.name;
                }
                if(req.files.img2.name != "")
                {
                    fs.readFile(req.files.img2.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img2.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img2='/article_image/'+req.files.img2.name;
                }

                if(req.files.img3.name != "")
                {
                    fs.readFile(req.files.img3.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img3.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img3='/article_image/'+req.files.img3.name;
                }
                if(req.files.img4.name != "")
                {
                    fs.readFile(req.files.img4.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img4.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img4='/article_image/'+req.files.img4.name;
                }
                if(req.files.img5&&req.files.img5.name != "")
                {
                    fs.readFile(req.files.img5.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img5.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img5='/article_image/'+req.files.img5.name;
                }
                if(req.files.img6&&req.files.img6.name != "")
                {
                    fs.readFile(req.files.img6.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img6.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img6='/article_image/'+req.files.img6.name;
                }
                if(req.files.img7&&req.files.img7.name != "")
                {
                    fs.readFile(req.files.img7.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img7.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img7='/article_image/'+req.files.img7.name;
                }

                if(req.files.img8&&req.files.img8.name != "")
                {
                    fs.readFile(req.files.img8.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img8.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img8='/article_image/'+req.files.img8.name;
                }

                if(req.files.img9&&req.files.img9.name != "")
                {
                    fs.readFile(req.files.img9.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img9.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img9='/article_image/'+req.files.img9.name;
                }

                if(req.files.img10&&req.files.img10.name != "")
                {
                    fs.readFile(req.files.img10.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img10.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img10='/article_image/'+req.files.img10.name;
                }
                if(req.files.img11&&req.files.img11.name != "")
                {
                    fs.readFile(req.files.img11.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img11.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img11='/article_image/'+req.files.img11.name;
                }
                if(req.files.img12&&req.files.img12.name != "")
                {
                    fs.readFile(req.files.img12.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img12.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img12='/article_image/'+req.files.img12.name;
                }
                if(req.files.img13&&req.files.img13.name != "")
                {
                    fs.readFile(req.files.img13.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img13.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img13='/article_image/'+req.files.img13.name;
                }
                if(req.files.img14&&req.files.img14.name != "")
                {
                    fs.readFile(req.files.img14.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img14.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img14='/article_image/'+req.files.img14.name;
                }
                if(req.files.img15&&req.files.img15.name != "")
                {
                    fs.readFile(req.files.img15.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img15.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img15='/article_image/'+req.files.img15.name;
                }
                if(req.files.img16&&req.files.img16.name != "")
                {
                    fs.readFile(req.files.img16.path, function (err, data) {
                      
                        fs.writeFile('public/article_image/'+req.files.img16.name,data, function (err) {
                          if (err) 
                            console.log('Error');
                            //throw err; 
                          else
                            console.log('Done success');
                        });
                      });
                    
                     user.writeArticle[i].img16='/article_image/'+req.files.img16.name;
                }

              user.save(function(err){
                        if(err)
                          console.log(err);
                          else
                          {
                               User.findOne({userType:'admin'}).exec(function(err,admin){
                                if(err)
                                  console.log(err);
                                else
                                { 
                                    admin.editedArticle.push(article);
                                    admin.save(function(err){
                                      if(err)
                                        console.log(err);
                                      else{
                                        res.redirect('/uploadFiles');
                                      }
                                    })

                                }
                              })
                          }
                      })    
             
            }
        }
    }
  })


}
exports.selectionSubmit=function(req,res){
    console.log(req.body);
    // var cc='checkbox_'+1+1;
    // console.log(req.body.cc);
    User.findOne({_id:req.body.userId}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==req.body.articleID)
              {   user.writeArticle[i].status='Request Authors Approval';
                  if(req.body.checkbox_11)
                  {
                    user.writeArticle[i].selectedartistImagePage1=user.writeArticle[i].artistImagePage1[0].img;
                    user.writeArticle[i].selectedartistPage1=user.writeArticle[i].artistImagePage1[0].artist;
                  }
                  else if(req.body.checkbox_12)
                  {
                    user.writeArticle[i].selectedartistImagePage1=user.writeArticle[i].artistImagePage1[1].img;
                    user.writeArticle[i].selectedartistPage1=user.writeArticle[i].artistImagePage1[1].artist;
                  }
                  else if(req.body.checkbox_13)
                  {
                    user.writeArticle[i].selectedartistImagePage1=user.writeArticle[i].artistImagePage1[2].img;
                    user.writeArticle[i].selectedartistPage1=user.writeArticle[i].artistImagePage1[2].artist;
                  }

                   if(req.body.checkbox_21)
                  {
                    user.writeArticle[i].selectedartistImagePage2=user.writeArticle[i].artistImagePage2[0].img;
                    user.writeArticle[i].selectedartistPage2=user.writeArticle[i].artistImagePage2[0].artist;
                  }
                  else if(req.body.checkbox_22)
                  {
                    user.writeArticle[i].selectedartistImagePage2=user.writeArticle[i].artistImagePage2[1].img;
                    user.writeArticle[i].selectedartistPage2=user.writeArticle[i].artistImagePage2[1].artist;
                  }
                  else if(req.body.checkbox_23)
                  {
                    user.writeArticle[i].selectedartistImagePage2=user.writeArticle[i].artistImagePage2[2].img;
                    user.writeArticle[i].selectedartistPage2=user.writeArticle[i].artistImagePage2[2].artist;
                  }
                   if(req.body.checkbox_31)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].artistImagePage3[0].img;
                    user.writeArticle[i].selectedartistPage3=user.writeArticle[i].artistImagePage3[0].artist;
                  }
                  else if(req.body.checkbox_32)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].artistImagePage3[1].img;
                    user.writeArticle[i].selectedartistPage3=user.writeArticle[i].artistImagePage3[1].artist;
                  }
                  else if(req.body.checkbox_33)
                  {
                    user.writeArticle[i].selectedartistImagePage3=user.writeArticle[i].artistImagePage3[2].img;
                    user.writeArticle[i].selectedartistPage3=user.writeArticle[i].artistImagePage3[2].artist;
                  }

                   if(req.body.checkbox_41)
                  {
                    user.writeArticle[i].selectedartistImagePage4=user.writeArticle[i].artistImagePage4[0].img;
                    user.writeArticle[i].selectedartistPage4=user.writeArticle[i].artistImagePage4[0].artist;
                  }
                  else if(req.body.checkbox_42)
                  {
                    user.writeArticle[i].selectedartistImagePage4=user.writeArticle[i].artistImagePage4[1].img;
                    user.writeArticle[i].selectedartistPage4=user.writeArticle[i].artistImagePage4[1].artist;
                  }
                  else if(req.body.checkbox_43)
                  {
                    user.writeArticle[i].selectedartistImagePage4=user.writeArticle[i].artistImagePage4[2].img;
                    user.writeArticle[i].selectedartistPage4=user.writeArticle[i].artistImagePage4[2].artist;
                  }

                   if(req.body.checkbox_51)
                  {
                    user.writeArticle[i].selectedartistImagePage5=user.writeArticle[i].artistImagePage5[0].img;
                    user.writeArticle[i].selectedartistPage5=user.writeArticle[i].artistImagePage5[0].artist;
                  }
                  else if(req.body.checkbox_52)
                  {
                    user.writeArticle[i].selectedartistImagePage5=user.writeArticle[i].artistImagePage5[1].img;
                    user.writeArticle[i].selectedartistPage5=user.writeArticle[i].artistImagePage5[1].artist;
                  }
                  else if(req.body.checkbox_53)
                  {
                    user.writeArticle[i].selectedartistImagePage5=user.writeArticle[i].artistImagePage5[2].img;
                    user.writeArticle[i].selectedartistPage5=user.writeArticle[i].artistImagePage5[2].artist;
                  }

                   if(req.body.checkbox_61)
                  {
                    user.writeArticle[i].selectedartistImagePage6=user.writeArticle[i].artistImagePage6[0].img;
                    user.writeArticle[i].selectedartistPage6=user.writeArticle[i].artistImagePage6[0].artist;
                  }
                  else if(req.body.checkbox_62)
                  {
                    user.writeArticle[i].selectedartistImagePage6=user.writeArticle[i].artistImagePage6[1].img;
                    user.writeArticle[i].selectedartistPage6=user.writeArticle[i].artistImagePage6[1].artist;
                  }
                  else if(req.body.checkbox_63)
                  {
                    user.writeArticle[i].selectedartistImagePage6=user.writeArticle[i].artistImagePage6[2].img;
                    user.writeArticle[i].selectedartistPage6=user.writeArticle[i].artistImagePage6[2].artist;
                  }

                   if(req.body.checkbox_71)
                  {
                    user.writeArticle[i].selectedartistImagePage7=user.writeArticle[i].artistImagePage7[0].img;
                    user.writeArticle[i].selectedartistPage7=user.writeArticle[i].artistImagePage7[0].artist;
                  }
                  else if(req.body.checkbox_72)
                  {
                    user.writeArticle[i].selectedartistImagePage7=user.writeArticle[i].artistImagePage7[1].img;
                    user.writeArticle[i].selectedartistPage7=user.writeArticle[i].artistImagePage7[1].artist;
                  }
                  else if(req.body.checkbox_73)
                  {
                    user.writeArticle[i].selectedartistImagePage7=user.writeArticle[i].artistImagePage7[2].img;
                    user.writeArticle[i].selectedartistPage7=user.writeArticle[i].artistImagePage7[2].artist;
                  }

                   if(req.body.checkbox_81)
                  {
                    user.writeArticle[i].selectedartistImagePage8=user.writeArticle[i].artistImagePage8[0].img;
                    user.writeArticle[i].selectedartistPage8=user.writeArticle[i].artistImagePage8[0].artist;
                  }
                  else if(req.body.checkbox_82)
                  {
                    user.writeArticle[i].selectedartistImagePage8=user.writeArticle[i].artistImagePage8[1].img;
                    user.writeArticle[i].selectedartistPage8=user.writeArticle[i].artistImagePage8[1].artist;
                  }
                  else if(req.body.checkbox_83)
                  {
                    user.writeArticle[i].selectedartistImagePage8=user.writeArticle[i].artistImagePage8[2].img;
                    user.writeArticle[i].selectedartistPage8=user.writeArticle[i].artistImagePage8[2].artist;
                  }

                   if(req.body.checkbox_91)
                  {
                    user.writeArticle[i].selectedartistImagePage9=user.writeArticle[i].artistImagePage9[0].img;
                    user.writeArticle[i].selectedartistPage9=user.writeArticle[i].artistImagePage9[0].artist;
                  }
                  else if(req.body.checkbox_92)
                  {
                    user.writeArticle[i].selectedartistImagePage9=user.writeArticle[i].artistImagePage9[1].img;
                    user.writeArticle[i].selectedartistPage9=user.writeArticle[i].artistImagePage9[1].artist;
                  }
                  else if(req.body.checkbox_93)
                  {
                    user.writeArticle[i].selectedartistImagePage9=user.writeArticle[i].artistImagePage9[2].img;
                    user.writeArticle[i].selectedartistPage9=user.writeArticle[i].artistImagePage9[2].artist;
                  }

                   if(req.body.checkbox_101)
                  {
                    user.writeArticle[i].selectedartistImagePage10=user.writeArticle[i].artistImagePage10[0].img;
                    user.writeArticle[i].selectedartistPage10=user.writeArticle[i].artistImagePage10[0].artist;
                  }
                  else if(req.body.checkbox_102)
                  {
                    user.writeArticle[i].selectedartistImagePage10=user.writeArticle[i].artistImagePage10[1].img;
                    user.writeArticle[i].selectedartistPage10=user.writeArticle[i].artistImagePage10[1].artist;
                  }
                  else if(req.body.checkbox_103)
                  {
                    user.writeArticle[i].selectedartistImagePage10=user.writeArticle[i].artistImagePage10[2].img;
                    user.writeArticle[i].selectedartistPage10=user.writeArticle[i].artistImagePage10[2].artist;
                  }

                   if(req.body.checkbox_111)
                  {
                    user.writeArticle[i].selectedartistImagePage11=user.writeArticle[i].artistImagePage11[0].img;
                    user.writeArticle[i].selectedartistPage11=user.writeArticle[i].artistImagePage11[0].artist;
                  }
                  else if(req.body.checkbox_112)
                  {
                    user.writeArticle[i].selectedartistImagePage11=user.writeArticle[i].artistImagePage11[1].img;
                    user.writeArticle[i].selectedartistPage11=user.writeArticle[i].artistImagePage11[1].artist;
                  }
                  else if(req.body.checkbox_113)
                  {
                    user.writeArticle[i].selectedartistImagePage11=user.writeArticle[i].artistImagePage11[2].img;
                    user.writeArticle[i].selectedartistPage11=user.writeArticle[i].artistImagePage11[2].artist;
                  }

                   if(req.body.checkbox_121)
                  {
                    user.writeArticle[i].selectedartistImagePage12=user.writeArticle[i].artistImagePage12[0].img;
                    user.writeArticle[i].selectedartistPage12=user.writeArticle[i].artistImagePage12[0].artist;
                  }
                  else if(req.body.checkbox_122)
                  {
                    user.writeArticle[i].selectedartistImagePage12=user.writeArticle[i].artistImagePage12[1].img;
                    user.writeArticle[i].selectedartistPage12=user.writeArticle[i].artistImagePage12[1].artist;
                  }
                  else if(req.body.checkbox_123)
                  {
                    user.writeArticle[i].selectedartistImagePage12=user.writeArticle[i].artistImagePage12[2].img;
                    user.writeArticle[i].selectedartistPage12=user.writeArticle[i].artistImagePage12[2].artist;
                  }

                   if(req.body.checkbox_131)
                  {
                    user.writeArticle[i].selectedartistImagePage13=user.writeArticle[i].artistImagePage13[0].img;
                    user.writeArticle[i].selectedartistPage13=user.writeArticle[i].artistImagePage13[0].artist;
                  }
                  else if(req.body.checkbox_132)
                  {
                    user.writeArticle[i].selectedartistImagePage13=user.writeArticle[i].artistImagePage13[1].img;
                    user.writeArticle[i].selectedartistPage13=user.writeArticle[i].artistImagePage13[1].artist;
                  }
                  else if(req.body.checkbox_133)
                  {
                    user.writeArticle[i].selectedartistImagePage13=user.writeArticle[i].artistImagePage13[2].img;
                    user.writeArticle[i].selectedartistPage13=user.writeArticle[i].artistImagePage13[2].artist;
                  }

                   if(req.body.checkbox_141)
                  {
                    user.writeArticle[i].selectedartistImagePage14=user.writeArticle[i].artistImagePage14[0].img;
                    user.writeArticle[i].selectedartistPage14=user.writeArticle[i].artistImagePage14[0].artist;
                  }
                  else if(req.body.checkbox_142)
                  {
                    user.writeArticle[i].selectedartistImagePage14=user.writeArticle[i].artistImagePage14[1].img;
                    user.writeArticle[i].selectedartistPage14=user.writeArticle[i].artistImagePage14[1].artist;
                  }
                  else if(req.body.checkbox_143)
                  {
                    user.writeArticle[i].selectedartistImagePage14=user.writeArticle[i].artistImagePage14[2].img;
                    user.writeArticle[i].selectedartistPage14=user.writeArticle[i].artistImagePage14[2].artist;
                  }

                   if(req.body.checkbox_151)
                  {
                    user.writeArticle[i].selectedartistImagePage15=user.writeArticle[i].artistImagePage15[0].img;
                    user.writeArticle[i].selectedartistPage15=user.writeArticle[i].artistImagePage15[0].artist;
                  }
                  else if(req.body.checkbox_152)
                  {
                    user.writeArticle[i].selectedartistImagePage15=user.writeArticle[i].artistImagePage15[1].img;
                    user.writeArticle[i].selectedartistPage15=user.writeArticle[i].artistImagePage15[1].artist;
                  }
                  else if(req.body.checkbox_153)
                  {
                    user.writeArticle[i].selectedartistImagePage15=user.writeArticle[i].artistImagePage15[2].img;
                    user.writeArticle[i].selectedartistPage15=user.writeArticle[i].artistImagePage15[2].artist;
                  }
                   if(req.body.checkbox_161)
                  {
                    user.writeArticle[i].selectedartistImagePage16=user.writeArticle[i].artistImagePage16[0].img;
                    user.writeArticle[i].selectedartistPage1=user.writeArticle[i].artistImagePage16[0].artist;
                  }
                  else if(req.body.checkbox_162)
                  {
                    user.writeArticle[i].selectedartistImagePage16=user.writeArticle[i].artistImagePage16[1].img;
                    user.writeArticle[i].selectedartistPage16=user.writeArticle[i].artistImagePage16[1].artist;
                  }
                  else if(req.body.checkbox_163)
                  {
                    user.writeArticle[i].selectedartistImagePage16=user.writeArticle[i].artistImagePage16[2].img;
                    user.writeArticle[i].selectedartistPage16=user.writeArticle[i].artistImagePage16[2].artist;
                  }
              }
          }
          user.save(function(err){
              if(err)
                console.log(err);
              else
                res.redirect('/requestartist');
          })
      }
    })
    // for(var i=1;i<=16;i++)
    // {
    //     for( var j=1;j<=3;j++)
    //     {
    //       console.log(req.body.checkbox_ij);
    //        // if(req.body.checkbox_+i+j)
    //     }
    // }


}
exports.viewChangedarticle=function(req,res)
{
    var userId=req.param('id');
    var articleId=req.param('article');
    console.log('articleId')
    console.log(userId)
    var author;
    var article;
    User.findOne({_id:userId}).exec(function(err,user){
      if(err)
        console.log('eddds '+err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==articleId)
              {
                  article=user.writeArticle[i];
                  author=user;
              }
          }
          console.log("dcjs");
          console.log(article)
          res.render('viewChangedarticle',{
            article:article,
            author:author,
          })
      }
    })
}
exports.viewarticle=function(req,res){
  
 // console.log('Server view:');
  
  console.log(req.param('id'));
  console.log(req.param('articleid'));

  var userId=req.param('id');
  var articleId=req.param('articleid');

  var article;
  count=0;
    User.find().exec(function(err,user)
    {
        
        if(err)
          console.log('Error');
        else
        {
            
            console.log(user.length);
            for (var i=0;i<user.length;i++)
            {

                if(user[i]._id == userId)
                {


                  for(var k=0;k<user[i].writeArticle.length;k++)
                  {

                      if(user[i].writeArticle[k]._id == articleId)
                      {
                        console.log(user[i].writeArticle[k].title);
                        article=user[i].writeArticle[k];

                        console.log(article);

                      }
                  }     
                }

            }
            res.render('viewarticle',{


                article:article,


             });

        }


    });


}

exports.activation=function(req,res)
{
    console.log(req.params.user_id);
    userId=req.params.user_id;

    User.findOne({_id:userId}).exec(function(err,user){
        if(err)
            console.log(err);
        else
            {
                console.log(user.email);
                user.activation='Active';
                user.save(function(err){

                    if(err)
                      console.log(err);
                    else
                      res.redirect('/');
                })
            }

    })
}
exports.changeUserPassword=function(req,res)
{   console.log("user reset"+req.param('resetpasswordId'));
    console.log(req.param('user_id'));
    userId=req.param('user_id');
    var resetpass=req.param('resetpasswordId');
    User.findOne({_id:userId}).exec(function(err,user){
      if(err)
        console.log(err);
      else{
        if(user.resetPassId==resetpass){
          
          res.redirect('/recoverPassword/'+userId);
        }
        else{
          res.redirect('/signin#resetpasswordError');
        }
      }

    })
    

    // User.findOne({_id:userId}).exec(function(err,user){
    //     if(err){
    //       console.log("got err for changeUserPassword")
    //       console.log(err);
    //     }
            
    //     else
    //         {
    //                   // res.redirect('/');
                      // console.log(user.email);
                      //  user.setPassword(req.body.modal_password);
                      // user.save(function(err){

                      //     if(err)
                      //       console.log(err);
                      //     else
                      //       res.redirect('/');
                      // })
    //         }

    // })
}
exports.recoverPassword=function(req,res){
  console.log("id: recover "+req.params.userId);
    //user_Id=req.params.userId;
   res.render('recoverPassword',
    {
      userid:req.params.userId
    });
}
exports.resetpassword=function(req,res){
  // console.log("id: resetpassword: "+req.body.user_pass_id);
    userId=req.body.user_pass_id;
    User.findOne({_id:userId}).exec(function(err,user){
      if(err){
        console.log(err);
      }
      else if(user){
        console.log(user._id);
        user.resetPassId='';
          
        user.setPassword(req.body.password);
        user.save(function(err){

            if(err)
              console.log(err);
            else
              res.redirect('/signin#resetpassword');
        })

      }

    })
}
exports.registration=function(req,res){
  console.log('Registration:');
  // var user = new User();
  // user.name=req.body.name;
  // user.email=req.body.email;
  // var pass=req.body.password;
  // user.activation='Inactive';
  User.findOne({email:req.body.email}).exec(function(err,user){
    if(err){
      console.log(err);
    }
    else if(user){
     // alert('This email address have been used already!!!');
      res.redirect('/signup#signupError');
        //res.send('This email address has been already used!!');
       //return;

    }
    else if(!user){
       console.log("come here");
      // console.log(pass);
       var user = new User();
      user.name=req.body.name;
      user.email=req.body.email;
      user.activation='Inactive';
      user.setPassword(req.body.password);
      user.userType='Author';
      user.save(function(err){
            if(err){

              console.log(err);
            }
              
            else{
                      mandrill('/messages/send', {


                          "message": {
                              to: [{email: req.body.email}],
                              from_email: 'admin@patarboi.ca',
                              subject: "Registration",
                              "html":  "Your Registration has been successfull. Use this link to activate account : 162.243.133.214/activation/"+user._id ,
                          },
                         
                        
                      }, function(error, response)
                      {

                          if (error) {
                             console.log("got err");
                              console.log( JSON.stringify(error) );

                          }
                           
                          else 
                            {
                                console.log(response);
                                res.redirect('/signin');
                            }
                      });


            }
              
      });

    }
  })
    
  
  //user.password=req.body.password;
 
 

} 

exports.changePassword=function(req,res){
  console.log(req.body.modal_email);
  User.findOne({email:req.body.modal_email}).exec(function(err,user){
    if(err){
      
      console.log(err);
    }
    else if(!user){
       res.redirect('/signin#recoverPasswordError');
    }
    else{
      var resetId=password_generator();
      console.log("resetId");
      console.log(resetId);
      user.resetPassId=resetId;
      var id=user._id;
      console.log(id);
      user.save(function(err){

          if(err)
            console.log(err);
          else
           console.log("Done");
      }) 
                              // console.log(req.body.modal_password);
                              // user.setPassword(req.body.modal_password);
                              // user.save(function(err){

                              //     if(err)
                              //       console.log(err);
                              //     else
                              //       res.redirect('/');
                              // }) 
          mandrill('/messages/send',{
            "message":{
              to: [{email: req.body.modal_email}],
              from_email: 'admin@patarboi.ca',
              subject: "changePassword",
              "html":  "You want to change password?.Use this link to change password : 162.243.133.214/changeUserPassword?user_id="+user._id+"&resetpasswordId="+resetId ,
                         
            },
          },function(error,response){
            if (error) 
                console.log( JSON.stringify(error) );

              else 
                {
                    console.log(response);
                    res.redirect('/signin');
                }
          });
        }
  });

}
//   console.log(req.body);
//   User.findOne({_id:req.body.modal_email}).exec(function(err,user){
//       if(err)
//           console.log('Error');
//       else
//       {
//           var id=user._id;
//            mandrill('/messages/send', {


//                       "message": {
                      //     to: [{email: req.body.modal_email}],
                      //     from_email: 'admin@patarboi.ca',
                      //     subject: "changePassword",
                      //     "html":  "Your password has been changed successfully. Use this link to activate account : localhost/changePassword/"+user._id ,
                      // },
                     
                    
//                   }, function(error, response)
//                   {

//                       if (error) 
//                         console.log( JSON.stringify(error) );

//                       else 
//                         {
//                             console.log(response);
//                             res.redirect('/signin');
//                         }
//                   });
// }
 // res.redirect('/userArticle');
  // if(req.user.password==req.body.password)
  // {

  //   req.user.password=req.body.new_password;
  // }
  // req.user.save(function(err){
  //     if(err)
  //       console.log(err);
  //     else
  //     {
          
  //         //console.log(req.user);
          
  //         res.redirect('/userArticle');
  //     }
      
  //   });

 // }



exports.signout = function (req, res) {
  req.logout()
  res.redirect('/')
}

exports.updateProfile=function(req,res){
  console.log(req.body);

  if(req.files.imagefile.name!='user.png'){
  fs.readFile(req.files.imagefile.path, function (err, data) {
  
    fs.writeFile('public/profile_pictures/'+req.files.imagefile.name,data, function (err) {
      if (err) 
        console.log(err);
        //throw err; 
      else
        {
           
            gm('public/profile_pictures/'+req.files.imagefile.name)
            .resize(600, 450, '!')
            .write('public/profile_pictures/'+req.files.imagefile.name, function(err){
              if (err) 
               console.log(err);//return console.dir(arguments)
              else
               console.log('done')
            })
      
    }
    });
});
  req.user.profilePicture='profile_pictures/'+req.files.imagefile.name; 
}
  req.user.age = req.body.age;
  req.user.gender=req.body.gender;
  req.user.country=req.body.country;
  req.user.preferredname=req.body.preferredname;
  req.user.biography=req.body.biography;
  // save image name in database
  
  req.user.save(function(err){
        if(err)
          console.log(err);
        else
        {
            
          res.redirect('/userArticle');
        }
      
    });
}
exports.viewSearchuserProfile=function(req,res){
  var email=req.param('email');
  console.log(email);
  User.findOne({email:email}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
        res.render('viewsearchedUser',{
        user:user,
        currentUser:req.user,
      })
      }
  })
}
exports.profilePicture=function(req,res){

   // console.log('hu');
    fs.readFile(req.files.image.path, function (err, data) {
      
        fs.writeFile('public/profile_pictures/'+req.files.image.name,data, function (err) {
          if (err) 
            console.log('Error');
            //throw err; 
          else
            console.log('Done success');
        });
    });

    req.user.profilePicture='profile_pictures/' +req.files.image.name;
    req.user.save(function(err){
      if(err)
        console.log(err)
      else
      {
        console.log('Save image');
        res.send('Success');
      }
        
    });
}

exports.newArtistImage=function(req,res)
{
    //console.log(req.files.img.name);
      

}

exports.submitted=function(req,res)
{

      res.render('submitted',{

          author:req.user

       });

}

exports.newImage=function(req,res)
{
    console.log('index:'+req.body.index);
    var index=req.body.index;

    if(req.files.img.name != "")
    {
        fs.readFile(req.files.img.path, function (err, data) {
          
            fs.writeFile('public/article_image/'+req.files.img.name,data, function (err) {
              if (err) 
                console.log('Error');
                //throw err; 
              else
                console.log('Done success');
            });
          });

    }

    if(req.files.img2)
    {
        fs.readFile(req.files.img2.path, function (err, data) {
          
            fs.writeFile('public/article_image/'+req.files.img2.name,data, function (err) {
              if (err) 
                console.log('Error');
                //throw err; 
              else
                console.log('Done success');
            });
          });

    }

    if(req.files.img3)
    {
        fs.readFile(req.files.img3.path, function (err, data) {
        
          fs.writeFile('public/article_image/'+req.files.img3.name,data, function (err) {
            if (err) 
              console.log('Error');
              //throw err; 
            else
              console.log('Done success');
          });
        });

    }

    if(req.files.img4)
    {
       fs.readFile(req.files.img4.path, function (err, data) {
        
          fs.writeFile('public/article_image/'+req.files.img4.name,data, function (err) {
            if (err) 
              console.log('Error');
              //throw err; 
            else
              console.log('Done success');
          });
        });

    }



    

    console.log('Bilash');
    console.log(req.user.selectedArtist.length);
      for(var i=0;i<req.user.selectedArtist.length;i++)
      {
            if(req.user.selectedArtist[i]._id == index)
            {

              console.log(req.user.selectedArtist[i].title);
              req.user.selectedArtist[i].status='Finish';

              if(req.files.img.name != "")
                req.user.selectedArtist[i].newimage1=req.files.img.name;
              if(req.files.img2)
                req.user.selectedArtist[i].newimage2=req.files.img2.name;
              if(req.files.img3)
                req.user.selectedArtist[i].newimage3=req.files.img3.name;
              if(req.files.img4)
                req.user.selectedArtist[i].newimage4=req.files.img4.name;

            }

      }


    

    req.user.save(function(err){
      if(err)
        console.log(err)
      else
      {
        console.log('Save image');
      //  res.redirect('/artist');
      }
        
    });



     User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
             {
              for(var j=0;j<user[i].writeArticle.length;j++)
              {
                  if(user[i].writeArticle[j]._id==req.body.index)
                  {

                    for(var k=0;k<user[i].writeArticle[j].article_artist.length;k++)
                    {
                        if(user[i].writeArticle[j].article_artist[k].articleArtistemail==req.user.email)
                        {
                          user[i].writeArticle[j].article_artist[k].status='Bidding complete';
                          user[i].writeArticle[j].status='Bidding complete';
                            user[i].save(function(err){
                              if(err)
                                      console.log('Error');
                                  else
                                     res.redirect('/artist');
                              });
                        }
                    }
                      // user[i].writeArticle[j].status='complete';
                      // user[i].save(function(err){

                      //       if(err)
                      //         console.log('Error');
                      //       else
                      //         {
                      //            res.redirect("/assignartist");
                      //         }
                      //     });

                  }
              }
             }  
        }
      })
  
  //console.log(req.files.img.name);
}
exports.Request_to_bid=function(req,res){
  var artist=[];
  var article;
  newdate=new Date();

  User.find().exec(function(err,user1){
    for(var k=0;k<user1.length;k++)
    {
        for(var l=0;l<user1[k].writeArticle.length;l++)
        {
            if(user1[k].writeArticle[l]._id==req.body.articleId)
            {article=user1[k].writeArticle[l];
               User.find().exec(function(err,user){
                if(err)
                  console.log(err)
                else
                {
                  
                  for(var i=0;i<user.length;i++)
                  {(function(i){
                    for(var j=0;j<user[i].userSubtype.length;j++)
                    {
                         if(user[i].userSubtype[j].subtype=='Artists'||user[i].selectedArtist.length>0)
                         {
                            artist.push(user[i]);  

                               mandrill('/messages/send', {
                                "message": {
                                    to: [{email: user[i].email}],
                                    from_email: 'admin@patarboi.ca',
                                    subject: "Request for Bidding",
                                    "html": "You are requested to bid for a article. Please click the link :162.243.133.214/bid_article?articleId="+req.body.articleId,
                                  },
                                }, function(error, response)
                                {
                                    if (error) 
                                      console.log( JSON.stringify(error) );
                                    else {
                                        user[i].selectedArtist.push(article);
                                        user[i].selectedArtistId.push({id:article._id,startDate:newdate,endDate:req.body.dueDate});
                                        user[i].save(function(err){
                                          if(err)
                                            console.log(err)
                                          else
                                            console.log(response);
                                        })
                                        
                                    }
                                      
                                });

                         }
                          
                    }
                  })(i);
                  }
                }
              })
            }
        }
    }
  })
            
 //  if(req.body.artist1){
   // mandrill('/messages/send', {
   //          "message": {
   //              to: [{email: req.body.artist1}],
   //              from_email: 'admin@patarboi.ca',
   //              subject: "Request for Bidding",
   //              "html": "You are requested to bid for a article. Please click the link :162.243.133.214/bid_article?articleId="+req.body.articleId,
   //          },
   //      }, function(error, response)
   //      {

   //          if (error) 
   //            console.log( JSON.stringify(error) );

   //          else 
   //            console.log(response);
   //      });
 // }
 // if(req.body.artist2){
 //   mandrill('/messages/send', {


 //            "message": {
 //                to: [{email: req.body.artist2}],
 //                from_email: 'admin@patarboi.ca',
 //                subject: "Request for Bidding",
 //                "html": "You are requested to bid for a article. Please click the link :162.243.133.214/bid_article?articleId="+req.body.articleId,
 //            },
           
          
 //        }, function(error, response)
 //        {

 //            if (error) 
 //              console.log( JSON.stringify(error) );

 //            else 
 //              console.log(response);
 //        });
 // }
 // if(req.body.artist3){
 //   mandrill('/messages/send', {


 //            "message": {
 //                to: [{email: req.body.artist3}],
 //                from_email: 'admin@patarboi.ca',
 //                subject: "Request for Bidding",
 //                "html": "You are requested to bid for a article. Please click the link :162.243.133.214/bid_article?articleId="+req.body.articleId,
 //            },
           
          
 //        }, function(error, response)
 //        {

 //            if (error) 
 //              console.log( JSON.stringify(error) );

 //            else 
 //              console.log(response);
 //        });
 // }
 //      res.send('Done');

}
exports.viewBidding_Article=function(req,res){
      console.log(req.param('articleId'));
      var article;
      var author;
      User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            {
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                    if(user[i].writeArticle[j]._id==req.param('articleId'))
                    {
                        article=user[i].writeArticle[j];
                        author=user[i];

                    }
                }
            }
            res.render('viewBiddingArticle',{
              singleArticle:article,
              articleUser:author
            })
        }
      })
}
exports.bidding=function(req,res){
   console.log('req.body');
  console.log(req.body);
  var check=0;
  var ind;

  User.findOne({_id:req.user._id}).exec(function(err,requser){
    {
      for(var i=0;i<requser.selectedArtist.length;i++)
      {
          if(requser.selectedArtist[i]._id == req.body.articleId)
          {
              requser.selectedArtist[i].status ="Accepted";
              requser.save(function(err){
                if(err)
                  console.log(err);
            });

          }
      }
          
    }
  })
User.findOne({_id:req.body.authorId}).exec(function(err,user){
  if(err)
    console.log(err);
  else
  {
      for(var i=0;i<user.writeArticle.length;i++){
        if(user.writeArticle[i]._id==req.body.articleId){
          if(user.writeArticle[i].article_artist[0])
          {
            
                   mandrill('/messages/send', {
                        "message": {
                            to: [{email: user.writeArticle[i].article_artist[0].articleArtistemail}],
                            from_email: 'admin@patarboi.ca',
                            subject: "Bidding progress",
                            "html": " A bidding is placed by "+req.user.email+ " with "+req.body.cost+" taka/page." ,
                        },
                       
                      
                    }, function(error, response)
                    {

                        if (error) 
                          console.log( JSON.stringify(error) );

                        else 
                          console.log(response);
                    });
           
              
          }
          if(user.writeArticle[i].article_artist[1])
          {
            
                   mandrill('/messages/send', {
                        "message": {
                            to: [{email: user.writeArticle[i].article_artist[1].articleArtistemail}],
                            from_email: 'admin@patarboi.ca',
                            subject: "Bidding progress",
                            "html": " A bidding is placed by "+req.user.email+ " with "+req.body.cost+" taka/page." ,
                        },
                       
                      
                    }, function(error, response)
                    {

                        if (error) 
                          console.log( JSON.stringify(error) );

                        else 
                          console.log(response);
                    });
           
              
          }
          if(user.writeArticle[i].article_artist[2])
          {
            
                   mandrill('/messages/send', {
                        "message": {
                            to: [{email: user.writeArticle[i].article_artist[2].articleArtistemail}],
                            from_email: 'admin@patarboi.ca',
                            subject: "Bidding progress",
                            "html": " A bidding is placed by "+req.user.email+ " with "+req.body.cost+" taka/page." ,
                        },
                       
                      
                    }, function(error, response)
                    {

                        if (error) 
                          console.log( JSON.stringify(error) );

                        else 
                          console.log(response);
                    });
           
              
          }
        }
      }
  }
})
  User.findOne({_id:req.body.authorId}).exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
           (function(i){
            if(user.writeArticle[i]._id==req.body.articleId)
            {
              user.writeArticle[i].status='Bidding in progress'
               // user.writeArticle[i].bidding_Article.push({artistId:req.user._id,bidding_status:'Bidding in progress',no_of_image:req.body.no_of_image,cost:req.body.cost});
                for(var k=0;k<user.writeArticle[i].bidding_Article.length;k++)
                {
                    if(user.writeArticle[i].bidding_Article[k].artistId==req.user._id)
                    {
                        check=1;
                        user.writeArticle[i].bidding_Article[k].no_of_image=req.body.no_image;
                        user.writeArticle[i].bidding_Article[k].cost=req.body.cost;
                    }
                }
                if(check==0)
                  user.writeArticle[i].bidding_Article.push({artistId:req.user._id,artistEmail:req.user.email,bidding_status:'Bidding in progress',no_of_image:req.body.no_image,cost:req.body.cost,status:''});
            }



            for(var j=0;j<user.writeArticle[i].article_artist.length;j++)
            {
              (function(j){

              
             
              
              if(user.writeArticle[i].article_artist[j].articleArtistemail==req.user.email)
              {
                  user.writeArticle[i].article_artist[j].status='Bidding in progress';
              }
              //console.log(user.writeArticle[i].article_artist[j].articleArtistemail)
              })(j)
            }
          })(i)
          if(i==user.writeArticle.length-1)
          {
             user.save(function(err){
                if(err)
                  console.log(err);
                else
                {
                    
                    res.send("save")
                }
              
            });
          }
        }
        
    }
  })
}
exports.bid_article=function(req,res){
  console.log(req.param('articleId'));
  var article;
  var author;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                if(user[i].writeArticle[j]._id==req.param('articleId'))
                {
                    article=user[i].writeArticle[j];
                    author=user[i];

                }
            }
        }
        res.render('bid_article',{
          article:article,
          author:author,
          user:req.user,
        })
    }
  })
}
exports.writeArticle=function(req,res){

   res.render('writeArticle');
}
exports.deleteSearchedArticle=function(req,res)
{
  console.log(req.body)
  var id=req.body.user;
  var articleId=req.body.articleId;
  User.findOne({_id:id}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==articleId)
            {
              user.writeArticle.splice(i,1);
              user.save(function(err){
                  if(err)
                    console.log(err);
                  else
                  {
                      
                      res.send("save")
                  }
                
               });
            }
               
        }
      }
  })
}
exports.RemoveArtist=function(req,res)
{
  console.log("got");
  User.findOne({_id:req.body.authorId}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
      
      for(var i=0;i<user.writeArticle.length;i++)
      {
          if(user.writeArticle[i]._id==req.body.writeArticleId)
          {
              //console.log(user.writeArticle[i].bookTitle)
              user.writeArticle[i].noOfArtist=user.writeArticle[i].noOfArtist-1
              user.writeArticle[i].article_artist.splice(req.body.ind-1,1);
               user.save(function(err){
                    if(err)
                      console.log('Error');
                    else
                    {
                      User.findOne({email:req.body.artist_1}).exec(function(err,artist){
                        if(err)
                          console.log(err)
                        else
                        {
                             for(var i=0;i<artist.selectedArtist.length;i++)
                             {
                                if(artist.selectedArtist[i]._id==req.body.writeArticleId)
                                {
                                    artist.selectedArtist.splice(i,1);
                                    artist.selectedArtistId.splice(i,1);
                                    artist.save(function(err){
                                      if(err)
                                        console.log(err)
                                      else
                                      {
                                          console.log('Save');
                                          res.send("save");
                                      }
                                    });
                                }
                             }
                        }
                      })
                    }
                  });
          }
      }
    }
  })

}
exports.deleteReviewer=function(req,res){
   User.findOne({_id: req.body.userid}).exec(function(err,user){
      if(err)
        console.log('Error');
      else
      {
        var count=0;
        
        

        for(var i=0;i<user.writeArticle.length && count==0;i++)
        {

              if(user.writeArticle[i]._id == req.body.articleId)
              {
               
                  count=1;
                  user.writeArticle[i].noOfreviewer=user.writeArticle[i].noOfreviewer-1
                  user.writeArticle[i].article_reviewer.splice(req.body.ind-1,1);
                  if(user.writeArticle[i].articleReviewer_comment)
                  user.writeArticle[i].articleReviewer_comment.splice(req.body.ind-1,1);
                  user.save(function(err){
                    if(err)
                      console.log('Error');
                    else
                    {
                        User.findOne({email:req.body.reviewer}).exec(function(err,user1){
                          if(err)
                            console.log(err)
                          else{
                            for(var i=0;i<user1.reviewer.length;i++)
                            {
                                if(user1.reviewer[i].writeArticleId==req.body.articleId)
                                {

                                  console.log('MAtch complete');
                                 // console.log(user1.reviewer[i]);
                                   user1.reviewer.splice(i,1);
                                    user1.save(function(err){
                                    if(err)
                                      console.log(err);
                                    else{
                                      console.log('Dine delete')
                                      res.send('Review data saved successful');
                                    }
                                })

                                }
                            }
                            // var find=_.findWhere(user1.reviewer,{writeArticleId: req.body.articleId});
                            // user1.reviewer=_.without(user1.reviewer,_.findWhere(user1.reviewer,{writeArticleId:req.body.articleId}));
                            // if(find)
                            // {
                            //    user1.
                            // }
                          }
                        })
                    }
                  })






                  //   User.find().exec(function(err,reviewUser_1){
                  //     if(err)
                  //       console.log('reviewUser_1  Error');
                  //     else
                  //     {

                  //         for(var i=0;i<reviewUser_1.length;i++)
                  //         {

                  //               if(reviewUser_1[i].email == req.body.reviewer)
                  //               {

                  //                  // for(var k=0;k<reviewUser_1[i].reviewer.length;k++)
                  //                  // {
                  //                     var find=_.findWhere(reviewUser_1[i].reviewer,{writeArticleId: req.body.articleId});
                  //                     console.log('find');
                  //                     console.log(find)
                  //                     reviewUser_1[i].reviewer=_.without(reviewUser_1[i].reviewer,_.findWhere(reviewUser_1[i].reviewer,{writeArticleId: req.body.articleId}));
                  //                     console.log('find');
                  //                     console.log(reviewUser_1[i].reviewer)
                  //                     if(find){
                                        // reviewUser_1[i].save(function(err){
                                        //     if(err)
                                        //       console.log(err);
                                        //     else
                                        //     {
                                        //         console.log("got my reviewer dsd");
                                        //         res.send('Review data saved successful');
                                        //        // res.redirect('/userArticle');
                                        //     }
                                          
                                        // });
                  //                     }
                                      
                  //                     // if(reviewUser_1[i].reviewer[k].writeArticleId==req.body.articleId)
                  //                     // {
                  //                     //    console.log("got my reviewer");
                  //                     //     console.log(k);
                  //                     //     console.log(reviewUser_1[i].reviewer[k]);
                  //                     //     //reviewUser_1[i].reviewer[k].reviewer_status='Delete'
                  //                     //  // reviewUser_1[i].reviewer.splice(k,1);
                  //                     //    //reviewUser_1[i].reviewer[k].reviewer_status='Delete'
                  //                     //   // user.unreadMessage=_.without(user.unreadMessage,_.findWhere(user.unreadMessage,{_id: deleteMessage[k]}));


                  //                     //   reviewUser_1[i].save(function(err)
                  //                     //   {

                  //                     //       if(err)
                  //                     //         res.send('Error');
                  //                     //       else
                  //                     //       {

                  //                     //         console.log("got my reviewer dsd");
                  //                     //         res.send('Review data saved successful');
                  //                     //       }
                                              
                  //                     //    });
                  //                     // }
                  //                //  }   


                                            

                   

                  //               }


                  //         }

                  //     }
        

                  // });
                }
        }
      }
    })
}
exports.articleDelete=function(req,res){
  console.log(req.body);
  req.user.writeArticle.splice(req.body.index,1);
  req.user.pendingArticleCount=req.user.pendingArticleCount-1;
  if(req.pendingArticleCount>=0)
      {
          req.user.pendingArticleCount=user.pendingArticleCount-parseInt(req.user.pendingArticleCount);

      }
    req.user.save(function(err){
        if(err)
          console.log(err);
        else
        {
            
            res.redirect('/userArticle');
        }
      
    });
}


// this is for article submit:
// this is for article submit:
exports.article=function(req,res)
{  
    // console.log('Article');
    // console.log(req.body.hiddenCategory);

     var str = req.body.hiddenCategory.toString();
     var newCat=str.split(",");
    // console.log(newCat);
     for(var i=0;i<newCat.length;i++)
     {
        console.log(newCat[i]);
     }

    // console.log('Length cat:'+newCat.length);
   //  console.log(newCat);





     var dpic;
     if(req.body.default_image == '1')
     {
        dpic=req.files.img.name;
     }
     else if(req.body.default_image == '2')
     {
        dpic=req.files.img2.name;

     }
     else if(req.body.default_image == '3')
     {
        dpic=req.files.img3.name;

     }
     else if(req.body.default_image == '4')
     {
        dpic=req.files.img4.name;

     }
    else if(req.body.default_image == '5')
     {
        dpic=req.files.img5.name;

     }
    else if(req.body.default_image == '6')
     {
        dpic=req.files.img6.name;

     }
     else if(req.body.default_image == '7')
     {
        dpic=req.files.img7.name;

     }
    else if(req.body.default_image == '8')
     {
        dpic=req.files.img8.name;

     }
    else if(req.body.default_image == '9')
     {
        dpic=req.files.img9.name;
     }
     else if(req.body.default_image == '10')
     {
        dpic=req.files.img10.name;

     }
     else if(req.body.default_image == '11')
     {
        dpic=req.files.img11.name;

     }
     else if(req.body.default_image == '12')
     {
        dpic=req.files.img12.name;

     }
    else if(req.body.default_image == '13')
     {
        dpic=req.files.img13.name;

     }
    else if(req.body.default_image == '14')
     {
        dpic=req.files.img14.name;

     }
     else if(req.body.default_image == '15')
     {
        dpic=req.files.img15.name;

     }
    else if(req.body.default_image == '16')
     {
        dpic=req.files.img16.name;

     }
     dpic=req.files.coverimg.name;

console.log(dpic);
       fs.readFile(req.files.coverimg.path, function (err, data) {
                
          fs.writeFile('public/article_image/'+req.files.coverimg.name,data, function (err) {
            if (err) 
              console.log(err);
              //throw err; 
            else
              console.log('Done success');
          });
        });
     var number;

     User.find().exec(function(err,user){

            if(err)
              console.log('Found Error');
            else
            {
                for(var i=0;i<user.length;i++)
                {

                    if(user[i].userType == 'admin')
                    {

                        number=user[i].articleNumber;
                        console.log('number: '+ number);
                        user[i].articleNumber= user[i].articleNumber + 1;

                        user[i].save(function(err){
                              if(err)
                                console.log('Error admin');
                          })

                    }
                }




            }
     });

     var str = req.body.hiddenTag.toString();
     var newstr=str.split(",");
     var image;
     var image2;
     var image3;
     var image4;
     var image5;
     var image6;
     var image7;
     var image8;
     var image9;
     var image10;
     var image11;
     var image12;
     var image13;
     var image14;
     var image15;
     var image16;
     var no_page=0;
     console.log(newstr);
     for(var i=0;i<newstr.length;i++)
     {
        console.log(newstr[i]);
     }



              fs.readFile(req.files.img.path, function (err, data) {
                  
                fs.writeFile('public/article_image/'+req.files.img.name,data, function (err) {
                  if (err) 
                    console.log('Error');
                    //throw err; 
                  else{
                   // no_page=no_page+1;
                    console.log('Done success');
                  }
                    
                });
                
                

              });
              fs.readFile(req.files.img2.path, function (err, data) {
              
                fs.writeFile('public/article_image/'+req.files.img2.name,data, function (err) {
                  if (err) 
                    console.log(err);
                    //throw err; 
                   else{
                   // no_page=no_page+1;
                    console.log('Done success');
                  }
                });
                //image2='/article_image/'+req.files.img2.name;
              });
              fs.readFile(req.files.img3.path, function (err, data) {
              
                fs.writeFile('public/article_image/'+req.files.img3.name,data, function (err) {
                  if (err) 
                    console.log(err);
                    //throw err; 
                   else{
                   // no_page=no_page+1;
                    console.log('Done success');
                  }
                });
                //image3='/article_image/'+req.files.img3.name;
              });
              fs.readFile(req.files.img4.path, function (err, data) {
              
                fs.writeFile('public/article_image/'+req.files.img4.name,data, function (err) {
                  if (err) 
                    console.log(err);
                    //throw err; 
                   else{
                   // no_page=no_page+1;
                    console.log('Done success');
                  }
                });
               // image4='/article_image/'+req.files.img4.name;
              });

      if(req.body.morepage == 1 || req.body.morepage == 2 || req.body.morepage == 3) // for 5-8
      {

                fs.readFile(req.files.img5.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img5.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });
                fs.readFile(req.files.img6.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img6.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                     // no_page=no_page+1;
                      console.log('Done success');
                    }
                  });
                });
                fs.readFile(req.files.img7.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img7.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });
                fs.readFile(req.files.img8.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img8.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });

      }
      else if(req.body.morepage ==2 || req.body.morepage ==3)
      {

                fs.readFile(req.files.img9.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img9.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });
                fs.readFile(req.files.img10.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img10.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });
                fs.readFile(req.files.img11.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img11.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });
                fs.readFile(req.files.img12.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img12.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else{
                     // no_page=no_page+1;
                      console.log('Done success');
                    }
                  });
                });


      }
      else if(req.body.morepage == 3)
      {

                fs.readFile(req.files.img13.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img13.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else{
                     // no_page=no_page+1;
                      console.log('Done success');
                    }
                  });
                });
                fs.readFile(req.files.img114.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img114.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else{
                     // no_page=no_page+1;
                      console.log('Done success');
                    }
                  });
                });
                fs.readFile(req.files.img15.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img15.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });
                fs.readFile(req.files.img16.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img16.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                     else{
                       // no_page=no_page+1;
                        console.log('Done success');
                      }
                  });
                });


      }

      if(req.files.img.name){
           no_page=no_page+1;
           image='/article_image/'+req.files.img.name;
         }
      if(req.files.img2.name){
        no_page=no_page+1;
        image2='/article_image/'+req.files.img2.name;
      }
          
      if(req.files.img3.name){
        no_page=no_page+1;
        image3='/article_image/'+req.files.img3.name;
      }
          
      if(req.files.img4.name){
        no_page=no_page+1;
        image4='/article_image/'+req.files.img4.name;
      }
          
      if(req.files.img5.name){
        no_page=no_page+1;
        image5='/article_image/'+req.files.img5.name;
      }
          
      if(req.files.img6.name){
        no_page=no_page+1;
        image6='/article_image/'+req.files.img6.name;
      }
          
      if(req.files.img7.name){
        no_page=no_page+1;
        image7='/article_image/'+req.files.img7.name;
      }
          
      if(req.files.img8.name){
        no_page=no_page+1;
        image8='/article_image/'+req.files.img8.name;
      }
          
      if(req.files.img9.name){
        no_page=no_page+1;
        image9='/article_image/'+req.files.img9.name;
      }
          
      if(req.files.img10.name){
        no_page=no_page+1;
        image10='/article_image/'+req.files.img10.name;
      }
          
      if(req.files.img11.name){
        no_page=no_page+1;
        image11='/article_image/'+req.files.img11.name;
      }
          
      if(req.files.img12.name){
        no_page=no_page+1;
        image12='/article_image/'+req.files.img12.name;
      }
          
      if(req.files.img13.name){
        no_page=no_page+1;
        image13='/article_image/'+req.files.img13.name;
      }
          
      if(req.files.img14.name){
        no_page=no_page+1;
        image14='/article_image/'+req.files.img14.name;
      }
          
      if(req.files.img15.name){
        no_page=no_page+1;
        image15='/article_image/'+req.files.img15.name;
      }
          
      if(req.files.img16.name){
        no_page=no_page+1;
        image16='/article_image/'+req.files.img16.name;
      }
       console.log(no_page)   
      if(req.body.morepage == 0)
      {
           req.user.writeArticle.push({no_page:no_page,NickName:req.body.nick_name,artistName:req.body.artist_name,homePic:dpic,default_image:req.body.default_image,sound:'No',bookTitle:req.body.titleofBook,title: req.body.articleTitle, article: req.body.article,articleImage:image,credit: req.body.source,bottomText: req.body.bottomText,tags:newstr,cetagory:newCat,label:req.body.label, status:req.body.draft,articleId: 'atricle#'+x,
          topFontcolor:req.body.topFontColor,topTextalign:req.body.toptextAlign, topBackgroundcolor: req.body.topbackgroundColor,topTexttransparency:req.body.topTexttransparency, imagePosition:req.body.imageposition, bottomTextcolor: req.body.bottomTextcolor, bottomTextalign:req.body.bottomTextAlign, bottomBackgroundcolor: req.body.bottomBackground, bottomTransparency:req.body.bottomTransparency,
          articleTitle2:req.body.articleTitle2 ,article2:req.body.article2 ,img2:image2 ,source2:req.body.source2 ,bottomText2:req.body.bottomText2 ,articleTitle3:req.body.articleTitle3,article3:req.body.article3,img3:image3 ,source3:req.body.source3,bottomText3:req.body.bottomText3,articleTitle4:req.body.articleTitle4,article4:req.body.article4,img4:image4,source4:req.body.source4,bottomText4:req.body.bottomText4,morepage:req.body.morepage});

      }

     //---------------------Mustakim Starts-------------------
      else if(req.body.morepage == 1)
      {
           req.user.writeArticle.push({no_page:no_page,NickName:req.body.nick_name,artistName:req.body.artist_name,homePic:dpic,default_image:req.body.default_image,sound:'No',bookTitle:req.body.titleofBook,title: req.body.articleTitle, article: req.body.article,articleImage:image,credit: req.body.source,bottomText: req.body.bottomText,tags:newstr,cetagory:newCat ,label:req.body.label, status:req.body.draft,articleId: 'atricle#'+x,
          topFontcolor:req.body.topFontColor,topTextalign:req.body.toptextAlign, topBackgroundcolor: req.body.topbackgroundColor,topTexttransparency:req.body.topTexttransparency, imagePosition:req.body.imageposition, bottomTextcolor: req.body.bottomTextcolor, bottomTextalign:req.body.bottomTextAlign, bottomBackgroundcolor: req.body.bottomBackground, bottomTransparency:req.body.bottomTransparency,
          articleTitle2:req.body.articleTitle2 ,article2:req.body.article2 ,img2:image2 ,source2:req.body.source2 ,bottomText2:req.body.bottomText2 ,articleTitle3:req.body.articleTitle3,article3:req.body.article3,img3:image3,source3:req.body.source3,bottomText3:req.body.bottomText3,articleTitle4:req.body.articleTitle4,article4:req.body.article4,img4:image4,source4:req.body.source4,bottomText4:req.body.bottomText4,
          articleTitle5:req.body.articleTitle5 ,article5:req.body.article5 ,img5:image5,source5:req.body.source5 ,bottomText5:req.body.bottomText5 ,articleTitle6:req.body.articleTitle6,article6:req.body.article6,img6:image6, source6:req.body.source6,bottomText6:req.body.bottomText6,articleTitle7:req.body.articleTitle7,article7:req.body.article7,img7:image7,source7:req.body.source7,bottomText7:req.body.bottomText7, 
          articleTitle8:req.body.articleTitle8, article8:req.body.article8, img8:image8, source8:req.body.source8, bottomText8:req.body.bottomText8,morepage:req.body.morepage});
      }

      else if(req.body.morepage == 2)
      {
           req.user.writeArticle.push({no_page:no_page,NickName:req.body.nick_name,artistName:req.body.artist_name,homePic:dpic,default_image:req.body.default_image,sound:'No',bookTitle:req.body.titleofBook,title: req.body.articleTitle, article: req.body.article,articleImage:image,credit: req.body.source,bottomText: req.body.bottomText,tags:newstr,cetagory:newCat ,label:req.body.label, status:req.body.draft,articleId: 'atricle#'+x,
          topFontcolor:req.body.topFontColor,topTextalign:req.body.toptextAlign, topBackgroundcolor: req.body.topbackgroundColor,topTexttransparency:req.body.topTexttransparency, imagePosition:req.body.imageposition, bottomTextcolor: req.body.bottomTextcolor, bottomTextalign:req.body.bottomTextAlign, bottomBackgroundcolor: req.body.bottomBackground, bottomTransparency:req.body.bottomTransparency,
          articleTitle2:req.body.articleTitle2 ,article2:req.body.article2 ,img2:image2 ,source2:req.body.source2 ,bottomText2:req.body.bottomText2 ,articleTitle3:req.body.articleTitle3,article3:req.body.article3,img3:image3,source3:req.body.source3,bottomText3:req.body.bottomText3,articleTitle4:req.body.articleTitle4,article4:req.body.article4,img4:image4,source4:req.body.source4,bottomText4:req.body.bottomText4,
          articleTitle5:req.body.articleTitle5 ,article5:req.body.article5 ,img5:image5,source5:req.body.source5 ,bottomText5:req.body.bottomText5 ,articleTitle6:req.body.articleTitle6,article6:req.body.article6,img6:image6, source6:req.body.source6,bottomText6:req.body.bottomText6,articleTitle7:req.body.articleTitle7,article7:req.body.article7,img7:image7,source7:req.body.source7,bottomText7:req.body.bottomText7, 
          articleTitle8:req.body.articleTitle8, article8:req.body.article8, img8:image8, source8:req.body.source8, bottomText8:req.body.bottomText8, articleTitle9:req.body.articleTitle9,article9:req.body.article9,img9:image9, source9:req.body.source9,bottomText9:req.body.bottomText9, articleTitle10:req.body.articleTitle10,article10:req.body.article10,img10:image10, source10:req.body.source10,bottomText10:req.body.bottomText10,
          articleTitle11:req.body.articleTitle11,article11:req.body.article11,img11:image11, source11:req.body.source11,bottomText11:req.body.bottomText11, articleTitle12:req.body.articleTitle12,article12:req.body.article12,img12:image12, source12:req.body.source12,bottomText12:req.body.bottomText12,morepage:req.body.morepage});
      }

      else if(req.body.morepage == 3)
      {
          req.user.writeArticle.push({no_page:no_page,NickName:req.body.nick_name,artistName:req.body.artist_name,homePic:dpic,default_image:req.body.default_image,sound:'No',bookTitle:req.body.titleofBook,title: req.body.articleTitle, article: req.body.article,articleImage:image,credit: req.body.source,bottomText: req.body.bottomText,tags:newstr,cetagory:newCat ,label:req.body.label, status:req.body.draft,articleId: 'atricle#'+x,
          topFontcolor:req.body.topFontColor,topTextalign:req.body.toptextAlign, topBackgroundcolor: req.body.topbackgroundColor,topTexttransparency:req.body.topTexttransparency, imagePosition:req.body.imageposition, bottomTextcolor: req.body.bottomTextcolor, bottomTextalign:req.body.bottomTextAlign, bottomBackgroundcolor: req.body.bottomBackground, bottomTransparency:req.body.bottomTransparency,
          articleTitle2:req.body.articleTitle2 ,article2:req.body.article2 ,img2:image2 ,source2:req.body.source2 ,bottomText2:req.body.bottomText2 ,articleTitle3:req.body.articleTitle3,article3:req.body.article3,img3:image3,source3:req.body.source3,bottomText3:req.body.bottomText3,articleTitle4:req.body.articleTitle4,article4:req.body.article4,img4:image4,source4:req.body.source4,bottomText4:req.body.bottomText4,
          articleTitle5:req.body.articleTitle5 ,article5:req.body.article5 ,img5:image5,source5:req.body.source5 ,bottomText5:req.body.bottomText5 ,articleTitle6:req.body.articleTitle6,article6:req.body.article6,img6:image6, source6:req.body.source6,bottomText6:req.body.bottomText6,articleTitle7:req.body.articleTitle7,article7:req.body.article7,img7:image7,source7:req.body.source7,bottomText7:req.body.bottomText7, 
          articleTitle8:req.body.articleTitle8, article8:req.body.article8, img8:image8, source8:req.body.source8, bottomText8:req.body.bottomText8, articleTitle9:req.body.articleTitle9,article9:req.body.article9,img9:image9, source9:req.body.source9,bottomText9:req.body.bottomText9, articleTitle10:req.body.articleTitle10,article10:req.body.article10,img10:image10, source10:req.body.source10,bottomText10:req.body.bottomText10,
          articleTitle11:req.body.articleTitle11,article11:req.body.article11,img11:image11, source11:req.body.source11,bottomText11:req.body.bottomText11, articleTitle12:req.body.articleTitle12,article12:req.body.article12,img12:image12, source12:req.body.source12,bottomText12:req.body.bottomText12, articleTitle13:req.body.articleTitle13,article13:req.body.article13,img13:image13, source13:req.body.source13,bottomText13:req.body.bottomText13,
          articleTitle14:req.body.articleTitle14,article14:req.body.article14,img14:image14, source14:req.body.source14,bottomText14:req.body.bottomText14, articleTitle15:req.body.articleTitle15,article15:req.body.article15,img15:image15, source15:req.body.source15,bottomText15:req.body.bottomText15, articleTitle16:req.body.articleTitle16,article16:req.body.article16,img16:image16, source16:req.body.source16,bottomText16:req.body.bottomText16,morepage:req.body.morepage});

      }

      //-----------Mustakim Billah ends-----------

      console.log('Draft:'+req.body.draft);
      if(req.body.draft == 'waiting')
      {
            console.log('Check it')     
            req.user.pendingArticleCount=req.user.pendingArticleCount+1;
            console.log(req.user.pendingArticleCount);

      } 

      x++;


          if(req.pendingArticleCount>0)
          {
              req.user.pendingArticleCount=user.pendingArticleCount-parseInt(req.user.pendingArticleCount);

          }     
      



 
    req.user.save(function(err){
            if(err){
             
              console.log(err);
            }
              
            else
             { 
              //console.log("req.body.draft")
            //     console.log(req.body.draft)
            if(req.body.draft=='draft')
              res.redirect('/draft');
            else
                res.redirect('/userArticle');
            }
          
        });   




}



exports.draft=function(req,res)
{
  res.render('draft',{
    article:req.user.writeArticle,
  });
}
exports.showreviedArticle=function(req,res)
{
    var id=req.param('articleid');
    var articleUser;
    var article;
    User.find().exec(function(err,user){
      if(err)
        console.log(err);
      else
      {
          for(var i=0;i<user.length;i++)
          {
              for(var j=0;j<user[i].writeArticle.length;j++)
              {
                  if(user[i].writeArticle[j]._id==id)
                  {
                      article=user[i].writeArticle[j];
                      articleUser=user[i];
                      res.render('previous-single-article',{
                        singleArticle:article,
                        articleUser:articleUser
                      });
                  }
              }
          }
      }
    })
}
exports.review=function(req,res)
{
    var id=req.param('articleid');
    console.log(id)
    res.render('review',{
      user:req.user,
      id:id,
    });

}
exports.previousarticle=function(req,res)
{
    console.log(req.param('articleid'));
    var articleUser;
    var id=req.param('articleid');
    var article;
    articleUser=req.user;
    for(var i=0;i<req.user.writeArticle.length;i++)
    {   console.log(req.user.writeArticle[i]._id)
        if(req.user.writeArticle[i]._id == id)
        {
            article=req.user.writeArticle[i];
            // console.log(article);
            // console.log('Got it');
            res.render('previous-single-article',{
              singleArticle:article,
              articleUser:articleUser
            });
        }
    }
}


exports.editarticle=function(req,res)
{
     console.log(req.param('articleid'));
    var id=req.param('articleid');
    var article;
    for(var i=0;i<req.user.writeArticle.length;i++)
    {
        if(req.user.writeArticle[i]._id == id)
        {
            article=req.user.writeArticle[i];
            console.log(article);
            console.log('Got it');
            res.render('editarticle',{
              singleArticle:article
            });
        }
    }

}
exports.OurUser=function(req,res)
{
    if(req.body.type=='ourAuthor')
    {
        req.user.textourAuthor=req.body.text;
    }

    else if(req.body.type=='ourAuthor')
    {
        req.user.textOurreviewer=req.body.text;
    }
    else if(req.body.type=='ourAuthor')
    {
        req.user.textOurArtist=req.body.text;
    }
    else if(req.body.type=='ourAuthor')
    {
        req.user.textOurDonor=req.body.text;
    }
    req.user.save(function(err){
      if(err)
        console.log(err)
      else
        console.log("save");
    })
}
exports.invite=function(req,res)
{
  if(req.body.type=='User')
  {
    req.user.textInviteUser=req.body.text;
  }
  else if(req.body.type=='Reviewer')
  {
    req.user.textInviteReviewer=req.body.text;
  }
  else if(req.body.type=='Artists')
  {
    req.user.textInviteArtist=req.body.text;
  }
  req.user.save(function(err){
    if(err)
      console.log(err);
    else
      console.log("save")
  })
var check=0;
  User.findOne({email:req.body.email}).exec(function(err,user){
    if(err)
      console.log(err);
    else if(user){
      if(req.body.type=='Reviewer')
      {
          user.reviewerType='Reviewer';

      }
      
    else{
      for(var k=0;k<user.userSubtype.length;k++)
      {
          if(user.userSubtype[k].subtype==req.body.type)
          {
              check==1;
          }
      }
      if(check==0)
      user.userSubtype.push({subtype:req.body.type});
    }
      user.save(function(err){
        if(err)
          console.log(err)
        else{
             mandrill('/messages/send', {
                  "message": {
                      to: [{email: req.body.email}],
                      from_email: 'admin@patarboi.ca',
                      subject: "Invitation in www.patarboi.com !!",
                      "html": ""+req.body.text ,
                  }, 
              }, function(error, response)
              {
                  if (error) 
                    console.log( JSON.stringify(error) );
                  else 
                    console.log(response);
              });
              res.send('Done');
            }
      })
    }

  })
      //console.log(req.body);
       
}

exports.assignreviewer=function(req,res)
{
    console.log('Server ');
    var author=[];
    var counter=0;
   
    User.find().exec(function(err,user)
    {
        
        if(err)
          console.log(err);
        else
        {
            

            for(var i=0;i<user.length;i++)
            {
                if(user[i].userType == 'Author')
                {
                      author[counter]=user[i];
                      counter++;
                }

            }    


            //console.log(author);        

          res.render('assignreviewer',{

                author:author,

           });

        }


    });




}


exports.articlerating=function(req,res)
{
    //var ratedUser=0;
    console.log('Server rating:');
    console.log(req.body);
    User.find().exec(function(err,user){

        if(err)
          console.log('Error');

         else
         {
            //console.log(user.length);
            for(var i=0;i<user.length;i++)
            {
                 
                if(user[i]._id == req.body.userId)
                {
                  console.log(user[i].name);

                  for(var j=0;j<user[i].writeArticle.length;j++)
                  {

                      if(user[i].writeArticle[j]._id == req.body.articleId)
                      {   
                          console.log('got it');
                          //console.log(user[i].writeArticle[j].ratedUser);
                          var y=user[i].writeArticle[j].ratedUser+1;// user
                          var x= parseInt(y);
                          user[i].writeArticle[j].ratedUser=x;
                          console.log('count' + user[i].writeArticle[j].ratedUser);

                          var currentRating=req.body.rating;
                          var rate= parseInt(currentRating);
                          console.log('current'+ currentRating);

                          var temp=parseInt(user[i].writeArticle[j].rating) + rate;// rating
                          console.log('temp:'+temp);

                          user[i].writeArticle[j].rating = temp/(user[i].writeArticle[j].ratedUser);

                          console.log('Rating:'+user[i].writeArticle[j].rating);
                          user[i].writeArticle[j].ratedUser=1;
                          

                          user[i].save(function(err){
                              if(err)
                                console.log('Error');
                              else
                                res.send('Send successful');

                          })
                          
                      }


                  }

                }
            }
          


         }


   });

}

exports.assignartist=function(req,res){
    var counter=0;
    var article_user=[];
    User.find().exec(function(err,user)
    {
        if(err)
          console.log('Error');
        else
        {
            for(var i=0;i<user.length;i++)
            {
                if(user[i].userType == 'Author')
                {
                    article_user[counter]=user[i];
                    counter++;
                }
            }            
            res.render('assignartist',{
             author:article_user,
             });
        }
    });
}
exports.completereview=function(req,res){
  var author=[];
  var article=[];
  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else{
      for(var i=0;i<user.length;i++)
      {
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
              if(user[i].writeArticle[j].status=='Review complete'||user[i].writeArticle[j].status=='Request Changed'||user[i].writeArticle[j].status=='With Condition')
              {
                  author[count]=user[i];
                  article[count]=user[i].writeArticle[j];
                  count++;
              }
          }
      }
      res.render('completeReview',{

       author:author,
       article:article,

       });
    }
  })

      
}
exports.acceptArticle=function(req,res)
{
    var userId=req.param('user');
    var article=req.param('article')
    // console.log(user+" "+article);
    User.findOne({_id:userId}).exec(function(err,user){
      if(err)
        console.log(err)
      else if(user)
      { 
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==article)
              {
                  user.writeArticle[i].status="Accepted";
                  user.save(function(err){
                   
                    if(err)
                      console.log(err)
                    else
                    {
                      console.log('Save');
                     res.redirect('/completereview');
                    }
                      
                  }) 

              }
          }
      }
    })
}
exports.rejectReviewArticle=function(req,res){
   console.log(req.body);
    User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            { 
              //console.log("got")  
              console.log("a "+i+" "+user.length)
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                   console.log("bb"+user[i].writeArticle[j]._id)
                    if(user[i].writeArticle[j]._id==req.body.writeArticleId)
                    {
                        //console.log('ABC')
                       // console.log(user[i].writeArticle[j].article_reviewer)
                       for(var k=0;k<user[i].writeArticle[j].article_reviewer.length;k++)
                       {
                          console.log(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail)
                          console.log(req.user._id)
                          if(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail==req.user.email)
                          {
                              user[i].writeArticle[j].article_reviewer[k].status='Decline';
                              user[i].save(function(err){
                                  if(err)
                                    console.log('Error');
                                  
                              })
                          }
                       }
                    }
                }
            }
        }
    })

    for(var i=0;i<req.user.reviewer.length;i++)
    {

        if(req.user.reviewer[i]._id == req.body.articleId)
        {

            console.log('Match');
            req.user.reviewer[i].reviewer_status ='Decline';
            req.user.save(function(err){
                if(err)
                  console.log('Error');
                else
                {
                    res.send('Status change');

                }
            })

        }
    }
    
}
exports.rejectArticle=function(req,res){
  var authorid=req.param('author');
  var articleid=req.param('article');
  var author;
  var article;
  User.findOne({_id:authorid}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
        for(var i=0;i<user.writeArticle.length;i++)
        {
          if(user.writeArticle[i]._id==articleid)
          {
             author=user;
              article=user.writeArticle[i];
          }
        }
        res.render('rejectArticle',{
          author:author,
          article:article,
        })
      }
  })
  
}
exports.reviewerDetails=function(req,res){
  console.log(req.param('id'));
  var articleId=req.param('articleId');
  var reviewing=0;
  var reviewed=0;
  var waiting=0;
  var decline=0;
if(req.param('id')!=null){
  User.findOne({_id:req.param('id')}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    { 
      for(var i=0;i<user.reviewer.length;i++)
      {
          if(user.reviewer[i].reviewer_status=='Accept')
            reviewing++;

      }
      for(var i=0;i<user.reviewer.length;i++)
      {
          if(user.reviewer[i].reviewer_status=='Finish')
            reviewed++;

      }
      for(var i=0;i<user.reviewer.length;i++)
      {
          if(user.reviewer[i].reviewer_status=='pending')
            waiting++;

      }
      for(var i=0;i<user.reviewer.length;i++)
      {
          if(user.reviewer[i].reviewer_status=='Decline')
            decline++;

      }
      User.find().exec(function(err,user1){
        if(err)
          console.log(err)
        else{
        for(var i=0;i<user1.length;i++)
        {
            for(var j=0;j<user1[i].writeArticle.length;j++)
            {
                if(user1[i].writeArticle[j]._id==articleId)
                {
                   for(var k=0;k<user1[i].writeArticle[j].articleReviewer_comment.length;k++){
                    if(user1[i].writeArticle[j].articleReviewer_comment[k].commenttator==user._id)
                    {
                      rating=user1[i].writeArticle[j].articleReviewer_comment[k].rating;
                    }
                   
                 }
                }
            }
        }
         res.render('reviewerDetails',{
            reviewer:user,
            reviewing:reviewing,
            reviewed:reviewed,
            waiting:waiting,
            decline:decline,
            rating:rating,
          });
       }
      })
     
    }
  })
}else{
  console.log("no reviewer")
}
 
}
exports.searchArticleByArtist=function(req,res){
  console.log(req.body.searchByArtist);
  var author=[];
  var article=[];
  var biddingarticle=[];
  var count=0;
  User.findOne({name:req.body.searchByArtist}).exec(function(err,artist){
    if(err)
      console.log(err)
    else
    {
         User.find().exec(function(err,user){
              if(err)
                console.log(err)
              else
              {
                for(var i=0;i<user.length;i++)
                {
                  for(var j=0;j<user[i].writeArticle.length;j++)
                  {
                      for(var k=0;k<user[i].writeArticle[j].bidding_Article.length;k++)
                      {
                          if(user[i].writeArticle[j].bidding_Article[k].artistId==artist._id)
                          {
                              author[count]=user[i];
                              article[count]=user[i].writeArticle[j];
                              biddingarticle[count]=user[i].writeArticle[j].bidding_Article[k];
                              count++;
                          }
                      }
                  }
                }

                  res.render('managecostArtist',{
                    artist:artist,
                    article:article,
                    author:author,
                    biddingarticle:biddingarticle,
                  })
              }
            })
    }
  })
           
}
exports.viewReview=function(req,res){
  console.log(req.body);
  var articleId=req.body.articleId;
  if(req.body.reviewer!=null){
      User.findOne({email:req.body.reviewer}).exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
          //  res.render('reviewerDetails')
          res.send('/reviewerDetails?id='+user._id+'&articleId='+articleId);
        }
      })
    }
    else{
      console.log("no reviewer")
    }
}
exports.submitChangeRequest=function(req,res)
{
  User.findOne({email:req.body.email}).exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==req.body.artID)
            {
                user.writeArticle[i].status='Request Change';
            }
        }
    }
  })
    mandrill('/messages/send', {


          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Change Article",
              "html": "Reviewers comment: " + req.body.comment1+ "  "+ req.body.comment2 +"  "+ req.body.comment3+ "  "+". Please change this state : "+req.body.reason+".Please click this link to change the contents: 162.243.133.214/changeArticle?articleId="+req.body.artID +".  Unless it is done, the article can’t be accepted.",
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else 
            console.log(response);
      });
   res.redirect('/completereview');
}
exports.changecontentsubmit=function(req,res){
    // console.log(req.body);// req.body
    var author=req.body.author;
    var articleId=req.body.articleId;
    var article;
    User.findOne({_id:author}).exec(function(err,user){
      if(err)
        console.log(err);
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==articleId)
              {
                article=user.writeArticle[i];
                if(req.body.titleofBook&&req.body.titleofBook!='')
                       user.writeArticle[i].bookTitle=req.body.titleofBook;

                
                if(req.body.articleTitle&&req.body.articleTitle!='')
                       user.writeArticle[i].title=req.body.articleTitle;
                     
                if(req.body.article&&req.body.article!='')
                       user.writeArticle[i].article=req.body.article;

                 if(req.body.source&&req.body.source!='')
                       user.writeArticle[i].credit=req.body.source;

                if(req.body.bottomText&&req.body.bottomText!='')
                       user.writeArticle[i].bottomText=req.body.bottomText;

                 
                if(req.body.articleTitle2&&req.body.articleTitle2!='')
                       user.writeArticle[i].articleTitle2=req.body.articleTitle2;  
                
                if(req.body.article2&&req.body.article2!='')
                       user.writeArticle[i].article2=req.body.article2; 

                if(req.body.source2&&req.body.source2!='')
                       user.writeArticle[i].source2=req.body.source2; 

                if(req.body.bottomText2&&req.body.bottomText2!='')
                       user.writeArticle[i].bottomText2=req.body.bottomText2;

                if(req.body.articleTitle3&&req.body.articleTitle3!='')
                       user.writeArticle[i].articleTitle3=req.body.articleTitle3;  
                
                if(req.body.article3&&req.body.article3!='')
                       user.writeArticle[i].article3=req.body.article3; 

                if(req.body.source3&&req.body.source3!='')
                       user.writeArticle[i].source3=req.body.source3; 

                if(req.body.bottomText3&&req.body.bottomText3!='')
                       user.writeArticle[i].bottomText3=req.body.bottomText3;


                if(req.body.articleTitle4&&req.body.articleTitle4!='')
                       user.writeArticle[i].articleTitle4=req.body.articleTitle4;  
                
                if(req.body.article4&&req.body.article4!='')
                       user.writeArticle[i].article4=req.body.article4; 

                if(req.body.source4&&req.body.source4!='')
                       user.writeArticle[i].source4=req.body.source4; 

                if(req.body.bottomText4&&req.body.bottomText4!='')
                       user.writeArticle[i].bottomText4=req.body.bottomText4;

                if(req.body.articleTitle5&&req.body.articleTitle5!='')
                       user.writeArticle[i].articleTitle5=req.body.articleTitle5;  
                
                if(req.body.article5&&req.body.article5!='')
                       user.writeArticle[i].article5=req.body.article5; 

                if(req.body.source5&&req.body.source5!='')
                       user.writeArticle[i].source5=req.body.source5; 

                if(req.body.bottomText5&&req.body.bottomText5!='')
                       user.writeArticle[i].bottomText5=req.body.bottomText5;


                if(req.body.articleTitle6&&req.body.articleTitle6!='')
                       user.writeArticle[i].articleTitle6=req.body.articleTitle6;  
                
                if(req.body.article6&&req.body.article6!='')
                       user.writeArticle[i].article6=req.body.article6; 

                if(req.body.source6&&req.body.source6!='')
                       user.writeArticle[i].source6=req.body.source6; 

                if(req.body.bottomText6&&req.body.bottomText6!='')
                       user.writeArticle[i].bottomText6=req.body.bottomText6;


                if(req.body.articleTitle7&&req.body.articleTitle7!='')
                       user.writeArticle[i].articleTitle7=req.body.articleTitle7;  
                
                if(req.body.article7&&req.body.article7!='')
                       user.writeArticle[i].article7=req.body.article7; 

                if(req.body.source7&&req.body.source7!='')
                       user.writeArticle[i].source7=req.body.source7; 

                if(req.body.bottomText7&&req.body.bottomText7!='')
                       user.writeArticle[i].bottomText7=req.body.bottomText7;


                if(req.body.articleTitle8&&req.body.articleTitle8!='')
                       user.writeArticle[i].articleTitle8=req.body.articleTitle8;  
                
                if(req.body.article8&&req.body.article8!='')
                       user.writeArticle[i].article8=req.body.article8; 

                if(req.body.source8&&req.body.source8!='')
                       user.writeArticle[i].source8=req.body.source8; 

                if(req.body.bottomText8&&req.body.bottomText8!='')
                       user.writeArticle[i].bottomText8=req.body.bottomText8;


                if(req.body.articleTitle9&&req.body.articleTitle9!='')
                       user.writeArticle[i].articleTitle9=req.body.articleTitle9;  
                
                if(req.body.article9&&req.body.article9!='')
                       user.writeArticle[i].article9=req.body.article9; 

                if(req.body.source9&&req.body.source9!='')
                       user.writeArticle[i].source9=req.body.source9; 

                if(req.body.bottomText9&&req.body.bottomText9!='')
                       user.writeArticle[i].bottomText9=req.body.bottomText9;


                if(req.body.articleTitle10&&req.body.articleTitle10!='')
                       user.writeArticle[i].articleTitle10=req.body.articleTitle10;  
                
                if(req.body.article10&&req.body.article10!='')
                       user.writeArticle[i].article10=req.body.article10; 

                if(req.body.source10&&req.body.source10!='')
                       user.writeArticle[i].source10=req.body.source10; 

                if(req.body.bottomText10&&req.body.bottomText10!='')
                       user.writeArticle[i].bottomText10=req.body.bottomText10;

                if(req.body.articleTitle11&&req.body.articleTitle11!='')
                       user.writeArticle[i].articleTitle11=req.body.articleTitle11;  
                
                if(req.body.article11&&req.body.article11!='')
                       user.writeArticle[i].article11=req.body.article11; 

                if(req.body.source11&&req.body.source11!='')
                       user.writeArticle[i].source11=req.body.source11; 

                if(req.body.bottomText11&&req.body.bottomText11!='')
                       user.writeArticle[i].bottomText11=req.body.bottomText11;


                if(req.body.articleTitle12&&req.body.articleTitle12!='')
                       user.writeArticle[i].articleTitle12=req.body.articleTitle12;  
                
                if(req.body.article12&&req.body.article12!='')
                       user.writeArticle[i].article12=req.body.article12; 

                if(req.body.source12&&req.body.source12!='')
                       user.writeArticle[i].source12=req.body.source12; 

                if(req.body.bottomText12&&req.body.bottomText12!='')
                       user.writeArticle[i].bottomText12=req.body.bottomText12;

                if(req.body.articleTitle13&&req.body.articleTitle13!='')
                       user.writeArticle[i].articleTitle13=req.body.articleTitle13;  
                
                if(req.body.article13&&req.body.article13!='')
                       user.writeArticle[i].article13=req.body.article13; 

                if(req.body.source13&&req.body.source13!='')
                       user.writeArticle[i].source13=req.body.source13; 

                if(req.body.bottomText13&&req.body.bottomText13!='')
                       user.writeArticle[i].bottomText13=req.body.bottomText13;

                if(req.body.articleTitle14&&req.body.articleTitle14!='')
                       user.writeArticle[i].articleTitle14=req.body.articleTitle14;  
                
                if(req.body.article14&&req.body.article14!='')
                       user.writeArticle[i].article14=req.body.article14; 

                if(req.body.source14&&req.body.source14!='')
                       user.writeArticle[i].source14=req.body.source14; 

                if(req.body.bottomText14&&req.body.bottomText14!='')
                       user.writeArticle[i].bottomText14=req.body.bottomText14;


                if(req.body.articleTitle15&&req.body.articleTitle15!='')
                       user.writeArticle[i].articleTitle15=req.body.articleTitle15;  
                
                if(req.body.article15&&req.body.article15!='')
                       user.writeArticle[i].article15=req.body.article15; 

                if(req.body.source15&&req.body.source15!='')
                       user.writeArticle[i].source15=req.body.source15; 

                if(req.body.bottomText15&&req.body.bottomText15!='')
                       user.writeArticle[i].bottomText15=req.body.bottomText15;


                if(req.body.articleTitle16&&req.body.articleTitle16!='')
                       user.writeArticle[i].articleTitle16=req.body.articleTitle16;  
                
                if(req.body.article16&&req.body.article16!='')
                       user.writeArticle[i].article16=req.body.article16; 

                if(req.body.source16&&req.body.source16!='')
                       user.writeArticle[i].source16=req.body.source16; 

                if(req.body.bottomText16&&req.body.bottomText16!='')
                       user.writeArticle[i].bottomText16=req.body.bottomText16;
              //  user.writeArticle[i].editcontent='Done';

                 if(req.files.img.name != "")
                  {
                      fs.readFile(req.files.img.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].articleImage='/article_image/'+req.files.img.name;
                  }
                  if(req.files.img2.name != "")
                  {
                      fs.readFile(req.files.img2.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img2.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img2='/article_image/'+req.files.img2.name;
                  }

                  if(req.files.img3.name != "")
                  {
                      fs.readFile(req.files.img3.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img3.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img3='/article_image/'+req.files.img3.name;
                  }
                  if(req.files.img4.name != "")
                  {
                      fs.readFile(req.files.img4.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img4.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img4='/article_image/'+req.files.img4.name;
                  }
                  if(req.files.img5&&req.files.img5.name != "")
                  {
                      fs.readFile(req.files.img5.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img5.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img5='/article_image/'+req.files.img5.name;
                  }
                  if(req.files.img6&&req.files.img6.name != "")
                  {
                      fs.readFile(req.files.img6.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img6.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img6='/article_image/'+req.files.img6.name;
                  }
                  if(req.files.img7&&req.files.img7.name != "")
                  {
                      fs.readFile(req.files.img7.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img7.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img7='/article_image/'+req.files.img7.name;
                  }

                  if(req.files.img8&&req.files.img8.name != "")
                  {
                      fs.readFile(req.files.img8.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img8.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img8='/article_image/'+req.files.img8.name;
                  }

                  if(req.files.img9&&req.files.img9.name != "")
                  {
                      fs.readFile(req.files.img9.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img9.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img9='/article_image/'+req.files.img9.name;
                  }

                  if(req.files.img10&&req.files.img10.name != "")
                  {
                      fs.readFile(req.files.img10.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img10.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img10='/article_image/'+req.files.img10.name;
                  }
                  if(req.files.img11&&req.files.img11.name != "")
                  {
                      fs.readFile(req.files.img11.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img11.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img11='/article_image/'+req.files.img11.name;
                  }
                  if(req.files.img12&&req.files.img12.name != "")
                  {
                      fs.readFile(req.files.img12.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img12.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img12='/article_image/'+req.files.img12.name;
                  }
                  if(req.files.img13&&req.files.img13.name != "")
                  {
                      fs.readFile(req.files.img13.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img13.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img13='/article_image/'+req.files.img13.name;
                  }
                  if(req.files.img14&&req.files.img14.name != "")
                  {
                      fs.readFile(req.files.img14.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img14.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img14='/article_image/'+req.files.img14.name;
                  }
                  if(req.files.img15&&req.files.img15.name != "")
                  {
                      fs.readFile(req.files.img15.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img15.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img15='/article_image/'+req.files.img15.name;
                  }
                  if(req.files.img16&&req.files.img16.name != "")
                  {
                      fs.readFile(req.files.img16.path, function (err, data) {
                        
                          fs.writeFile('public/article_image/'+req.files.img16.name,data, function (err) {
                            if (err) 
                              console.log('Error');
                              //throw err; 
                            else
                              console.log('Done success');
                          });
                        });
                      
                       user.writeArticle[i].img16='/article_image/'+req.files.img16.name;
                  }
                  user.writeArticle[i].status="Request Changed";
                user.save(function(err){
                          if(err)
                            console.log(err);
                            else
                            {
                                 User.findOne({userType:'admin'}).exec(function(err,admin){
                                  if(err)
                                    console.log(err);
                                  else
                                  { 
                                      admin.editedArticle.push(article);
                                      admin.save(function(err){
                                        if(err)
                                          console.log(err);
                                        else{
                                          res.redirect('/userArticle');
                                        }
                                      })

                                  }
                                })
                            }
                        })    
               
              }
          }
      }
    })

}
exports.changeArticle=function(req,res){
  var articleId=req.param('articleId');
  var article;
 // console.log(articleId)
    User.findOne({_id:req.user._id}).exec(function(err,user){
      if(err)
        console.log(err);
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==articleId)
              {
                  // res.render('article',{

                  // })
                  article=user.writeArticle[i];
              }
          }
          res.render('changeArticle',{
            article:article,
            author:req.user,
          })
      }
    })

}
exports.changeRequest=function(req,res){
     var authorid=req.param('author');
      var articleid=req.param('article');
      var author;
      var article;
      User.findOne({_id:authorid}).exec(function(err,user){
          if(err)
            console.log(err)
          else
          {
            for(var i=0;i<user.writeArticle.length;i++)
            {
              if(user.writeArticle[i]._id==articleid)
              {
                 author=user;
                  article=user.writeArticle[i];
              }
            }
            res.render('changeRequest',{
              author:author,
              article:article,
            })
          }
      })
}
exports.submitReason=function(req,res)
{
  var id=req.body.artID;
  console.log(req.body);
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
      for(var i=0;i<user.length;i++)
      {
        for(var j=0;j<user[i].writeArticle.length;j++)
        {
          console.log(user[i].writeArticle[j]._id+"  "+id)
          if(user[i].writeArticle[j]._id==id)
          {
              user[i].writeArticle[j].status='Rejected'
               user[i].save(function(err){
                   
                    if(err)
                      console.log("dsd"+err)
                   
                  }) 
          }
        }
      }
    }
  })

   mandrill('/messages/send', {


          "message": {
              to: [{email: req.body.email}],
              from_email: 'admin@patarboi.ca',
              subject: "Rejected Article",
              "html": "Reviewers comment: " + req.body.comment1+ "  "+ req.body.comment2 +"  "+ req.body.comment3+ "  "+"Reason Of Rejection: "+req.body.reason,
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else 
            console.log(response);
      });
   res.redirect('/completereview');

}
// not working yet
exports.reviewcomplete=function(req,res)
{

      res.render('reviewcomplete',{

       author:req.user

       });

  


}
exports.latestarticle=function(req,res)
{
    var login=0;
    var latestarticleuser;
    console.log('Test:'+req.user);
    if(req.user)
    {
      login=1;
    }

    userId=req.param('userid');
    articleId=req.param('articleid');
    console.log(userId);
    console.log(articleId);
    var article=[];
    var user_article;
    var artist;
    User.find().exec(function(err,user){
        if(err)
          console.log('Error');
        else
        {

            for(var i=0;i<user.length;i++)
            {

                if(user[i]._id == userId)
                {  
                   var name=user[i].name;
                    var country=user[i].country;
                    latestarticleuser=user[i];
                   console.log(name);
                }


                if(user[i].userType == 'admin')
                {

                    for(var k=0;k<user[i].latestArticle.length;k++)
                    {
                        if(user[i].latestArticle[k]._id == articleId)
                        {
                          
                          console.log('got it');
                          console.log(user[i].latestArticle[k].title);
                          article=user[i].latestArticle[k];

                        }

                    }
                }


            }
            for(var i=0;i<user.length;i++)
            {
              if(user[i]._id == userId)
                { 
                  for(var j=0;j<user[i].writeArticle.length;j++)
                  {
                    if(user[i].writeArticle[j]._id==articleId)
                    {
                      user_article=user[i].writeArticle[j];
                      User.findOne({_id:article.artistId}).exec(function(err,user1){
                            if(err)
                              console.log(err);
                            else{
                              console.log("err");

                              artist=user1;
                               console.log(artist);
                            }
                              
                          })
                    }
                  }
                }
            }
            res.render('latestarticle',{
              country:country,
                author:name,
                article:article,
                login:login,
                latestarticleuser:latestarticleuser,
                user_article:user_article,
                artist:artist

             });




        }
    })



}
exports.uploadletter=function(req,res){
   // var alluser;
   //  User.find().exec(function(err,user){
   //    if(user){
   //      alluser=user;
   //      res.render('upload_letter',{
   //      alluser:alluser,
   //    });
          
   //    }
   //  })

     var alluser;
      var author=req.param('author');
      var articleId=req.param('article');
      User.findOne({_id:author}).exec(function(err,user){
        if(err)
          console.log(err)
        if(user){
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==articleId)
              {
                  alluser=user;
                  writeArticle=user.writeArticle[i];

              }
          }
          
          res.render('upload_letter',{
          alluser:alluser,
          writeArticle:writeArticle,
        });
            
        }
      })

}
exports.uploadletterSubmit=function(req,res){
     fs.readFile(req.files.pdf_upload.path, function (err, data) {
            
        fs.writeFile('public/letter/'+req.files.pdf_upload.name,data, function (err) {
          if (err) 
            console.log('Error');
            //throw err; 
          else
            console.log('Done success');
        });
    });
    User.findOne({_id:req.body.user_article}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==req.body.articleWritlearticle)
              {
                  user.writeArticle[i].letterArticle='/letter/'+req.files.pdf_upload.name;
                  user.writeArticle[i].uploadLetter='Done';
              }
          }
        

      }
         user.save(function(err){
         
          if(err)
            console.log(err)
          else
          {
            console.log('Save pdf');
            res.redirect('/uploadFiles');
          }
            
        }) 
    })
}
exports.defineAsComplete=function(req,res){
  var date=new Date();
    User.findOne({_id:req.param('id')}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {

          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==req.param('articleid'))
              {
                  user.writeArticle[i].status='complete';
                   user.writeArticle[i].completeDate=date;
              }
          }
            user.save(function(err){
              if(err)
                console.log(err)
              else
              {
               res.redirect('/uploadFiles');
              }
                
            }) 
      }
    })
}
exports.uploadppt=function(req,res){
  // var alluser;
  // User.find().exec(function(err,user){
  //   if(user){
  //     alluser=user;
  //     res.render('upload_ppt',{
  //     alluser:alluser,
  //   });
        
  //   }
  // })


    var alluser;
    var author=req.param('author');
    var articleId=req.param('article');
    User.findOne({_id:author}).exec(function(err,user){
      if(err)
        console.log(err)
      if(user){
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==articleId)
            {
                alluser=user;
                writeArticle=user.writeArticle[i];

            }
        }
        
        res.render('upload_ppt',{
        alluser:alluser,
        writeArticle:writeArticle,
      });
          
      }
    })
}
exports.uploadpptSubmit=function(req,res){

     fs.readFile(req.files.pdf_upload.path, function (err, data) {
          
            fs.writeFile('public/slides/'+req.files.pdf_upload.name,data, function (err) {
              if (err) 
                console.log('Error');
                //throw err; 
              else
                console.log('Done success');
            });
        });
    User.findOne({_id:req.body.user_article}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==req.body.articleWritlearticle)
              {
                  user.writeArticle[i].pptArticle='/slides/'+req.files.pdf_upload.name;
                  user.writeArticle[i].uploadPpt='Done';
              }
          }
        

      }
         user.save(function(err){
         
          if(err)
            console.log(err)
          else
          {
            console.log('Save ppt');
            res.redirect('/uploadFiles');
          }
            
        }) 
    })
}
exports.uploadPdf=function(req,res){
  var alluser;
  var author=req.param('author');
  var articleId=req.param('article');
User.findOne({_id:author}).exec(function(err,user){
  if(err)
    console.log(err)
  if(user){
    for(var i=0;i<user.writeArticle.length;i++)
    {
        if(user.writeArticle[i]._id==articleId)
        {
            alluser=user;
            writeArticle=user.writeArticle[i];

        }
    }
    
    res.render('upload_pdf',{
    alluser:alluser,
    writeArticle:writeArticle,
  });
      
  }
})

}
exports.uploadPdfSubmit=function(req,res){
  console.log("bjhhjh");
  console.log(req.body);
   fs.readFile(req.files.pdf_upload.path, function (err, data) {
          
            fs.writeFile('public/pdf/'+req.files.pdf_upload.name,data, function (err) {
              if (err) 
                console.log('Error');
                //throw err; 
              else
                console.log('Done success');
            });
        });
    User.findOne({_id:req.body.user_article}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==req.body.articleWritlearticle)
              {
                  user.writeArticle[i].pdfArticle='/pdf/'+req.files.pdf_upload.name;
                  user.writeArticle[i].uploadPdf='Done';
              }
          }
        

      }
         user.save(function(err){
         
          if(err)
            console.log(err)
          else
          {
            console.log('Save pdf');
            res.redirect('/uploadFiles');
          }
            
        }) 
    })
}
exports.singlearticle=function(req,res){
      
      console.log(req.param('userid'));
      userId=req.param('userid');
      articleId=req.param('articleid');
      var name;
      var articleUser;
      var article=[];
      var articleReviewer=[];
      var artist;
      var login=0;
      var alluser;
      var c=0;
       if(req.user)
      {
        login=1;
      }

      User.find().exec(function(err,user){
           if(user)
           {

            for(var i=0;i<user.length;i++){
              for(var j=0;j<user[i].reviewer.length;j++){
                  if(user[i].reviewer[j].writeArticleId==articleId)
                  {
                      articleReviewer[c]=user[i];
                      c++;
                  }
              }
            }
             alluser=user;
                for(var i=0;i<user.length;i++)
                { 
                    if(user[i]._id == userId)
                    {
                        console.log(user[i].name);
                        name=user[i].name;
                        articleUser=user[i];
                        for(var k=0;k<user[i].writeArticle.length;k++)
                        {
                            if(user[i].writeArticle[k]._id == articleId)
                            {
                                console.log('Article' + user[i].writeArticle[k].title);
                                article=user[i].writeArticle[k];
                                User.findOne({_id:article.artistId}).exec(function(err,user1){
                                              if(err)
                                                console.log(err);
                                              else{
                                                console.log("err");

                                                artist=user1;
                                                 console.log(artist);
                                              }
                                                
                                            })
                                user[i].writeArticle[k].read=user[i].writeArticle[k].read + 1;
                                console.log(user[i].writeArticle[k].read);
                                user[i].save(function(err){
                                    if(err)
                                      console.log('Error');
                                    else
                                    {
                                        console.log('Save');
                                            

                                            res.render('article',{
                                            articleUser:articleUser,
                                            author:name,
                                            article:article,
                                            login:login,
                                            artist:artist,
                                            alluser:alluser,
                                            currentUser:req.user,
                                            articleReviewer:articleReviewer,

                                         });

                                    }
                                      

                                })
                            }

                        }
                    }

                }
                
                
                //console.log(author);
 

           }
           

      });

 
    //res.redirect('/article');

}

exports.editLabelCategory=function(req,res){
    console.log(req.body);
    var articleId=req.body.articleId;
    var label=req.body.label;
    var str = req.body.hiddenCategory.toString();
    var newCat=str.split(",");
    console.log(newCat);
    for(var i=0;i<newCat.length;i++)
     {
        console.log(newCat[i]);
     }

     console.log('Length cat:'+newCat.length);
     console.log(newCat);
     User.find().exec(function(err,user){
      for(var i=0;i<user.length;i++)
      {
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
              if(user[i].writeArticle[j]._id==articleId)
              {
                if(label!='label')
                  user[i].writeArticle[j].label=label;
                 if(newCat!='')
                  user[i].writeArticle[j].cetagory=newCat;
                    user[i].save(function(err){

                            if(err)
                                console.log('Error');
                            else
                                res.redirect('/manageArticle')
                        });
              }
          }
      }
     })
}
exports.uploadFiles=function(req,res){
  var count=0;
  var author=[];
   var article=[];
  User.find().exec(function(err,user){
       if(user)
       {

            for(var i=0;i<user.length;i++)
            {

              for(var j=0;j<user[i].writeArticle.length;j++)
              {
                    if(user[i].writeArticle[j].status == 'Received Author’s Response')
                    {

                       
                        author[count]=user[i];
                        article[count]=user[i].writeArticle[j];
                        count++;
                    }

              }


            }
            
            
            //console.log(author);
            res.render('uploadFiles',{
                  author:author,
                  article:article,

            }); 

       }
       

  });

}
exports.deleteComment=function(req,res){
  var articleId=req.param('article_id');
  var commentId=req.param('commentId')
  var userid;
  console.log(articleId+"   "+commentId);
  User.find().exec(function(err,user){
    for(var i=0;i<user.length;i++)
    {
        for(var j=0;j<user[i].writeArticle.length;j++)
        {
            if(articleId==user[i].writeArticle[j]._id)
            {
              userid=user[i]._id;
               for(var k=0;k<user[i].writeArticle[j].articleImage_comment.length;k++)
               {
                  if(user[i].writeArticle[j].articleImage_comment[k]._id==commentId)
                  {
                    user[i].writeArticle[j].articleImage_comment.splice(k,1);
                    user[i].save(function(err){

                                  if(err)
                                      console.log('Error');
                                  else
                                      res.redirect('/article?userid='+userid+'&articleid='+articleId)
                              });
                    
                  }
               }
            }
        }
    }
  })
}
exports.articleImage=function(req,res){

  console.log('Article Image');
  ///console.log(req.files);
  //console.log(req.body);
    fs.readFile(req.files.img.path, function (err, data) {
      
        fs.writeFile('public/article_image/'+req.files.img.name,data, function (err) {
          if (err) 
            console.log('Error');
            //throw err; 
          else
            console.log('Done success');
        });
    });

    //req.user.profilePicture='profile_pictures/' +req.files.image.name;
    req.user.save(function(err){
      if(err)
        console.log(err)
      else
      {
        console.log('Save image');
        res.send('Success');
      }
        
    }) 
}

exports.updateArticle=function(req,res){

  console.log(req.body);

  var length=req.user.writeArticle.length;
  for(var i=0;i<length;i++)
  {
    if(req.user.writeArticle[i]._id == req.body.articleIdHidden)
    {

      console.log(req.user.writeArticle[i].title);
      req.user.writeArticle[i].title=req.body.articleTitle;
      req.user.writeArticle[i].bottomText=req.body.bottom;
      req.user.writeArticle[i].credit=req.body.credit;
      req.user.writeArticle[i].article=req.body.article;
      console.log(req.user.writeArticle[i]);
      
    }
  }

  req.user.save(function(err){
    if(err)
      console.log('Error');
    else
      res.redirect('/userArticle');

  })
} 

exports.reviewsinglearticle=function(req,res)
{
  // console.log(req.param('userid'));
  //   console.log(req.param('userid'));
    var articleId=req.param('articleid');
    // console.log(req.param('articleid'));
    // console.log(req.user.reviewer.length);
    var reviewArticle;

    for(var i=0;i<req.user.reviewer.length;i++)
    {

        console.log(req.user.reviewer[i]._id);

        if(req.user.reviewer[i]._id == articleId)
        {

            console.log('Got it');
            reviewArticle=req.user.reviewer[i];
        }
    }

    //console.log(reviewArticle);
    res.render('reviewsinglearticle',{
        
        reviewArticle:reviewArticle,

  }); 

}
exports.viewPreReviwersinglearticle=function(req,res){
  var articleId=req.param('articleid');
  var reviewArticle;
  var articlecomment=[];
  var articlecommentReviewer=[];
   for(var i=0;i<req.user.reviewer.length;i++)
    {

        //console.log(req.user.reviewer[i]._id);

        if(req.user.reviewer[i]._id == articleId)
        {

           // console.log('Got it');
            reviewArticle=req.user.reviewer[i];
        }
    }
    User.findOne({_id:req.param('userid')}).exec(function(err,user){
      if(err)
        console.log(err);
      else{
        for(var k=0;k<user.writeArticle.length;k++){
          (function(k){
               //console.log(user.writeArticle[k]._id+" "+reviewArticle.writeArticleId )
                if(user.writeArticle[k]._id==reviewArticle.writeArticleId)
                {
                         articlecomment=user.writeArticle[k].articleReviewer_comment;
                         for(var m=0;m<user.writeArticle[k].articleReviewer_comment.length;m++){
                           (function(m){
                           User.findOne({_id:user.writeArticle[k].articleReviewer_comment[m].commenttator}).exec(function(err,user1){
                                if(user1){
                                  articlecommentReviewer=articlecommentReviewer.concat(user1.name);
                                  //console.log("articlecommentReviewer");
                                 
                                   if(m==user.writeArticle[k].articleReviewer_comment.length-1){
                                       console.log(articlecommentReviewer);
                                        res.render('viewPreReviwersinglearticle',{
                                        reviewArticle:reviewArticle,
                                        user:user,
                                        articlecomment:articlecomment,
                                        articlecommentReviewer:articlecommentReviewer,
                                      });
                                    }
                                }
                           });
                         })(m)
                       }
                       
                }

                // if(k==user.writeArticle.length-1){
                //     res.render('viewPreReviwersinglearticle',{
                //     reviewArticle:reviewArticle,
                //     user:user,
                //     articlecomment:articlecomment,
                //     articlecommentReviewer:articlecommentReviewer,
                //   });
                // }
          })(k)
         
        }
         
       
    }
  })
}
exports.viewReviwersinglearticle=function(req,res)
{
  console.log(req.param('userid'));
    console.log(req.param('userid'));
    var articleId=req.param('articleid');
    console.log(req.param('articleid'));
    console.log(req.user.reviewer.length);
    var reviewArticle;

    for(var i=0;i<req.user.reviewer.length;i++)
    {

        console.log(req.user.reviewer[i]._id);

        if(req.user.reviewer[i]._id == articleId)
        {

            console.log('Got it');
            reviewArticle=req.user.reviewer[i];
        }
    }
    User.findOne({_id:req.param('userid')}).exec(function(err,user){
      if(err)
        console.log(err);
      else{
        res.render('viewReviwersinglearticle',{
        
        reviewArticle:reviewArticle,
        user:user,

  });
    }
  })
    //console.log(reviewArticle);
     

}

exports.adminArticleAccept=function(req,res)
{
    console.log('Manage article')
    console.log(req.body.user_id);
    console.log(req.body.index);
    var userId=req.body.user_id;  //user _id
    var index=req.body.index;  // article _id
    var originalId=req.body.index;
    var article;
    var author;

    User.find().select().exec(function(err,user){

          if(err)
          {

            console.log('Error');
          }
          else
          {
                console.log('Found all');
                console.log(user.length);
                for(var i=0;i<user.length;i++)
                {
                    
                    if(user[i]._id == userId)
                    {

                          console.log('OOO');
                          console.log(user[i].writeArticle.length);

                          for(var k=0;k<user[i].writeArticle.length;k++)
                          {
                              if(user[i].writeArticle[k]._id == req.body.index)
                              {
                                  console.log('Got it');
                                  user[i].writeArticle[k].status='Request Reviewer';
                                  user[i].acceptedArticleCount=user[i].acceptedArticleCount+1;
                                  user[i].pendingArticleCount=user[i].pendingArticleCount-1;
                                  article=user[i].writeArticle[k];
                                  author=user[i]._id;
                                  

                                  user[i].save(function(err){
                                      if(err)
                                        console.log('Error-1');
                                  })


                              }
                          }
                    }
                }

                console.log('Author : '+author);

                for(var i=0;i<user.length;i++)
                {
                    if(user[i].userType =='admin')
                    {     


                          console.log('got admin');
                          user[i].latestArticleCount= user[i].latestArticleCount + 1;
                          console.log('Count: '+user[i].latestArticleCount);
                          var index= user[i].latestArticleCount%4;
                          
                          user[i].latestArticle.push({originalArticleId:originalId,morepage:article.morepage,_id:article._id,bookTitle:article.bookTitle,articleId:article.articleId,title:article.title,article:article.article,bottomText:article.bottomText,cetagory:article.cetagory,label:article.label,articleImage:article.articleImage,status:article.status,
                            articleTitle2:article.articleTitle2,article2:article.article2,img2:article.img2,bottomText2:article.bottomText2,articleTitle3:article.articleTitle3,article3:article.article3,img3:article.img3,bottomText3:article.bottomText3,articleTitle4:article.articleTitle4,
                            article4:article.article4,img4:article.img4,bottomText4:article.bottomText4});              
                          
                          user[i].latestArticleAuthor.push({articleAuthor:author});
                         

                          if(user[i].latestArticleCount > '4')
                          {

                              user[i].latestArticle.splice(index,1);
                              user[i].latestArticleAuthor.splice(index,1);
                              


                          }
                            user[i].save(function(err){

                                  if(err)
                                      console.log('Error');
                                  else
                                      res.send('Done');
                              });
                    }
                }





 
                 

           } 


  });



}

exports.allauthors=function(req,res)
{
      console.log('Author page');
    var name=[];
    var email=[];
    var articleNo=[];
    var count=0;
    var reviewer=[];
    var artist=[];
    User.find().exec(function(err,user)
    {
        
        if(err)
          console.log('Error');
        else
        {
            
            console.log(user.length);
            for(var i=0;i<user.length;i++)
            {
                  if(user[i].userType == 'Author')
                  {
                      name[count]=user[i].name;
                      email[count]=user[i].email;
                      articleNo[count]=user[i].writeArticle.length;
                      count++;
                    //console.log('author ami');
                  }

            }
            for(var i=0;i<user.length;i++)
            {
                if(user[i].reviewerType=='Reviewer'||user[i].reviewer.length>0)
                  reviewer.push(user[i]);
            }
            for(var i=0;i<user.length;i++)
              {
                for(var j=0;j<user[i].userSubtype.length;j++)
                {
                     if(user[i].userSubtype[j].subtype=='Artists'||user[i].selectedArtist.length>0)
                      artist.push(user[i]);
                }
              }
             res.render('allauthors',{
                  no:count,
                  name:name,
                  email:email,
                  articleNo:articleNo,
                  reviewer:reviewer,
                  artist:artist,
                }); 

        }


    });

  
}
exports.submitDraft=function(req,res)
{

    console.log(req.body);
    for(var i=0;i<req.user.writeArticle.length;i++)
    {

      if(req.user.writeArticle[i]._id == req.body.id)
      {
          console.log('Got it');
          req.user.writeArticle[i].status = 'waiting';

            req.user.pendingArticleCount=req.user.pendingArticleCount+1;
            console.log(req.user.pendingArticleCount);

     
          req.user.save(function(err){

                if(err)
                  console.log('Error');
                else
                  res.send('Done');
               // res.redirect('/userArticle');

          })
      }
    }
    
}

exports.decide2review=function(req,res)
{

    console.log('Decide to review');
    res.render('decide2review',{
      user:req.user,
    });

}

exports.adminArticleReject=function(req,res)
{
    console.log(req.body);

    User.findOne({_id: req.body.user}).select('writeArticle pendingArticleCount rejectedArticleCount').exec(function(err,user){
        if(err)
        {
          console.log('Error');
        }
        else
        {
            for(var i=0;i<user.writeArticle.length;i++)
            {

                if(user.writeArticle[i]._id == req.body.index)
                {
                    //console.log(user.writeArticle[i].title);
                    user.writeArticle[i].status='Rejected';
                    user.writeArticle[i].rejectMessage=req.body.message;
                    user.rejectedArticleCount=user.rejectedArticleCount+1;
                    user.pendingArticleCount=user.pendingArticleCount-1;
                }
            }
            //console.log(user.writeArticle);
           user.save(function(err){
              if(err)
                console.log('Error');
              else
              {
                console.log('Save successful');
                res.send('Data Delete Successful');
              }
                
            });    
            
            
        }

    });


}
exports.savedArticleAccept=function(req,res){
  var reviewStatus=req.body.status;
   User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            { 
              //console.log("got")  
              //console.log("a "+i+" "+user.length)
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                  // console.log("bb"+user[i].writeArticle[j]._id)
                    if(user[i].writeArticle[j]._id==req.body.writeArticleId)
                    {
                        //console.log('ABC')
                       // console.log(user[i].writeArticle[j].article_reviewer)
                       for(var k=0;k<user[i].writeArticle[j].article_reviewer.length;k++)
                       {
                          //console.log(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail)
                         // console.log(req.user._id)
                          if(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail==req.user.email)
                          {

                             if(reviewStatus=='Accept')
                              user[i].writeArticle[j].article_reviewer[k].status='Review complete';
                              user[i].save(function(err){
                                  if(err)
                                    console.log('Error 1');
                                  
                              })
                          }
                       }
                    }
                }
            }
        }
    })

     User.findOne({_id:req.body.author}).exec(function(err,user){
    if(err)
      console.log('Error 2');
    else
    {
          console.log(user.reviewer.length);
          
          var dateNow=new Date();
          for(var i=0;i<req.user.reviewer.length;i++)
          {
              if(req.user.reviewer[i]._id == req.body.reviewArticleId)
              {
                  if(reviewStatus=='Accept'){
                      req.user.reviewer[i].reviewer_status = 'Finish';
                      req.user.reviewer[i].reviewSubmittedDate =dateNow;
                  }
                  
                    
                    //req.user.reviewer[i].reviewSubmittedDate =dateNow;
                    req.user.save(function(err){
                      if(err)
                        console.log('Error 3');
                    })
              }
          }

          for (var i=0;i<user.writeArticle.length;i++)
          {
            if(user.writeArticle[i]._id == req.body.writeArticleId)
            {
                if(reviewStatus=='Accept'){
                        user.writeArticle[i].status='Review complete';
                        user.writeArticle[i].reviewSubmittedDate=dateNow;
                  }
                 
               
            }
          }

        user.save(function(err){
          if(err)
            console.log('Error 4');
          else
            res.send('Successfull');
        }); 


    }



  });
}
exports.reviewerArticleAccept=function(req,res)
{
  var comment1;
  var comment2;
  var comment3;
  var comment4;
  var comment5;
  var comment6;
  var comment7;
  var comment8;
  var comment9;
  var comment10;
  var comment11;
  var comment12;
  var comment13;
  var comment14;
  var comment15;
  var comment16;
  var reviewStatus=req.body.status;
  // console.log(req.user.name);
  // console.log('Accept Article');
  // console.log(req.body);
  console.log("reviewerArticleAccept")
  var comment=req.body.comment;
  if(req.body.comment1)
    comment1=req.body.comment1;
  else
    comment1="";
  if(req.body.comment2)
    comment2=req.body.comment2;
  else
    comment2="";
  if(req.body.comment3)
    comment3=req.body.comment3;
  else
    comment3="";
  if(req.body.comment4)
    comment4=req.body.comment4;
  else
    comment4="";
  if(req.body.comment5)
    comment5=req.body.comment5;
  else
    comment5="";
  if(req.body.comment6)
    comment6=req.body.comment6;
  else
    comment6="";
  if(req.body.comment7)
    comment7=req.body.comment7;
  else
    comment7="";
  if(req.body.comment8)
    comment8=req.body.comment8;
  else
    comment8="";
  if(req.body.comment9)
    comment9=req.body.comment9;
  else
    comment9="";
  if(req.body.comment10)
    comment10=req.body.comment10;
  else
    comment10="";
  if(req.body.comment11)
    comment11=req.body.comment11;
  else
    comment11="";
  if(req.body.comment12)
    comment12=req.body.comment12;
  else
    comment12="";
  if(req.body.comment13)
    comment13=req.body.comment13;
  else
    comment13="";
  if(req.body.comment14)
    comment14=req.body.comment14;
  else
    comment14="";
  if(req.body.comment15)
    comment15=req.body.comment15;
  else
    comment15="";
  if(req.body.comment16)
    comment16=req.body.comment16;
  else
    comment16="";
  var rating=parseInt(req.body.rating);
  //var index=req.body.id;
   User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            { 
              //console.log("got")  
              //console.log("a "+i+" "+user.length)
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                  // console.log("bb"+user[i].writeArticle[j]._id)
                    if(user[i].writeArticle[j]._id==req.body.writeArticleId)
                    {
                        //console.log('ABC')
                       // console.log(user[i].writeArticle[j].article_reviewer)
                       for(var k=0;k<user[i].writeArticle[j].article_reviewer.length;k++)
                       {
                          //console.log(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail)
                         // console.log(req.user._id)
                          if(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail==req.user.email)
                          {

                             if(reviewStatus=='Accept')
                              user[i].writeArticle[j].article_reviewer[k].status='Review complete';
                              else if(reviewStatus=='Save')
                                user[i].writeArticle[j].article_reviewer[k].status='Save Review';
                              else if(reviewStatus=='Condition')
                                 user[i].writeArticle[j].article_reviewer[k].status='With Condition';
                                user[i].save(function(err){
                                  if(err)
                                    console.log('Error 1');
                                  
                              })
                          }
                       }
                    }
                }
            }
        }
    })
  User.findOne({_id:req.body.author}).exec(function(err,user){
    if(err)
      console.log('Error 2');
    else
    {
          console.log(user.reviewer.length);
          
          var dateNow=new Date();
          for(var i=0;i<req.user.reviewer.length;i++)
          {
              if(req.user.reviewer[i]._id == req.body.reviewArticleId)
              {
                  if(reviewStatus=='Accept'){
                      req.user.reviewer[i].reviewer_status = 'Finish';
                      req.user.reviewer[i].reviewSubmittedDate =dateNow;
                  }
                  else if(reviewStatus=='Save'){
                    req.user.reviewer[i].reviewer_status = 'Save Review';
                  }
                  else if(reviewStatus=='Condition')
                    req.user.reviewer[i].reviewer_status='With Condition';  
                    //req.user.reviewer[i].reviewSubmittedDate =dateNow;
                    req.user.save(function(err){
                      if(err)
                        console.log('Error 3');
                    })
              }
          }

          for (var i=0;i<user.writeArticle.length;i++)
          {
            if(user.writeArticle[i]._id == req.body.writeArticleId)
            {
                if(reviewStatus=='Accept'){
                        user.writeArticle[i].status='Review complete';
                        user.writeArticle[i].reviewSubmittedDate=dateNow;
                  }
                  else if(reviewStatus=='Save'){
                    user.writeArticle[i].status = 'Save Review';
                  }
                   else if(reviewStatus=='Condition')
                    user.writeArticle[i].status='With Condition';  
               if(req.body.condition&&req.body.condition!='')
                user.writeArticle[i].articleReviewer_comment.push({condition:req.body.condition,comment:comment,commenttator:req.user._id,rating:rating,comment1:comment1,comment2:comment2,comment3:comment3,comment4:comment4,comment5:comment5,comment6:comment6,comment7:comment7,comment8:comment8,comment9:comment9,comment10:comment10,comment11:comment11,comment12:comment12,comment13:comment13,comment14:comment14,comment15:comment15,comment16:comment16});
                else{
                  user.writeArticle[i].articleReviewer_comment.push({comment:comment,commenttator:req.user._id,rating:rating,comment1:comment1,comment2:comment2,comment3:comment3,comment4:comment4,comment5:comment5,comment6:comment6,comment7:comment7,comment8:comment8,comment9:comment9,comment10:comment10,comment11:comment11,comment12:comment12,comment13:comment13,comment14:comment14,comment15:comment15,comment16:comment16});
                }
            }
          }

        user.save(function(err){
          if(err)
            console.log('Error 4');
          else
            res.send('Successfull');
        }); 


    }



  });


  
}

exports.previouslyreviewed=function(req,res)
{

    res.render('previouslyreviewed',{
          user:req.user
        });
}

exports.requestartist=function(req,res){
  var count=0;
  var author=[];
  var article=[];
  var completeSelection=[];
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else{
        for(var i=0;i<user.length;i++){
          for(var j=0;j<user[i].writeArticle.length;j++)
          {
              if(user[i].writeArticle[j].status=='Bidding Proccessing'||user[i].writeArticle[j].status=='Request Authors Approval'||user[i].writeArticle[j].status=='Received Author’s Response')
              {
                  if(user[i].writeArticle[j].selectedartistImagePage1||user[i].writeArticle[j].selectedartistImagePage2||user[i].writeArticle[j].selectedartistImagePage3||user[i].writeArticle[j].selectedartistImagePage4){
                    if(user[i].writeArticle[j].status=='Request Authors Approval')
                    completeSelection[count]=1;
                    if(user[i].writeArticle[j].status=='Received Author’s Response')
                       completeSelection[count]=2;
                  }
                  else{
                    completeSelection[count]=0;
                  }
                  author[count]=user[i];
                  article[count]=user[i].writeArticle[j];
                  count++;
              }
          }
        }
        res.render('requestartist',{
          author:author,
          article:article,
          completeSelection:completeSelection,
        })
    }
  })
  // User.find().exec(function(err,user){
  //      if(user)
  //      {

  //           for(var i=0;i<user.length;i++)
  //           {

  //             for(var j=0;j<user[i].selectedArtist.length;j++)
  //             {
  //                   if(user[i].selectedArtist[j].status == 'Finish')
  //                   {

                       
  //                       author[count]=user[i];
  //                       count++;
  //                   }

  //             }


  //           }
            
            
  //           //console.log(author);
  //           res.render('requestartist',{
                  
  //                 author:author

  //           }); 

  //      }
       

  // });



   
}
exports.searchArticle=function(req,res)
{
  var articleSearchContent=req.body.search_article;
  var article=[];
  var author=[];
  var count=0;
    console.log(articleSearchContent);
    User.find().exec(function(err,user){
      if(err)
        console.log(err);
      else
      {
          for(var i=0;i<user.length;i++)
          {
              if(user[i].name==articleSearchContent)
              {
                  for(var j=0;j<user[i].writeArticle.length;j++)
                  {   console.log(user[i].writeArticle[j].created)
                      article[count]=user[i].writeArticle[j];
                      author[count]=user[i];
                      count++;
                  }
              }
          }
          for(var i=0;i<user.length;i++)
          {
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                    if(user[i].writeArticle[j].bookTitle==articleSearchContent)
                    {
                         article[count]=user[i].writeArticle[j];
                          author[count]=user[i];
                          count++;
                    }
                    else if(user[i].writeArticle[j].status==articleSearchContent)
                    {
                         article[count]=user[i].writeArticle[j];
                          author[count]=user[i];
                          count++;

                    }
                    else if(user[i].writeArticle[j].artistName==articleSearchContent)
                    {
                         article[count]=user[i].writeArticle[j];
                          author[count]=user[i];
                          count++;

                    }
                }
          }

        
            res.render('searchedArticles',{
                article:article,
                author:author,
            });
          console.log(article.length)
      }
    })

}
exports.skipReviewer=function(req,res)
{
    console.log('ABC')
    console.log(req.param('articleId'))
    var articleId=req.param('articleId');
    User.find().exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
            for(var i=0;i<user.length;i++)
            {
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                    if(user[i].writeArticle[j]._id==articleId)
                    {
                        user[i].writeArticle[j].status='Accepted';
                        user[i].save(function(err){
                              if(err)
                                  console.log('Error');
                              else
                                  res.redirect('/manageArticle');
                        });
                    }
                }
            }
      }

    })
}
exports.showartistarticle=function(req,res)
{

    console.log(req.param('id'));
    var index=req.param('id');
    var article;
    User.find().exec(function(err,user){

          if(err)
              console.log('Error');
          else
          {

              for(var i=0;i<user.length;i++)
              {
                  
                  if(user[i].userType != 'admin')
                  {
                      for(var k=0;k<user[i].selectedArtist.length;k++)
                      {
                          if(user[i].selectedArtist[k]._id == index)
                           {
                                article=user[i].selectedArtist[k];
                                res.render('showartistarticle',{
                                    article:article,
                                });
                           }
                      }
                  }

              }

          }

    });

    
}

exports.athorapproval=function(req,res){




      res.render('athorapproval',{
        
        author:req.user

  }); 

}



exports.authorapprovalarticle=function(req,res)
{


    console.log(req.param('articleId'));
    var article;
    var name;

    for(var i=0;i<req.user.writeArticle.length;i++)
    {
        console.log(req.user.writeArticle.length);
        console.log(req.user.writeArticle[i]._id);
        if(req.user.writeArticle[i]._id == req.param('articleId'))
        {

          console.log('Done');
          article=req.user.writeArticle[i];
          name=req.user.name;
        }

    }
    console.log('Show');
    console.log(article);
    res.render('authorapprovalarticle',{
        article:article,
        name:name,
      
    });


}
exports.viewArtWork=function(req,res){
  var articleId=req.param('article');
  var author=req.param('author');
  var article;
  User.findOne({_id:author}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.writeArticle.length;i++)
        {
            if(user.writeArticle[i]._id==articleId)
            {
              article=user.writeArticle[i];
              res.render('viewArtWork',{
                article:article,
                author:author
              })
            }
        }
    }
  })
}
exports.saveCredit=function(req,res)
{
  // User.findOne({_id:req.user._id}).exec(function(err,user){
  //   if(err)
  //   {
  //     console.log(err);
  //   }
  //   else
  //   {
  //       user.creditForCompany.push({name:req.body.name,email:req.body.email});
  //       user.save(function(err){
  //         if(err)
  //           console.log(err);
  //         else
  //         {
  //           res.redirect('/managewebsite');
  //         }
  //       })
  //   }
  // })
  console.log("req.body");
  console.log(req.body);
    req.user.creditForCompany=req.body.text;

    req.user.save(function(err){
      if(err)
        console.log('Error');
      else
        console.log('Save data');
    })
    res.send('Done')
}
exports.ackAuthour=function(req,res){
  if(req.body.type=='accept')
  req.user.textackAuthour=req.body.text;
  else if(req.body.type=='reject')
  req.user.textAckRejectAuthour=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err);
    else
      console.log("save");
  })
      var email=req.body.email;
      mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Acknowledgement",
              "html": ""+req.body.text,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.remindArtistArtWork=function(req,res)
{
  req.user.textReminderArtWork=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
      console.log("save")
  })
    var email=req.body.email;
    mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Reminder For Art work",
              "html": "" + req.body.text,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.requestArtistArtWork=function(req,res)
{
  req.user.textRequestSubmitArtWork=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
      console.log("save")
  })
    var email=req.body.email;
    mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Request For Bid",
              "html": "" + req.body.text,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.requestForBid=function(req,res)
{
    var email=req.param('email');
    var articleId=req.param('article');
    var title=req.param('articleTitle');
    var status=req.param('status');
    if(status=='Request')
    {
      mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Request For Bid",
              "html": "Please submit art work for the article " + title,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
    }
    else if(status=='Reminder')
    {
      mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Reminder For Art work",
              "html": "Please submit art work for the article " + title,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
    }
     
}
exports.email_rejectedArticle=function(req,res)
{
  req.user.textemailRejectedArticle=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err);
    else
      console.log("save")
  })
    var email=req.body.email;
    var title=req.body.text;
     mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Article Reject",
              "html": "" + title ,
          },
      }, function(error, response)
      {
          if (error) {
            console.log( JSON.stringify(error) );
             res.redirect('/managewebsite')
          }
            
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.thanksReviewer=function(req,res)
{
  req.user.textThank_review_article=req.body.message;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
      console.log("save")
  })
    var email=req.body.email;
    var message=req.body.message;
     mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Thanks",
              "html": "" + message,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.thanks_submittedArticle=function(req,res)
{
    req.user.textThank_submit_article=req.body.message;
    req.user.save(function(err){
      if(err)
        console.log(err)
      else
        console.log("save")
    });
    
    var email=req.body.email;
    var title=req.body.title;
     mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Thanks",
              "html": ""+req.body.message ,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.thank_Bid=function(req,res)
{
  req.user.textThankParticipateBid=req.body.message;
    var email=req.body.email;
    var articleId=req.body.article;
    var message=req.body.message;
     mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Thanks",
              "html": "" + message,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }   
      });
}
exports.remindReviewer=function(req,res)
{
  req.user.textRemindReviewSubmit=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
      console.log("save");
  })
    var email=req.body.email;
    var title=req.body.text;
     mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Reminder",
              "html": " " + title,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }     
      });
}
exports.remindArtist=function(req,res)
{
  req.user.textRemindParticipate=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
      console.log("save");
  })
    var email=req.body.email;
    var title=req.body.text;
     mandrill('/messages/send', {
          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Reminder",
              "html": "" + title,
          },
      }, function(error, response)
      {
          if (error) 
            console.log( JSON.stringify(error) );
          else {
            console.log(response);
            res.redirect('/managewebsite')
          }        
      });
}
exports.thanksEmail=function(req,res)
{
  req.user.textThankSubmittingArt=req.body.text;
  req.user.save(function(err){
    if(err)
      console.log(err)
    else
      console.log("save")
  })
    var email=req.body.email;
    var articleId=req.body.article
    var title=req.body.text;
     mandrill('/messages/send', {


          "message": {
              to: [{email: email}],
              from_email: 'admin@patarboi.ca',
              subject: "Thanks",
              "html": "" + title,
          },
         
        
      }, function(error, response)
      {

          if (error) 
            console.log( JSON.stringify(error) );

          else {
            console.log(response);
            res.redirect('/managewebsite')

          }
            
      });

}
exports.requestAuthorsApproval=function(req,res)
{
    var authorId=req.param('author');
    var articleId=req.param('article');
    var location=req.param('location');
    console.log(location)
    var pagestandard=0;
    User.findOne({_id:authorId}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
          for(var i=0;i<user.writeArticle.length;i++)
          {
              if(user.writeArticle[i]._id==articleId)
              {
                  user.writeArticle[i].artistImage1=user.writeArticle[i].selectedartistImagePage1;
                  user.writeArticle[i].artistImage2=user.writeArticle[i].selectedartistImagePage2;
                  user.writeArticle[i].artistImage3=user.writeArticle[i].selectedartistImagePage3;
                  user.writeArticle[i].artistImage4=user.writeArticle[i].selectedartistImagePage4;
                  user.writeArticle[i].artistImage5=user.writeArticle[i].selectedartistImagePage5;
                  user.writeArticle[i].artistImage6=user.writeArticle[i].selectedartistImagePage6;
                  user.writeArticle[i].artistImage7=user.writeArticle[i].selectedartistImagePage7;
                  user.writeArticle[i].artistImage8=user.writeArticle[i].selectedartistImagePage8;
                  user.writeArticle[i].artistImage9=user.writeArticle[i].selectedartistImagePage9;
                  user.writeArticle[i].artistImage10=user.writeArticle[i].selectedartistImagePage10;
                  user.writeArticle[i].artistImage11=user.writeArticle[i].selectedartistImagePage11;
                  user.writeArticle[i].artistImage12=user.writeArticle[i].selectedartistImagePage12;
                  user.writeArticle[i].artistImage13=user.writeArticle[i].selectedartistImagePage13;
                  user.writeArticle[i].artistImage14=user.writeArticle[i].selectedartistImagePage14;
                  user.writeArticle[i].artistImage15=user.writeArticle[i].selectedartistImagePage15;
                  user.writeArticle[i].artistImage16=user.writeArticle[i].selectedartistImagePage16;

                  for(var j=0;j<user.writeArticle[i].bidding_Article.length;j++)
                  {
                    pagestandard=0;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage1)
                        pagestandard=pagestandard+1;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage2)
                        pagestandard=pagestandard+1;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage3)
                        pagestandard=pagestandard+1;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage4)
                        pagestandard=pagestandard+1;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage5)
                        pagestandard=pagestandard+1;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage6)
                        pagestandard=pagestandard+1;
                      if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage7)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage8)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage9)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage10)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage11)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage12)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage13)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage14)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage15)
                        pagestandard=pagestandard+1;
                       if(user.writeArticle[i].bidding_Article[j].artistId==user.writeArticle[i].selectedartistPage16)
                        pagestandard=pagestandard+1;

                      user.writeArticle[i].bidding_Article[j].standardPage=pagestandard;
                  }
                 // user.writeArticle[i].artistName=req.body.artistname;
                  user.writeArticle[i].status='Request Authors Approval';
                  user.save(function(err){
                      if(err)
                          console.log('Error');
                      else{
                        if(location=='requestartist')
                          res.redirect('/requestartist');
                        else if(location=='managewebsite')
                          res.redirect('/managewebsite');
                      }
                          
                });
              }
          }
      }
    })
}
exports.requestAuthor=function(req,res){

  console.log(req.body);

  var artist=req.body.image2;
  var index=req.body.articleId;

  User.find().exec(function(err,user){

        if(err)
            console.log('Error');
        else
            {

                for(var i=0;i<user.length;i++)
                {
                    if(user[i].userType != 'admin')
                    {

                        for(var k=0;k<user[i].writeArticle.length;k++)
                        {

                            if(user[i].writeArticle[k]._id ==index)
                            {
                                console.log('Got it');
                                user[i].writeArticle[k].artistImage1=req.body.image1;
                                user[i].writeArticle[k].artistImage2=req.body.image2;
                                user[i].writeArticle[k].artistImage3=req.body.image3;
                                user[i].writeArticle[k].artistImage4=req.body.image4;
                                user[i].writeArticle[k].artistId=req.body.artist;
                                
                                user[i].writeArticle[k].artistName=req.body.artistname;
                                user[i].writeArticle[k].status='Request Authors Approval';
                                user[i].save(function(err){
                                    if(err)
                                        console.log('Error');
                                    else
                                        res.send('Done');
                              });

                            }
                        }
                    }

                }



            }
  })




}

exports.artistarticle=function(req,res)
{
      //console.log(req.param('userid'));
      articleId=req.param('articleId');

      console.log(req.user._id);
      console.log(req.user.selectedArtist.length);
      var article;

      for(var i=0;i<req.user.selectedArtist.length;i++)
      {

          if(req.user.selectedArtist[i]._id == articleId)
          {

              console.log(req.user.selectedArtist[i].title);
              article=req.user.selectedArtist[i];
          }
      }

      res.render('artistarticle',{

        article:article,
      })
 

}
exports.artwork=function(req,res)
{
      //console.log(req.param('userid'));
      articleId=req.param('articleId');

      console.log(req.user._id);
      console.log(req.user.selectedArtist.length);
      var article;
      var article1;
      for(var i=0;i<req.user.selectedArtist.length;i++)
      {

          if(req.user.selectedArtist[i]._id == articleId)
          {

              console.log(req.user.selectedArtist[i].title);
              article=req.user.selectedArtist[i];
          }
      }
      User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
          for(var i=0;i<user.length;i++)
          {
            for(var j=0;j<user[i].writeArticle.length;j++)
            {
                if(user[i].writeArticle[j]._id==articleId)
                {
                  article1=user[i].writeArticle[j];
                   res.render('artwork',{
                      article:article,
                      article1:article1,
                      author:user[i],
                    })
                }
            }
          }
        }
      })
     
 

}
exports.assign_Artist=function(req,res){

    console.log(req.body);
    var artist_1=req.body.artist_1;
    var articleId=req.body.articleId;
    var writeArticleId=req.body.writeArticleId;
    var authorId=req.body.authorId;
    var article;
    
    var newdate=new Date();
    User.findOne({_id:req.body.authorId}).exec(function(err,user){

        if(err)
          console.log('Error');
        else
          {
              //console.log(user.name);

              for(var i=0;i<user.writeArticle.length;i++)
              {
                  if(user.writeArticle[i]._id == req.body.writeArticleId)
                  {
                      user.writeArticle[i].status='Request Artist';
                      user.writeArticle[i].startDate=newdate;
                      user.writeArticle[i].endDate=req.body.dueDate;
                      user.writeArticle[i].noOfArtist=user.writeArticle[i].noOfArtist + 1;
                      user.writeArticle[i].article_artist.push({articleArtistemail:artist_1,status:' Waiting for Submission'})
                      article=user.writeArticle[i];
                  }

              }

                   user.save(function(err){
                      if(err)
                        console.log('Save Error');
                      else 
                        {
                            console.log('Save successfull');
                           // res.send('Successful');

                        }
                  });
              User.find().exec(function(err,artist){
                  if(err)
                    console.log('Artist Not Found');
                  else
                  {

                      
                      for(var i=0;i<artist.length;i++)
                      {

                          if(artist[i].email == artist_1)
                          {

                            console.log('Got it');
                            // console.log(artist[i].name);
                            // console.log(article);
                            artist[i].selectedArtist.push(article);
                            artist[i].selectedArtistId.push({id:article._id,startDate:newdate,endDate:req.body.dueDate});
                            
                          }
                           artist[i].save(function(err){
                              if(err)
                                console.log('Error');
                              // else{
                              //  // res.send('Send');

                              // }
                            });

                      }

                     
                  }

              }); 

              res.send('Send');
          
          }
    });


}
exports.selectReviewer=function(req,res)   // select reviewer for submitted post
{    console.log('req.body.reviewer')
     console.log(req.body.reviewer)
    var reviewUser1;
     User.find().exec(function(err,user1){
          if(err)
            console.log(err);
          else {
             for(var i=0;i<user1.length;i++)
                  {
                    //console.log(user1.length)
                    if(user1[i].email == req.body.reviewer)
                    {
                      user1.reviewerType='Reviewer';
                      reviewUser1=user1;
                      console.log(reviewUser1)
                      user1[i].save(function(err){
                        if(err)
                          console.log(err);
                        else{
                          console.log("save Reviewer");
                              User.findOne({_id: req.body.userid}).exec(function(err,user){
                                if(err)
                                  console.log('Error');
                                else
                                {
                                  var count=0;
                                  
                                  

                                  for(var i=0;i<user.writeArticle.length && count==0;i++)
                                  {

                                        if(user.writeArticle[i]._id == req.body.articleId)
                                        {
                                          console.log('reviewUser1')
                                          console.log(reviewUser1)
                                            count=1;
                                            user.writeArticle[i].noOfreviewer=user.writeArticle[i].noOfreviewer+1
                                            user.writeArticle[i].article_reviewer.push({articleReviewerId:reviewUser1._id,articleRevieweremail:req.body.reviewer,status:'pending',dueDate:req.body.dueDate})
                                            user.save(function(err){
                                              if(err)
                                                console.log('Error');
                                            })
                                            var article=user.writeArticle[i];
                                            var articleId=user.writeArticle[i].articleId;
                                            var status=user.writeArticle[i].status;
                                            
                                            var bookTitle=user.writeArticle[i].bookTitle;
                                            var bookTitleSound=user.writeArticle[i].bookTitleSound;//
                                            var articleTitle=user.writeArticle[i].title;
                                            var articleTitleSound=user.writeArticle[i].title1Sound;//
                                           

                                            var article= user.writeArticle[i].article;
                                            var articleSound=user.writeArticle[i].topSubText1Sound;// 
                                         

                                            var credit=user.writeArticle[i].credit;
                                            var bottomText=user.writeArticle[i].bottomText;
                                            var bottomTextSound=user.writeArticle[i].topBottomText1Sound;//
                                            var image=user.writeArticle[i].articleImage;
                                            

                                            var reviewArticleId=user.writeArticle[i]._id;
                                            var writeArticleId=user.writeArticle[i]._id;


                                            //2nd page:
                                            var articleTitle2= user.writeArticle[i].articleTitle2;
                                            var articleTitle2Sound= user.writeArticle[i].articleTitle2Sound;//
                                            var article2= user.writeArticle[i].article2;
                                            var article2Sound= user.writeArticle[i].article2Sound;//
                                            var img2= user.writeArticle[i].img2;
                                            var source2= user.writeArticle[i].source2;
                                            var bottomText2= user.writeArticle[i].bottomText2;
                                            var bottomText2Sound= user.writeArticle[i].bottomText2Sound;

                                            //3rd page=
                                            var articleTitle3= user.writeArticle[i].articleTitle3;
                                            var articleTitle3Sound= user.writeArticle[i].articleTitle3Sound;//
                                            var article3= user.writeArticle[i].article3;
                                            var article3Sound= user.writeArticle[i].article3Sound;//
                                            var img3= user.writeArticle[i].img3;
                                            var source3= user.writeArticle[i].source3;
                                            var bottomText3= user.writeArticle[i].bottomText3;
                                            var bottomText3Sound= user.writeArticle[i].bottomText3Sound;//

                                            //4th page=
                                            var articleTitle4= user.writeArticle[i].articleTitle4;
                                            var articleTitle4Sound= user.writeArticle[i].articleTitle4Sound;//
                                            var article4= user.writeArticle[i].article4;
                                            var article4Sound= user.writeArticle[i].article4Sound;//
                                            var img4= user.writeArticle[i].img4;
                                            var source4= user.writeArticle[i].source4;
                                            var bottomText4= user.writeArticle[i].bottomText4;
                                            var bottomText4Sound= user.writeArticle[i].bottomText4Sound;//new
                                            //5th page=
                                            var articleTitle5= user.writeArticle[i].articleTitle5;
                                            var articleTitle5Sound= user.writeArticle[i].articleTitle5Sound;//
                                            var article5= user.writeArticle[i].article5;
                                            var article5Sound= user.writeArticle[i].article5Sound;//
                                            var img5= user.writeArticle[i].img5;
                                            var source5= user.writeArticle[i].source5;
                                            var bottomText5= user.writeArticle[i].bottomText5;
                                            var bottomText5Sound= user.writeArticle[i].bottomText5Sound;//new

                                            //6th page=
                                            var articleTitle6= user.writeArticle[i].articleTitle6;
                                            var articleTitle6Sound= user.writeArticle[i].articleTitle6Sound;//
                                            var article6= user.writeArticle[i].article6;
                                            var article6Sound= user.writeArticle[i].article6Sound;//
                                            var img6= user.writeArticle[i].img6;
                                            var source6= user.writeArticle[i].source6;
                                            var bottomText6= user.writeArticle[i].bottomText6;
                                            var bottomText6Sound= user.writeArticle[i].bottomText6Sound;//new

                                            //7th page=
                                            var articleTitle7= user.writeArticle[i].articleTitle7;
                                            var articleTitle7Sound= user.writeArticle[i].articleTitle7Sound;//
                                            var article7= user.writeArticle[i].article7;
                                            var article7Sound= user.writeArticle[i].article7Sound;//
                                            var img7= user.writeArticle[i].img7;
                                            var source7= user.writeArticle[i].source7;
                                            var bottomText7= user.writeArticle[i].bottomText7;
                                            var bottomText7Sound= user.writeArticle[i].bottomText7Sound;//new

                                            //8th page=
                                            var articleTitle8= user.writeArticle[i].articleTitle8;
                                            var articleTitle8Sound= user.writeArticle[i].articleTitle8Sound;//
                                            var article8= user.writeArticle[i].article8;
                                            var article8Sound= user.writeArticle[i].article8Sound;//
                                            var img8= user.writeArticle[i].img8;
                                            var source8= user.writeArticle[i].source8;
                                            var bottomText8= user.writeArticle[i].bottomText8;
                                            var bottomText8Sound= user.writeArticle[i].bottomText8Sound;//new

                                            //9th page=
                                            var articleTitle9= user.writeArticle[i].articleTitle9;
                                            var articleTitle9Sound= user.writeArticle[i].articleTitle9Sound;//
                                            var article9= user.writeArticle[i].article9;
                                            var article9Sound= user.writeArticle[i].article9Sound;//
                                            var img9= user.writeArticle[i].img9;
                                            var source9= user.writeArticle[i].source9;
                                            var bottomText9= user.writeArticle[i].bottomText9;
                                            var bottomText9Sound= user.writeArticle[i].bottomText9Sound;//new

                                            //10th page=
                                            var articleTitle10= user.writeArticle[i].articleTitle10;
                                            var articleTitle10Sound= user.writeArticle[i].articleTitle10Sound;//
                                            var article10= user.writeArticle[i].article10;
                                            var article10Sound= user.writeArticle[i].article10Sound;//
                                            var img10= user.writeArticle[i].img10;
                                            var source10= user.writeArticle[i].source10;
                                            var bottomText10= user.writeArticle[i].bottomText10;
                                            var bottomText10Sound= user.writeArticle[i].bottomText10Sound;//new

                                            //11th page=
                                            var articleTitle11= user.writeArticle[i].articleTitle11;
                                            var articleTitle11Sound= user.writeArticle[i].articleTitle11Sound;//
                                            var article11= user.writeArticle[i].article11;
                                            var article11Sound= user.writeArticle[i].article11Sound;//
                                            var img11= user.writeArticle[i].img11;
                                            var source11= user.writeArticle[i].source11;
                                            var bottomText11= user.writeArticle[i].bottomText11;
                                            var bottomText11Sound= user.writeArticle[i].bottomText11Sound;//new

                                            //12th page=
                                            var articleTitle12= user.writeArticle[i].articleTitle12;
                                            var articleTitle12Sound= user.writeArticle[i].articleTitle12Sound;//
                                            var article12= user.writeArticle[i].article12;
                                            var article12Sound= user.writeArticle[i].article12Sound;//
                                            var img12= user.writeArticle[i].img12;
                                            var source12= user.writeArticle[i].source12;
                                            var bottomText12= user.writeArticle[i].bottomText12;
                                            var bottomText12Sound= user.writeArticle[i].bottomText12Sound;//new

                                            //13th page=
                                            var articleTitle13= user.writeArticle[i].articleTitle13;
                                            var articleTitle13Sound= user.writeArticle[i].articleTitle13Sound;//
                                            var article13= user.writeArticle[i].article13;
                                            var article13Sound= user.writeArticle[i].article13Sound;//
                                            var img13= user.writeArticle[i].img13;
                                            var source13= user.writeArticle[i].source13;
                                            var bottomText13= user.writeArticle[i].bottomText13;
                                            var bottomText13Sound= user.writeArticle[i].bottomText13Sound;//new

                                            //14th page=
                                            var articleTitle14= user.writeArticle[i].articleTitle14;
                                            var articleTitle14Sound= user.writeArticle[i].articleTitle14Sound;//
                                            var article14= user.writeArticle[i].article14;
                                            var article14Sound= user.writeArticle[i].article14Sound;//
                                            var img14= user.writeArticle[i].img14;
                                            var source14= user.writeArticle[i].source14;
                                            var bottomText14= user.writeArticle[i].bottomText14;
                                            var bottomText14Sound= user.writeArticle[i].bottomText14Sound;//new

                                            //15th page=
                                            var articleTitle15= user.writeArticle[i].articleTitle15;
                                            var articleTitle15Sound= user.writeArticle[i].articleTitle15Sound;//
                                            var article15= user.writeArticle[i].article15;
                                            var article15Sound= user.writeArticle[i].article15Sound;//
                                            var img15= user.writeArticle[i].img15;
                                            var source15= user.writeArticle[i].source15;
                                            var bottomText15= user.writeArticle[i].bottomText15;
                                            var bottomText15Sound= user.writeArticle[i].bottomText15Sound;//new

                                            //16th page=
                                            var articleTitle16= user.writeArticle[i].articleTitle16;
                                            var articleTitle16Sound= user.writeArticle[i].articleTitle16Sound;//
                                            var article16= user.writeArticle[i].article16;
                                            var article16Sound= user.writeArticle[i].article16Sound;//
                                            var img16= user.writeArticle[i].img16;
                                            var source16= user.writeArticle[i].source16;
                                            var bottomText16= user.writeArticle[i].bottomText16;
                                            var bottomText16Sound= user.writeArticle[i].bottomText16Sound;//new

                                            var sound=user.writeArticle[i].sound;



                                            //console.log(articleTitleSound);

                                            User.find().exec(function(err,reviewUser_1){
                                                if(err)
                                                  console.log('reviewUser_1  Error');
                                                else
                                                {

                                                    for(var i=0;i<reviewUser_1.length;i++)
                                                    {

                                                          if(reviewUser_1[i].email == req.body.reviewer)
                                                          {
                                                              mandrill('/messages/send', {
                                                              "message": {
                                                                  to: [{email: reviewUser_1[i].email}],
                                                                  from_email: 'admin@patarboi.ca',
                                                                  subject: "Review Article",
                                                                  "html": "You have requested to review a article. Click this link to review the article.: 162.243.133.214/review?articleid=" + req.body.articleId,
                                                              },
                                                             
                                                            
                                                          }, function(error, response)
                                                          {

                                                              if (error) 
                                                                console.log( JSON.stringify(error) );

                                                              else {
                                                                console.log(response);
                                                                // res.redirect('/managewebsite')

                                                              }
                                                                
                                                          });
                                                                  console.log(articleTitleSound);
                                                                  reviewUser_1[i].reviewer.push({dueDate:req.body.dueDate,bookTitle:bookTitle,bookTitleSound:bookTitleSound,articleId:articleId,reviewer_status:'pending',title: articleTitle,title1Sound:articleTitleSound,article:article,topSubText1Sound:articleSound ,credit:credit,bottomText:bottomText,topBottomText1Sound:bottomTextSound,articleImage:image,articleAthor:user._id,reviewid:reviewArticleId,writeArticleId:writeArticleId,
                                                                          articleTitle2:articleTitle2,articleTitle2Sound:articleTitle2Sound,article2:article2,article2Sound:article2Sound,img2:img2 ,source2:source2 ,bottomText2:bottomText2,bottomText2Sound:bottomText2Sound,articleTitle3:articleTitle3,articleTitle3Sound:articleTitle3Sound,article3:article3,article3Sound:article3Sound,img3:img3,source3:source3,bottomText3:bottomText3,bottomText3Sound:bottomText3Sound ,
                                                                          articleTitle4:articleTitle4, articleTitle4Sound:articleTitle4Sound,article4:article4,article4Sound:article4Sound,img4:img4,source4:source4,bottomText4:bottomText4,bottomText4Sound:bottomText4Sound,
                                                                          articleTitle5:articleTitle5, articleTitle5Sound:articleTitle5Sound,article5:article5,article5Sound:article5Sound,img5:img5,source5:source5,bottomText5:bottomText5,bottomText5Sound:bottomText5Sound,
                                                                          articleTitle6:articleTitle6, articleTitle6Sound:articleTitle6Sound,article6:article6,article6Sound:article6Sound,img6:img6,source6:source6,bottomText6:bottomText6,bottomText6Sound:bottomText6Sound,
                                                                          articleTitle7:articleTitle7, articleTitle7Sound:articleTitle7Sound,article7:article7,article7Sound:article7Sound,img7:img7,source7:source7,bottomText7:bottomText7,bottomText7Sound:bottomText7Sound,
                                                                          articleTitle8:articleTitle8, articleTitle8Sound:articleTitle8Sound,article8:article8,article8Sound:article8Sound,img8:img8,source8:source8,bottomText8:bottomText8,bottomText8Sound:bottomText8Sound,
                                                                          articleTitle9:articleTitle9, articleTitle9Sound:articleTitle9Sound,article9:article9,article9Sound:article9Sound,img9:img9,source9:source9,bottomText9:bottomText9,bottomText9Sound:bottomText9Sound,
                                                                          articleTitle10:articleTitle10, articleTitle10Sound:articleTitle10Sound,article10:article10,article10Sound:article10Sound,img10:img10,source10:source10,bottomText10:bottomText10,bottomText10Sound:bottomText10Sound,
                                                                          articleTitle11:articleTitle11, articleTitle11Sound:articleTitle11Sound,article11:article11,article11Sound:article11Sound,img11:img11,source11:source11,bottomText11:bottomText11,bottomText11Sound:bottomText11Sound,
                                                                          articleTitle12:articleTitle12, articleTitle12Sound:articleTitle12Sound,article12:article12,article12Sound:article12Sound,img12:img12,source12:source12,bottomText12:bottomText12,bottomText12Sound:bottomText12Sound,
                                                                          articleTitle13:articleTitle13, articleTitle13Sound:articleTitle13Sound,article13:article13,article13Sound:article13Sound,img13:img13,source13:source13,bottomText13:bottomText13,bottomText13Sound:bottomText13Sound,
                                                                          articleTitle14:articleTitle14, articleTitle14Sound:articleTitle14Sound,article14:article14,article14Sound:article14Sound,img14:img14,source14:source14,bottomText14:bottomText14,bottomText14Sound:bottomText14Sound,
                                                                          articleTitle15:articleTitle15, articleTitle15Sound:articleTitle15Sound,article15:article15,article15Sound:article15Sound,img15:img15,source15:source15,bottomText15:bottomText15,bottomText15Sound:bottomText15Sound,
                                                                          articleTitle16:articleTitle16, articleTitle16Sound:articleTitle16Sound,article16:article16,article16Sound:article16Sound,img16:img16,source16:source16,bottomText16:bottomText16,bottomText16Sound:bottomText16Sound,sound:sound});
                                                                  
                                                                  console.log(reviewUser_1[i].reviewer[0]);

                                                                  reviewUser_1[i].save(function(err)
                                                                  {

                                                                      if(err)
                                                                        res.send('Error');
                                                                      else
                                                                      {
                                                                        console.log("got my reviewer");
                                                                        res.send('Review data saved successful');
                                                                      }
                                                                        
                                                                   });
                                                          }

                                                    }

                                                }
                                  

                                            });  // end reviewer-1 add


                                        }

                                  }
                                }
                              

                              });
                             
                        }
                      })
                    }
                  }
           }
         })
      console.log(reviewUser1)
   // console.log(req.body);

}



exports.reviewerpanel=function(req,res)
{
    var waiting=0;
    var waiting_review=0;
    var review_so_far=0;
    var savedReview=0;
    for(var i=0;i<req.user.reviewer.length;i++)
    {
      if(req.user.reviewer[i].reviewer_status == 'pending')
      {
        waiting++;
      }
    }
    for(var i=0;i<req.user.reviewer.length;i++)
    {
      if(req.user.reviewer[i].reviewer_status == 'Accept')
      {
        waiting_review++;
      }
    }
    for(var i=0;i<req.user.reviewer.length;i++)
    {
      if(req.user.reviewer[i].reviewer_status == 'Finish')
      {
        review_so_far++;
      }
    }
    for(var i=0;i<req.user.reviewer.length;i++)
    {
      if(req.user.reviewer[i].reviewer_status == 'Save Review')
      {
        savedReview++;
      }
    }
    res.render('reviewerpanel',{
        user:req.user,
        waiting:waiting,
        waiting_review:waiting_review,
        review_so_far:review_so_far,
        savedReview:savedReview,
    });

}
exports.reviewSave=function(req,res){
   res.render('SaveReviewed',{
      user:req.user
    });
}
exports.reviewarticlestatuschange=function(req,res)
{

    console.log(req.body);
    User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            { 
              //console.log("got")  
              console.log("a "+i+" "+user.length)
                for(var j=0;j<user[i].writeArticle.length;j++)
                {
                   console.log("bb"+user[i].writeArticle[j]._id)
                    if(user[i].writeArticle[j]._id==req.body.writeArticleId)
                    {
                        //console.log('ABC')
                       // console.log(user[i].writeArticle[j].article_reviewer)
                       for(var k=0;k<user[i].writeArticle[j].article_reviewer.length;k++)
                       {
                          console.log(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail)
                          console.log(req.user._id)
                          if(user[i].writeArticle[j].article_reviewer[k].articleRevieweremail==req.user.email)
                          {
                              user[i].writeArticle[j].article_reviewer[k].status='Working';
                              user[i].save(function(err){
                                  if(err)
                                    console.log('Error');
                                  
                              })
                          }
                       }
                    }
                }
            }
        }
    })

    for(var i=0;i<req.user.reviewer.length;i++)
    {

        if(req.user.reviewer[i]._id == req.body.articleId)
        {

            console.log('Match');
            req.user.reviewer[i].reviewer_status ='Accept';
            req.user.save(function(err){
                if(err)
                  console.log('Error');
                else
                {
                    res.send('Status change');

                }
            })

        }
    }
    
}



exports.editsubmit=function(req,res)
{
     console.log(req.body.hiddenCategory);

     var str = req.body.hiddenCategory.toString();
     var newCat=str.split(",");
     console.log(newCat);
     for(var i=0;i<newCat.length;i++)
     {
        console.log(newCat[i]);
     }

     console.log('Length cat:'+newCat.length);
     console.log(newCat);

     var dpic;
     if(req.body.default_image == '1')
     {
        dpic=req.files.img.name;
     }
     else if(req.body.default_image == '2')
     {
        dpic=req.files.img2.name;

     }
     else if(req.body.default_image == '3')
     {
        dpic=req.files.img3.name;

     }
     else if(req.body.default_image == '4')
     {
        dpic=req.files.img4.name;

     }
    else if(req.body.default_image == '5')
     {
        dpic=req.files.img5.name;

     }
    else if(req.body.default_image == '6')
     {
        dpic=req.files.img6.name;

     }
     else if(req.body.default_image == '7')
     {
        dpic=req.files.img7.name;

     }
    else if(req.body.default_image == '8')
     {
        dpic=req.files.img8.name;

     }
    else if(req.body.default_image == '9')
     {
        dpic=req.files.img9.name;
     }
     else if(req.body.default_image == '10')
     {
        dpic=req.files.img10.name;

     }
     else if(req.body.default_image == '11')
     {
        dpic=req.files.img11.name;

     }
     else if(req.body.default_image == '12')
     {
        dpic=req.files.img12.name;

     }
    else if(req.body.default_image == '13')
     {
        dpic=req.files.img13.name;

     }
    else if(req.body.default_image == '14')
     {
        dpic=req.files.img14.name;

     }
     else if(req.body.default_image == '15')
     {
        dpic=req.files.img15.name;

     }
    else if(req.body.default_image == '16')
     {
        dpic=req.files.img16.name;

     }

                fs.readFile(req.files.img.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
                fs.readFile(req.files.img2.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img2.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
                fs.readFile(req.files.img3.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img3.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });

                fs.readFile(req.files.img4.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img4.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
    // if(req.body.morepage == 1 || req.body.morepage == 2 || req.body.morepage == 3) // for 5-8
    //  {
              if(req.files.img5.name){
                fs.readFile(req.files.img5.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img5.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
            }
                if(req.files.img6.name){
                fs.readFile(req.files.img6.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img6.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
              if(req.files.img7.name){
                fs.readFile(req.files.img7.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img7.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
              if(req.files.img8.name){
                fs.readFile(req.files.img8.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img8.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
    //  }
    //  else if(req.body.morepage ==2 || req.body.morepage ==3)
      {
                if(req.files.img9.name){
                fs.readFile(req.files.img9.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img9.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
               if(req.files.img10.name){
                fs.readFile(req.files.img10.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img10.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
               if(req.files.img11.name){
                fs.readFile(req.files.img11.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img11.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
               if(req.files.img12.name){
                fs.readFile(req.files.img12.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img12.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }

      }
   //   else if(req.body.morepage == 3)
      {
                 if(req.files.img13.name){
                fs.readFile(req.files.img13.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img13.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
               if(req.files.img14.name){
                fs.readFile(req.files.img14.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img14.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
               if(req.files.img15.name){
                fs.readFile(req.files.img15.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img15.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }
               if(req.files.img16.name){
                fs.readFile(req.files.img16.path, function (err, data) {
                
                  fs.writeFile('public/article_image/'+req.files.img16.name,data, function (err) {
                    if (err) 
                      console.log(err);
                      //throw err; 
                    else
                      console.log('Done success');
                  });
                });
              }


      }

    for(var i=0;i<req.user.writeArticle.length;i++)
    {
        if(req.user.writeArticle[i]._id == req.body.articleId)
        {

              console.log('Got it');
              console.log(req.body.morepage);
              req.user.writeArticle[i].status=req.body.draft;
              req.user.writeArticle[i].morepage=req.body.morepage;
              req.user.writeArticle[i].NickName=req.body.nick_name;
              req.user.writeArticle[i].artistName=req.body.artist_name;
              if(dpic!='')
               req.user.writeArticle[i].homePic=dpic;
             if(newCat!='')
              req.user.writeArticle[i].cetagory=newCat;
              //1st page edit---

              req.user.writeArticle[i].bookTitle = req.body.titleofBook;
              req.user.writeArticle[i].title = req.body.articleTitle;
              req.user.writeArticle[i].article = req.body.article;
              req.user.writeArticle[i].credit = req.body.source;
              req.user.writeArticle[i].bottomText = req.body.bottomText;
               if(req.files.img.name)
                  req.user.writeArticle[i].articleImage='/article_image/'+req.files.img.name;
              //2nd page edit----
              req.user.writeArticle[i].articleTitle2 = req.body.articleTitle2;
              req.user.writeArticle[i].article2 = req.body.article2;
              req.user.writeArticle[i].source2 = req.body.source2;
              req.user.writeArticle[i].bottomText2 = req.body.bottomText2;
               if(req.files.img2.name!='')
                  req.user.writeArticle[i].img2='/article_image/'+req.files.img2.name;
              //3rd page edit-----
              req.user.writeArticle[i].articleTitle3 = req.body.articleTitle3;
              req.user.writeArticle[i].article3 = req.body.article3;
              req.user.writeArticle[i].source3 = req.body.source3;
              req.user.writeArticle[i].bottomText3 = req.body.bottomText3;
               if(req.files.img3.name!='')
                  req.user.writeArticle[i].img3='/article_image/'+req.files.img3.name;
              //4th page edit-----
              req.user.writeArticle[i].articleTitle4 = req.body.articleTitle4;
              req.user.writeArticle[i].article4 = req.body.article4;
              req.user.writeArticle[i].source4 = req.body.source4;
              req.user.writeArticle[i].bottomText4 = req.body.bottomText4;
               if(req.files.img4.name)
                  req.user.writeArticle[i].img4='/article_image/'+req.files.img4.name;
            //   if(req.body.morepage == 1 || req.body.morepage == 2 || req.body.morepage == 3) 
              {
                  //5th page edit-----
                  req.user.writeArticle[i].articleTitle5 = req.body.articleTitle5;
                  req.user.writeArticle[i].article5 = req.body.article5;
                  req.user.writeArticle[i].source5 = req.body.source5;
                  req.user.writeArticle[i].bottomText5 = req.body.bottomText5;
                  if(req.files.img5.name)
                  req.user.writeArticle[i].img5='/article_image/'+req.files.img5.name;
                  //6th page edit-------
                  req.user.writeArticle[i].articleTitle6 = req.body.articleTitle6;
                  req.user.writeArticle[i].article6 = req.body.article6;
                  req.user.writeArticle[i].source6 = req.body.source6;
                  req.user.writeArticle[i].bottomText6 = req.body.bottomText6;
                  if(req.files.img6.name)
                  req.user.writeArticle[i].img6='/article_image/'+req.files.img6.name;
                  //7th page edit-------
                  req.user.writeArticle[i].articleTitle7 = req.body.articleTitle7;
                  req.user.writeArticle[i].article7 = req.body.article7;
                  req.user.writeArticle[i].source7 = req.body.source7;
                  req.user.writeArticle[i].bottomText7 = req.body.bottomText7;
                  if(req.files.img7.name)
                  req.user.writeArticle[i].img7='/article_image/'+req.files.img7.name;
                  //8th page edit--------
                  req.user.writeArticle[i].articleTitle8 = req.body.articleTitle8;
                  req.user.writeArticle[i].article8 = req.body.article8;
                  req.user.writeArticle[i].source8 = req.body.source8;
                  req.user.writeArticle[i].bottomText8 = req.body.bottomText8;
                  if(req.files.img8.name)
                  req.user.writeArticle[i].img8='/article_image/'+req.files.img8.name;
            }
         //      if(req.body.morepage == 2 || req.body.morepage == 3)
              {
              //9th page edit--------
                  req.user.writeArticle[i].articleTitle9 = req.body.articleTitle9;
                  req.user.writeArticle[i].article9 = req.body.article9;
                  req.user.writeArticle[i].source9 = req.body.source9;
                  req.user.writeArticle[i].bottomText9 = req.body.bottomText9;
                   if(req.files.img9.name)
                  req.user.writeArticle[i].img9='/article_image/'+req.files.img9.name;
                  //10th page-------
                  req.user.writeArticle[i].articleTitle10 = req.body.articleTitle10;
                  req.user.writeArticle[i].article10 = req.body.article10;
                  req.user.writeArticle[i].source10 = req.body.source10;
                  req.user.writeArticle[i].bottomText10 = req.body.bottomText10;
                   if(req.files.img10.name)
                  req.user.writeArticle[i].img10='/article_image/'+req.files.img10.name;
                  //11th page--------
                  req.user.writeArticle[i].articleTitle11 = req.body.articleTitle11;
                  req.user.writeArticle[i].article11 = req.body.article11;
                  req.user.writeArticle[i].source11 = req.body.source11;
                  req.user.writeArticle[i].bottomText11 = req.body.bottomText11;
                   if(req.files.img11.name)
                  req.user.writeArticle[i].img11='/article_image/'+req.files.img11.name;
                  //12th page--------
                  req.user.writeArticle[i].articleTitle12 = req.body.articleTitle12;
                  req.user.writeArticle[i].article12 = req.body.article12;
                  req.user.writeArticle[i].source12 = req.body.source12;
                  req.user.writeArticle[i].bottomText12 = req.body.bottomText12;
                   if(req.files.img12.name)
                  req.user.writeArticle[i].img12='/article_image/'+req.files.img12.name;

            }

         //   if(req.body.morepage == 3)
              {
              //13th page-------
              req.user.writeArticle[i].articleTitle13 = req.body.articleTitle13;
              req.user.writeArticle[i].article13 = req.body.article13;
              req.user.writeArticle[i].source13 = req.body.source13;
              req.user.writeArticle[i].bottomText13 = req.body.bottomText13;
               if(req.files.img13.name)
                  req.user.writeArticle[i].img13='/article_image/'+req.files.img13.name;
              //14th page---------
              req.user.writeArticle[i].articleTitle14 = req.body.articleTitle14;
              req.user.writeArticle[i].article14 = req.body.article14;
              req.user.writeArticle[i].source14 = req.body.source14;
              req.user.writeArticle[i].bottomText14 = req.body.bottomText14;
               if(req.files.img14.name)
                  req.user.writeArticle[i].img14='/article_image/'+req.files.img14.name;
              //15th page-------
              req.user.writeArticle[i].articleTitle15 = req.body.articleTitle15;
              req.user.writeArticle[i].article15 = req.body.article15;
              req.user.writeArticle[i].source15 = req.body.source15;
              req.user.writeArticle[i].bottomText15 = req.body.bottomText15;
               if(req.files.img15.name)
                  req.user.writeArticle[i].img15='/article_image/'+req.files.img15.name;
              //16th page------
              req.user.writeArticle[i].articleTitle16 = req.body.articleTitle16;
              req.user.writeArticle[i].article16 = req.body.article16;
              req.user.writeArticle[i].source16 = req.body.source16;
              req.user.writeArticle[i].bottomText16 = req.body.bottomText16;
               if(req.files.img16.name){
                req.user.writeArticle[i].img16='/article_image/'+req.files.img16.name; 
               }
                  
            }

        
        req.user.writeArticle[i].status=req.body.draft;
        req.user.save(function(err){
          if(err)
            console.log('Error');
          else
          {

            console.log('Done');
            console.log(req.body.draft)
            if(req.body.draft=='waiting')
              res.redirect('/userArticle');
            else{
            res.render('previous',
              {

                 user: req.user,
              });
          }
          }
        })



        }

    }

}


exports.addSound=function(req,res)
{


    var userId=req.param('id');
    var articleId=req.param('articleid');
    var test=0;

    User.findOne({_id:userId}).exec(function(err,user){
            if(err)
              console.log('Error');
            else
            {
                for(var i=0;i<user.writeArticle.length;i++)
                {

                      if(user.writeArticle[i]._id == articleId)
                      {


                            test++;

                            var morepage=user.writeArticle[i].morepage;
                            console.log('MOre page:');
                            
                            console.log(morepage);

                            console.log('got it');
                            var article= user.writeArticle[i];

                            // for page-1:
                            var title=user.writeArticle[i].bookTitle;
                            var boottitle = title.split(" ");
                            console.log(boottitle);

                            var topText=user.writeArticle[i].title;
                            var topText1=topText.split(" ");

                            var topSubText=user.writeArticle[i].article;
                            var topSubText1=topSubText.split(" ");

                            var bottomText=user.writeArticle[i].article;
                            var bottomText1=bottomText.split(" ");


                            //for page-2:
                            var topText=user.writeArticle[i].articleTitle2;
                            var topText2=topText.split(" ");

                            var topSubText=user.writeArticle[i].article2;
                            var topSubText2=topSubText.split(" ");

                            var bottomText=user.writeArticle[i].bottomText2;
                            var bottomText2=bottomText.split(" ");


                            //for page-3:
                            var topText=user.writeArticle[i].articleTitle3;
                            var topText3=topText.split(" ");

                            var topSubText=user.writeArticle[i].article3;
                            var topSubText3=topSubText.split(" ");

                            var bottomText=user.writeArticle[i].bottomText3;
                            var bottomText3=bottomText.split(" ");

                            //for page-4:
                            var topText=user.writeArticle[i].articleTitle4;
                            var topText4=topText.split(" ");

                            var topSubText=user.writeArticle[i].article4;
                            var topSubText4=topSubText.split(" ");

                            var bottomText=user.writeArticle[i].bottomText4;
                            var bottomText4=bottomText.split(" ");

                            if(morepage == 1 || morepage == 2 || morepage == 3)
                            {

                                  test++;
                                    //for page-5:
                                    var topText=user.writeArticle[i].articleTitle5;
                                    var topText5=topText.split(" ");

                                    var topSubText=user.writeArticle[i].article5;
                                    var topSubText5=topSubText.split(" ");

                                    var bottomText=user.writeArticle[i].bottomText5;
                                    var bottomText5=bottomText.split(" ");

                                    //for page-6:
                                    var topText=user.writeArticle[i].articleTitle6;
                                    var topText6=topText.split(" ");

                                    var topSubText=user.writeArticle[i].article6;
                                    var topSubText6=topSubText.split(" ");

                                    var bottomText=user.writeArticle[i].bottomText6;
                                    var bottomText6=bottomText.split(" ");

                                    //for page-7:
                                    var topText=user.writeArticle[i].articleTitle7;
                                    var topText7=topText.split(" ");

                                    var topSubText=user.writeArticle[i].article7;
                                    var topSubText7=topSubText.split(" ");

                                    var bottomText=user.writeArticle[i].bottomText7;
                                    var bottomText7=bottomText.split(" ");

                                    //for page-8:
                                    var topText=user.writeArticle[i].articleTitle8;
                                    var topText8=topText.split(" ");

                                    var topSubText=user.writeArticle[i].article8;
                                    var topSubText8=topSubText.split(" ");

                                    var bottomText=user.writeArticle[i].bottomText8;
                                    var bottomText8=bottomText.split(" ");

                            }
                            if( morepage == 2 || morepage == 3)
                            {


                                test++;
                                //for page-9:
                                  var topText=user.writeArticle[i].articleTitle9;
                                  var topText9=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article9;
                                  var topSubText9=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText9;
                                  var bottomText9=bottomText.split(" ");

                                  //for page-10:
                                  var topText=user.writeArticle[i].articleTitle10;
                                  var topText10=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article10;
                                  var topSubText10=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText10;
                                  var bottomText10=bottomText.split(" ");

                                  //for page-11:
                                  var topText=user.writeArticle[i].articleTitle11;
                                  var topText11=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article11;
                                  var topSubText11=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText11;
                                  var bottomText11=bottomText.split(" ");

                                  //for page-12:
                                  var topText=user.writeArticle[i].articleTitle12;
                                  var topText12=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article12;
                                  var topSubText12=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText12;
                                  var bottomText12=bottomText.split(" ");
                            }

                            if(morepage == 3)
                            {

                                  test++;
                                  //for page-13:
                                  var topText=user.writeArticle[i].articleTitle13;
                                  var topText13=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article13;
                                  var topSubText13=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText13;
                                  var bottomText13=bottomText.split(" ");

                                  //for page-14:
                                  var topText=user.writeArticle[i].articleTitle14;
                                  var topText14=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article14;
                                  var topSubText14=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText14;
                                  var bottomText14=bottomText.split(" ");

                                  //for page-15:
                                  var topText=user.writeArticle[i].articleTitle15;
                                  var topText15=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article15;
                                  var topSubText15=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText15;
                                  var bottomText15=bottomText.split(" ");

                                  //for page-16:
                                  var topText=user.writeArticle[i].articleTitle16;
                                  var topText16=topText.split(" ");

                                  var topSubText=user.writeArticle[i].article16;
                                  var topSubText16=topSubText.split(" ");

                                  var bottomText=user.writeArticle[i].bottomText16;
                                  var bottomText16=bottomText.split(" ");
                            }

                            if(test ==1)
                            {
                                res.render('addsound',{
                                      userId:userId,
                                      article:article,
                                      morepage:morepage,
                                      boottitle:boottitle,
                                      topText1:topText1,
                                      topSubText1:topSubText1,
                                      bottomText1:bottomText1,

                                      topText2:topText2,
                                      topSubText2:topSubText2,
                                      bottomText2:bottomText2,

                                     topText3:topText3,
                                      topSubText3:topSubText3,
                                      bottomText3:bottomText3,

                                      topText4:topText4,
                                      topSubText4:topSubText4,
                                      bottomText4:bottomText4,
                                    });


                            }
                            else if(test ==2)
                            {
                                 res.render('addsound',{
                                      userId:userId,
                                      article:article,
                                      morepage:morepage,
                                      boottitle:boottitle,
                                      topText1:topText1,
                                      topSubText1:topSubText1,
                                      bottomText1:bottomText1,

                                      topText2:topText2,
                                      topSubText2:topSubText2,
                                      bottomText2:bottomText2,

                                     topText3:topText3,
                                      topSubText3:topSubText3,
                                      bottomText3:bottomText3,

                                      topText4:topText4,
                                      topSubText4:topSubText4,
                                      bottomText4:bottomText4,

                                      topText5:topText5,
                                      topSubText5:topSubText5,
                                      bottomText5:bottomText5,

                                      topText6:topText6,
                                      topSubText6:topSubText6,
                                      bottomText6:bottomText6,

                                      topText7:topText7,
                                      topSubText7:topSubText7,
                                      bottomText7:bottomText7,

                                      topText8:topText8,
                                      topSubText8:topSubText8,
                                      bottomText8:bottomText8,

                                    });

                            }
                            else if(test == 3)
                            {
                                 res.render('addsound',{
                                      userId:userId,
                                      article:article,
                                      morepage:morepage,
                                      boottitle:boottitle,
                                      topText1:topText1,
                                      topSubText1:topSubText1,
                                      bottomText1:bottomText1,

                                      topText2:topText2,
                                      topSubText2:topSubText2,
                                      bottomText2:bottomText2,

                                     topText3:topText3,
                                      topSubText3:topSubText3,
                                      bottomText3:bottomText3,

                                      topText4:topText4,
                                      topSubText4:topSubText4,
                                      bottomText4:bottomText4,

                                      topText5:topText5,
                                      topSubText5:topSubText5,
                                      bottomText5:bottomText5,

                                      topText6:topText6,
                                      topSubText6:topSubText6,
                                      bottomText6:bottomText6,

                                      topText7:topText7,
                                      topSubText7:topSubText7,
                                      bottomText7:bottomText7,

                                      topText8:topText8,
                                      topSubText8:topSubText8,
                                      bottomText8:bottomText8,

                                      topText9:topText9,
                                      topSubText9:topSubText9,
                                      bottomText9:bottomText9,

                                      topText10:topText10,
                                      topSubText10:topSubText10,
                                      bottomText10:bottomText10,

                                      topText11:topText11,
                                      topSubText11:topSubText11,
                                      bottomText11:bottomText11,

                                      topText12:topText12,
                                      topSubText12:topSubText12,
                                      bottomText12:bottomText12,
                                    });

                            }

                            else if(test ==4)
                            {

                                   res.render('addsound',{
                                        userId:userId,
                                        article:article,
                                        morepage:morepage,
                                        boottitle:boottitle,
                                        topText1:topText1,
                                        topSubText1:topSubText1,
                                        bottomText1:bottomText1,

                                        topText2:topText2,
                                        topSubText2:topSubText2,
                                        bottomText2:bottomText2,

                                       topText3:topText3,
                                        topSubText3:topSubText3,
                                        bottomText3:bottomText3,

                                        topText4:topText4,
                                        topSubText4:topSubText4,
                                        bottomText4:bottomText4,

                                        topText5:topText5,
                                        topSubText5:topSubText5,
                                        bottomText5:bottomText5,

                                        topText6:topText6,
                                        topSubText6:topSubText6,
                                        bottomText6:bottomText6,

                                        topText7:topText7,
                                        topSubText7:topSubText7,
                                        bottomText7:bottomText7,

                                        topText8:topText8,
                                        topSubText8:topSubText8,
                                        bottomText8:bottomText8,

                                        topText9:topText9,
                                        topSubText9:topSubText9,
                                        bottomText9:bottomText9,

                                        topText10:topText10,
                                        topSubText10:topSubText10,
                                        bottomText10:bottomText10,

                                        topText11:topText11,
                                        topSubText11:topSubText11,
                                        bottomText11:bottomText11,

                                        topText12:topText12,
                                        topSubText12:topSubText12,
                                        bottomText12:bottomText12,

                                        topText13:topText13,
                                        topSubText13:topSubText13,
                                        bottomText13:bottomText13,

                                        topText14:topText14,
                                        topSubText14:topSubText14,
                                        bottomText14:bottomText14,

                                        topText15:topText15,
                                        topSubText15:topSubText15,
                                        bottomText15:bottomText15,

                                        topText16:topText16,
                                        topSubText16:topSubText16,
                                        bottomText16:bottomText16,
                                        
                                    });

                            }




 
                      }

                }
            }

    });

      

}




exports.addsoundSubmit=function(req,res)
{

      console.log('Got it');
      console.log(req.body.booktitleindex);
      console.log(req.body);

      articleId=req.body.articleId;
      userId=req.body.userId;
      User.findOne({_id:userId}).exec(function(err,user){
            if(err)
              console.log('Error');
            else
            {

                  for(var i=0;i<user.writeArticle.length;i++)
                  {

                      if(user.writeArticle[i]._id == articleId)
                      {
                           // Book title text
                              user.writeArticle[i].addsound='Done';
                              user.writeArticle[i].sound = 'Yes';
                              for(var j=0; j<req.body.booktitleindex ;j++)
                              {
                                  var str="bookTitle_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="bookTitleSound_"+j;
                                 
                                  console.log(req.files[strSound].name);

                                  if(req.files[strSound].name != "")

                                    user.writeArticle[i].bookTitleSound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                    user.writeArticle[i].bookTitleSound.push({word:req.body[str],sound:'Null'});


                                  (function(strSound){

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        if(err)
                                          console.log(err);
                                        else
                                        {

                                           fs.writeFile('public/article_sound/'+ req.files[strSound].name,data, function (err) {
                                                if (err) 
                                                  console.log('Error');
                                                 
                                                else
                                                  console.log('Done success');
                                              });
                                        }
                                        


                                    });


                                  })(strSound);

                                  
                              }
                              // Page -1 start----------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>

                              for(var j=0;j<req.body.toptext1index;j++)  // Title 1 sound
                              {

                                  var str="topText1_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topText1Sound_"+j;

                                  if(req.files[strSound].name != "")
                                    user.writeArticle[i].title1Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                    user.writeArticle[i].title1Sound.push({word:req.body[str],sound:'Null'});

                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              for(var j=0;j<req.body.topsubtext1index;j++)  // Top sub text 1 sound
                              {

                                  var str="topSubText1_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topSubText1Sound_"+j;


                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].topSubText1Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].topSubText1Sound.push({word:req.body[str],sound:'Null'});

                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              
                              for(var j=0;j<req.body.bottomtext1index;j++)  // Top bottom text 1 sound
                              {

                                  var str="bottomText1_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="bottomText1Sound_"+j;
                                  
                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].topBottomText1Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].topBottomText1Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }


                              //----------------- Page - 1 End---------------------->>>>>>>>>>>>>>>>>>>>

                              //---------------Start page-2 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                              for(var j=0;j<req.body.toptext2index;j++)  // Title 2 sound
                              {

                                  var str="topText2_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topText2Sound_"+j;

                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].articleTitle2Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].articleTitle2Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              for(var j=0;j<req.body.topsubtext2index;j++)  // Top sub text 2 sound
                              {

                                  var str="topSubText2_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topSubText2Sound_"+j;

                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].article2Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].article2Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              
                              for(var j=0;j<req.body.bottomtext2index;j++)  // Top bottom text 2 sound
                              {

                                  var str="bottomText2_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="bottomText2Sound_"+j;
                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].bottomText2Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else 
                                      user.writeArticle[i].bottomText2Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }

                              //---------------page 2 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                              

                              //---------------Start page-3 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                              for(var j=0;j<req.body.toptext3index;j++)  // Title 3 sound
                              {

                                  var str="topText3_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topText3Sound_"+j;

                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].articleTitle3Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].articleTitle3Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              for(var j=0;j<req.body.topsubtext3index;j++)  // Top sub text 3 sound
                              {

                                  var str="topSubText3_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topSubText3Sound_"+j;

                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].article3Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].article3Sound.push({word:req.body[str],sound:'Null'});

                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              
                              for(var j=0;j<req.body.bottomtext3index;j++)  // Top bottom text 3 sound
                              {

                                  var str="bottomText3_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="bottomText3Sound_"+j;
                                  if(req.files[strSound].name != "")
                                    user.writeArticle[i].bottomText3Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                    user.writeArticle[i].bottomText3Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }

                              //---------------page 3 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                              //---------------Start page-4 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                              for(var j=0;j<req.body.toptext4index;j++)  // Title 4 sound
                              {

                                  var str="topText4_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topText4Sound_"+j;

                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].articleTitle4Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].articleTitle4Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              for(var j=0;j<req.body.topsubtext4index;j++)  // Top sub text 4 sound
                              {

                                  var str="topSubText4_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="topSubText4Sound_"+j;

                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].article4Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].article4Sound.push({word:req.body[str],sound:'Null'});

                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }
                              
                              for(var j=0;j<req.body.bottomtext4index;j++)  // Top bottom text 4 sound
                              {

                                  var str="bottomText4_"+j;
                                  console.log(str);
                                  console.log(req.body[str]);
                                  var strSound="bottomText4Sound_"+j;
                                  if(req.files[strSound].name != "")
                                      user.writeArticle[i].bottomText4Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                  else
                                      user.writeArticle[i].bottomText4Sound.push({word:req.body[str],sound:'Null'});
                                  (function(strSound){
                                    

                                    fs.readFile(req.files[strSound].path, function (err, data) {
                                        console.log(req.files[strSound].name);
                                        
                                        fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                            if (err) 
                                              console.log('Error');
                                              //throw err; 
                                            else
                                              console.log('Done success');
                                          });

                                    });
                                  })(strSound);

                              }

                              //---------------page 4 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                          if(req.body.morepage == '1')
                          {

                                    //---------------Start page-5 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.toptext5index;j++)  // Title 5 sound
                                    {

                                        var str="topText5_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText5Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].articleTitle5Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].articleTitle5Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topsubtext5index;j++)  // Top sub text 5 sound
                                    {

                                        var str="topSubText5_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText5Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].article5Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].article5Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomtext5index;j++)  // Top bottom text 5 sound
                                    {

                                        var str="bottomText5_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText5Sound_"+j;
                                        
                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].bottomText5Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].bottomText5Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 5 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                     //---------------Start page-6 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText6index;j++)  // Title 6 sound
                                    {

                                        var str="topText6_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText6Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].articleTitle6Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                             user.writeArticle[i].articleTitle6Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText6index;j++)  // Top sub text 6 sound
                                    {

                                        var str="topSubText6_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText6Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].article6Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].article6Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText6index;j++)  // Top bottom text 6 sound
                                    {

                                        var str="bottomText6_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText6Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].bottomText6Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].bottomText6Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 6 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                     //---------------Start page-7 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText7index;j++)  // Title 7 sound
                                    {

                                        var str="topText7_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText7Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].articleTitle7Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].articleTitle7Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText7index;j++)  // Top sub text 7 sound
                                    {

                                        var str="topSubText7_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText7Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].article7Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].article7Sound.push({word:req.body[str],sound:'Null'});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText7index;j++)  // Top bottom text 7 sound
                                    {

                                        var str="bottomText7_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText7Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].bottomText7Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else

                                            user.writeArticle[i].bottomText7Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 7 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                     //---------------Start page-8 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText8index;j++)  // Title 8 sound
                                    {

                                        var str="topText8_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText8Sound_"+j;

                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].articleTitle8Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                            user.writeArticle[i].articleTitle8Sound.push({word:req.body[str],sound:'Null'});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText8index;j++)  // Top sub text 8 sound
                                    {

                                        var str="topSubText8_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText8Sound_"+j;

                                        if(req.files[strSound].name != "")
                                          user.writeArticle[i].article8Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                          user.writeArticle[i].article8Sound.push({word:req.body[str],sound:'Null'});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText8index;j++)  // Top bottom text 8 sound
                                    {

                                        var str="bottomText8_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText8Sound_"+j;
                                        if(req.files[strSound].name != "")
                                            user.writeArticle[i].bottomText8Sound.push({word:req.body[str],sound:req.files[strSound].name});
                                        else
                                             user.writeArticle[i].bottomText8Sound.push({word:req.body[str],sound:'Null'});
                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 8 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                          }

                          if(req.body.morepage == '2')
                          {

                                      //---------------Start page-9 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                      for(var j=0;j<req.body.topText9index;j++)  // Title 9 sound
                                      {

                                          var str="topText9_"+j;
                                          console.log(str);
                                          console.log(req.body[str]);
                                          var strSound="topText9Sound_"+j;
                                          user.writeArticle[i].articleTitle9Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                          (function(strSound){
                                            

                                            fs.readFile(req.files[strSound].path, function (err, data) {
                                                console.log(req.files[strSound].name);
                                                
                                                fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                    if (err) 
                                                      console.log('Error');
                                                      //throw err; 
                                                    else
                                                      console.log('Done success');
                                                  });

                                            });
                                          })(strSound);

                                      }
                                      for(var j=0;j<req.body.topSubText9index;j++)  // Top sub text 9 sound
                                      {

                                          var str="topSubText9_"+j;
                                          console.log(str);
                                          console.log(req.body[str]);
                                          var strSound="topSubText9Sound_"+j;
                                          user.writeArticle[i].article9Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                          (function(strSound){
                                            

                                            fs.readFile(req.files[strSound].path, function (err, data) {
                                                console.log(req.files[strSound].name);
                                                
                                                fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                    if (err) 
                                                      console.log('Error');
                                                      //throw err; 
                                                    else
                                                      console.log('Done success');
                                                  });

                                            });
                                          })(strSound);

                                      }
                                      
                                      for(var j=0;j<req.body.bottomText9index;j++)  // Top bottom text 9 sound
                                      {

                                          var str="bottomText9_"+j;
                                          console.log(str);
                                          console.log(req.body[str]);
                                          var strSound="bottomText9Sound_"+j;

                                          user.writeArticle[i].bottomText9Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                          (function(strSound){
                                            

                                            fs.readFile(req.files[strSound].path, function (err, data) {
                                                console.log(req.files[strSound].name);
                                                
                                                fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                    if (err) 
                                                      console.log('Error');
                                                      //throw err; 
                                                    else
                                                      console.log('Done success');
                                                  });

                                            });
                                          })(strSound);

                                      }

                                      //---------------page 9 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



                                    //---------------Start page-10 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText10index;j++)  // Title 10 sound
                                    {

                                        var str="topText10_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText10Sound_"+j;
                                        user.writeArticle[i].articleTitle10Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText10index;j++)  // Top sub text 10 sound
                                    {

                                        var str="topSubText10_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText10Sound_"+j;
                                        user.writeArticle[i].article10Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText10index;j++)  // Top bottom text 10 sound
                                    {

                                        var str="bottomText10_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText10Sound_"+j;

                                        user.writeArticle[i].bottomText10Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 10 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                                    //---------------Start page-11 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText11index;j++)  // Title 11 sound
                                    {

                                        var str="topText11_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText11Sound_"+j;
                                        user.writeArticle[i].articleTitle11Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText11index;j++)  // Top sub text 11 sound
                                    {

                                        var str="topSubText11_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText11Sound_"+j;
                                        user.writeArticle[i].article11Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText11index;j++)  // Top bottom text 11 sound
                                    {

                                        var str="bottomText11_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText11Sound_"+j;

                                        user.writeArticle[i].bottomText11Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 11 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                                    //---------------Start page-12 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText12index;j++)  // Title 12 sound
                                    {

                                        var str="topText12_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText12Sound_"+j;
                                        user.writeArticle[i].articleTitle12Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText12index;j++)  // Top sub text 12 sound
                                    {

                                        var str="topSubText12_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText12Sound_"+j;
                                        user.writeArticle[i].article12Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText12index;j++)  // Top bottom text 12 sound
                                    {

                                        var str="bottomText12_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText12Sound_"+j;

                                        user.writeArticle[i].bottomText12Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 12 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>





                          }

                          if(req.body.morepage == '3')
                          {

                                    //---------------Start page-13 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText13index;j++)  // Title 13 sound
                                    {

                                        var str="topText13_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText13Sound_"+j;
                                        user.writeArticle[i].articleTitle13Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText13index;j++)  // Top sub text 13 sound
                                    {

                                        var str="topSubText13_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText13Sound_"+j;
                                        user.writeArticle[i].article13Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText13index;j++)  // Top bottom text 13 sound
                                    {

                                        var str="bottomText13_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText13Sound_"+j;

                                        user.writeArticle[i].bottomText13Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 13 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



                                    //---------------Start page-14 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText14index;j++)  // Title 14 sound
                                    {

                                        var str="topText14_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText14Sound_"+j;
                                        user.writeArticle[i].articleTitle14Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText14index;j++)  // Top sub text 14 sound
                                    {

                                        var str="topSubText14_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText14Sound_"+j;
                                        user.writeArticle[i].article14Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText14index;j++)  // Top bottom text 14 sound
                                    {

                                        var str="bottomText14_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText14Sound_"+j;

                                        user.writeArticle[i].bottomText14Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 14 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                                    //---------------Start page-15 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText15index;j++)  // Title 15 sound
                                    {

                                        var str="topText15_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText15Sound_"+j;
                                        user.writeArticle[i].articleTitle15Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText15index;j++)  // Top sub text 15 sound
                                    {

                                        var str="topSubText15_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText15Sound_"+j;
                                        user.writeArticle[i].article15Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText15index;j++)  // Top bottom text 15 sound
                                    {

                                        var str="bottomText15_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText15Sound_"+j;

                                        user.writeArticle[i].bottomText15Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 15 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



                                    //---------------Start page-16 start ---------------->>>>>>>>>>>>>>>>>>>>>>
                                    for(var j=0;j<req.body.topText16index;j++)  // Title 16 sound
                                    {

                                        var str="topText16_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topText16Sound_"+j;
                                        user.writeArticle[i].articleTitle16Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    for(var j=0;j<req.body.topSubText16index;j++)  // Top sub text 16 sound
                                    {

                                        var str="topSubText16_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="topSubText16Sound_"+j;
                                        user.writeArticle[i].article16Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }
                                    
                                    for(var j=0;j<req.body.bottomText16index;j++)  // Top bottom text 16 sound
                                    {

                                        var str="bottomText16_"+j;
                                        console.log(str);
                                        console.log(req.body[str]);
                                        var strSound="bottomText16Sound_"+j;

                                        user.writeArticle[i].bottomText16Sound.push({word:req.body[str],sound:req.files[strSound].name});

                                        (function(strSound){
                                          

                                          fs.readFile(req.files[strSound].path, function (err, data) {
                                              console.log(req.files[strSound].name);
                                              
                                              fs.writeFile('public/article_sound/'+req.files[strSound].name,data, function (err) {
                                                  if (err) 
                                                    console.log('Error');
                                                    //throw err; 
                                                  else
                                                    console.log('Done success');
                                                });

                                          });
                                        })(strSound);

                                    }

                                    //---------------page 16 end--------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>





                          }

                          user.save(function(err){
                              if(err)
                                console.log('Error');
                              else
                              {

                                        var article_user=[];
                                        var count=0;
                                        var counter=0;
                                      User.find().exec(function(err,user){
                                           // console.log(user.length);
                                            for(var i=0;i<user.length;i++)
                                            {
                                                if(user[i].userType == 'Author')
                                                {
                                                    article_user[counter]=user[i];
                                                    counter++;
                                                }

                                            }
                                            

                                            for(var i=0;i<user.length;i++)
                                            {
                                                for(var k=0;k<user[i].reviewArticle.length;k++)
                                                {
                                                  count++;
                                                }

                                            }

                                            console.log('Count:' +count);

                                            res.redirect('/uploadFiles');        

                                      });
                              }
                                

                          });

                        
                      } // if end

                  }


            }


      });
  
}

exports.previousArticleshow=function(req,res)
{
    res.render('previous',{

      user:req.user,
    });



}

exports.manageMembers=function(req,res)
{
  //console.log('THis is members area');

 
  var author=[];
  var searcheduser=[];
  var counter=0;
  User.find().exec(function(err,user){

        for(var i=0;i<user.length;i++)
        {
          if(user[i].userType == 'Author')
          {
             
              /*author_name[counter]=user[i].name;
              userType[counter]=user[i].userType;
              user_id[counter]=user[i]._id; */

              author[counter]=user[i];
              counter++;
          }
        }
        
        //console.log(author);
      //  console.log(author_name.length);

        res.render('manageMembers',{
          searcheduser:searcheduser,
          author:author

        });         

  });


}
exports.searchUser=function(req,res){
  var content=req.body.searchUser;
  var count=0;
  var counter=0;
  var searcheduser=[];
   var author=[];
  User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else{
        for(var i=0;i<user.length;i++)
        {   if(user[i].userType == 'Author')
          {
             author[counter]=user[i];
              counter++;
          }
            if(user[i].name==content)
            {
                searcheduser[count]=user[i];
                count++;
            }
            else if(user[i].email==content)
            {
                searcheduser[count]=user[i];
                count++;
            }
            else if(user[i].userType==content)
            {
                searcheduser[count]=user[i];
                count++;
            }
             else if(user[i].reviewer.length!=0&&content=='Reviewer' )
              {
                  searcheduser[count]=user[i];
                  count++;
              }
            for(var j=0;j<user[i].userSubtype.length;j++){
              if(user[i].userSubtype[j].subtype==content)
              {
                searcheduser[count]=user[i];
                count++;
              }
            }
        }
         res.render('manageMembers',{
          author:author,
          searcheduser:searcheduser

        }); 
      }
  })
}
exports.removeUserType=function(req,res){
  console.log(req.body);
  var type=req.body.category1;
  User.findOne({email:req.body.email}).exec(function(err,user){
        if(err)
          console.log(err)
        else if(!user){
          console.log('No user')
        }
        else if(user)
        {
            for(var i=0;i<user.userSubtype.length;i++)
            {
              if(user.userSubtype[i].subtype==type)
              {
                  user.userSubtype.splice(i,1);
                     user.save(function(err){
                      if(err)
                        console.log('Error');
                      else
                      {
                          
                          res.send('Successful');

                      }
                        
                    });   
              }
            }
        }

  })
}
exports.manageArticles=function(req,res){
   // console.log('manageArticles');

    var article_user=[];
    var count=0;
    var counter=0;
  User.find().exec(function(err,user){
       // console.log(user.length);
        for(var i=0;i<user.length;i++)
        {
            if(user[i].userType == 'Author')
            {
                article_user[counter]=user[i];
                counter++;
            }

        }
        

        for(var i=0;i<user.length;i++)
        {
            for(var k=0;k<user[i].reviewArticle.length;k++)
            {
              count++;
            }

        }

        console.log('Count:' +count);
        res.render('manageArticle',{
          article: article_user,
          count:count,

        });        

  });

}


exports.addtype=function(req,res){
  console.log('Check');
  var Check=0;
  console.log(req.body);

  User.findOne({_id:req.body.id}).exec(function(err,user){
    if(err)
    {
      console.log('Error');
    }
    else
    {
      for(var i=0;i<user.userSubtype.length;i++)
      {
          if(user.userSubtype[i].subtype==req.body.typeValue)
          {
              Check=1;
          }
      }
      console.log(Check);
      if(Check==0){
            console.log(user.userSubtype.length);
            user.userSubtype.push({subtype:req.body.typeValue});

            console.log(user.userSubtype);
            user.save(function(err){
              if(err)
                console.log('Error');
              else
              {
                  console.log('Successful');
                  res.send('Successful');

              }
                
            });   

          }
    }

  });
}

exports.assignReviewer=function(req,res)
{

  res.render('assignReviewer',{
          author_name: 0
         
        });   
}
exports.myWallet=function(req,res){
   var acceptedArticleForBid=[];
  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            { 
                for(var k=0;k<req.user.acceptedArtist.length;k++)
                {
                 // console.log(user[i].writeArticle[j]._id+ "  "+req.user.acceptedArtist[k].articleId)
                    if(user[i].writeArticle[j]._id==req.user.acceptedArtist[k].articleId)
                    {
                      acceptedArticleForBid[count]=user[i].writeArticle[j];
                      count++;
                    }
                }
            }
        }
        res.render('myWallet',{
          acceptedArticleForBid:acceptedArticleForBid,
          user:req.user,
        })
    }
  })
  //res.render('myWallet')
}
// for artist page in user
exports.artist=function(req,res){

  var waiting=0;
    var waiting_review=0;
    var review_so_far=0;
    var rebid=0;
    var artwork=0;
    for(var i=0;i<req.user.selectedArtist.length;i++)
    {
      if(req.user.selectedArtist[i].status == 'Request Artist')
      {
        waiting++;
      }
      if(req.user.selectedArtist[i].status != 'Request Artist'&&req.user.selectedArtist[i].status != 'Finish')
      {
        rebid++;
      }
      // if(req.user.selectedArtist[i].status == 'Request Artist')
      // {
      //   waiting++;
      // }
    }
    for(var i=0;i<req.user.selectedArtist.length;i++)
    {
      if(req.user.selectedArtist[i].status == 'Accepted')
      {
        waiting_review++;
      }
    }
    // for(var i=0;i<req.user.selectedArtist.length;i++)
    // {
    //   if(req.user.selectedArtist[i].status == 'Finish')
    //   {
    //     review_so_far++;
    //   }
    // }
    // for(var i=0;i<req.user.acceptedArtist.length;i++)
    // {
    //   if(req.user.acceptedArtist[i].status == 'Bidding in progress')
    //   {
    //     artwork++;
    //   }
    // }

      User.find().exec(function(err,user){
        if(err)
          console.log(err)
        else
        {
            for(var i=0;i<user.length;i++)
            {
              (function(i){
                   for(var j=0;j<user[i].writeArticle.length;j++)
                    { 
                        for(var k=0;k<req.user.acceptedArtist.length;k++)
                        {
                         // console.log(user[i].writeArticle[j]._id+ "  "+req.user.acceptedArtist[k].articleId)
                            if(user[i].writeArticle[j]._id==req.user.acceptedArtist[k].articleId&&user[i].writeArticle[j].status == 'Bidding in progress'||user[i].writeArticle[j].status == 'Bidding Proccessing')
                            {
                               artwork++;
                            }
                        }
                        // for(var m=0;m<req.user.selectedArtist.length;m++)
                        // {
                        //     if(req.user.selectedArtistId[i] == user[i].writeArticle._id&&user[i].writeArticle.status=='Received Author’s Response')
                        //       {
                        //         review_so_far++;
                        //       }
                        // }

                        for(var k=0;k<req.user.acceptedArtist.length;k++)
                          {
                              if(user[i].writeArticle[j]._id==req.user.acceptedArtist[k].articleId&&user[i].writeArticle[j].status=='Received Author’s Response')
                              {
                                review_so_far++;
                              }
                          }

                    }
              })(i)
               if(i==user.length-1)
               {
                  res.render('artist',{
                   user:req.user,
                   waiting:waiting,
                   waiting_review:waiting_review,
                   review_so_far:review_so_far,
                   rebid:rebid,
                   artwork:artwork,
                   
                  }); 
               }
            }
            
        }
      })

 // console.log(req.user);
         

}
exports.articleUpdate=function(req,res){
//  console.log(req.body);
  res.send(req.body.index);

} 

exports.viewArtistArtWork=function(req,res){
    var articleId=req.param('article');
    var author=req.param('author');
    var artistEmail=req.param('artist');
    var article;
    var artist;
    var working=0;
    var contributed=0;
    var biddingArticle;
    User.findOne({_id:author}).exec(function(err,user){
      if(err)
        console.log(err)
      else
      {
        for(var i=0;i<user.writeArticle.length;i++)
        {
          if(user.writeArticle[i]._id==articleId)
          {   
           
            article=user.writeArticle[i];
              for(var j=0;j<user.writeArticle[i].bidding_Article.length;j++)
              { 
                  if(user.writeArticle[i].bidding_Article[j].artistEmail==artistEmail) {
                     biddingArticle=user.writeArticle[i].bidding_Article[j];
                   
                    // console.log(biddingArticle);
                  }  
                 
                
                
              }
          }
        }
         User.find().exec(function(err,user1){
          if(err)
            console.log(err)
          else
          {
              for(var i=0;i<user1.length;i++)
              {
                  if(user1[i].email==artistEmail)
                  {
                      artist=user1[i];
                  }
              }

              for(var i=0;i<user1.length;i++)
              {
                  for(var j=0;j<user1[i].writeArticle.length;j++)
                  {
                      for(var k=0;k<user1[i].writeArticle[j].bidding_Article.length;k++)
                      {
                          if(user1[i].writeArticle[j].bidding_Article[k].artistEmail==artistEmail&&user1[i].writeArticle[j].bidding_Article[k].status=='')
                          {
                            working=working++;
                          }
                          if(user1[i].writeArticle[j].bidding_Article[k].artistEmail==artistEmail&&user1[i].writeArticle[j].bidding_Article[k].status=='Accepted')
                          {
                            contributed=contributed++;
                          }
                      }
                  }
              }

                res.render('viewArtistArtWork',{
                  biddingArticle:biddingArticle,
                  article:article,
                  artist:artist,
                  working:working,
                  contributed:contributed,
                })
          }

         
         })

         
      }
    })

}
exports.viewBids=function(req,res){
  var articleId=req.param('article');
  var author=req.param('author');
  var biddingArticle=[];
  var artist=[];
  var article;
  var working=[];
  working[0]=0;
  working[1]=0;
  working[2]=0;
  var contributed=[];
  contributed[0]=0;
  contributed[1]=0;
  contributed[2]=0;
  var c=0;var c1=0;
  User.findOne({_id:author}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
      for(var i=0;i<user.writeArticle.length;i++)
      {
        if(user.writeArticle[i]._id==articleId)
        {   article=user.writeArticle[i];
            for(var j=0;j<user.writeArticle[i].bidding_Article.length;j++)
            {   
                biddingArticle[c]=user.writeArticle[i].bidding_Article[j];
                c++;
              
            }
        }
      }
        User.find().exec(function(err,user1){
                  if(err)
                    console.log(err);
                  else
                  {
                      for(var a=0;a<user1.length;a++)
                      {
                          for(var i=0;i<biddingArticle.length;i++)
                          {
                              if(user1[a]._id==biddingArticle[i].artistId)
                              {
                                  artist[c1]=user1[a];
                                  c1++;
                              }
                              // for(var j=0;j<user1[a].writeArticle.length;j++)
                              // {
                              //     for(var k=0;k<user1[a].writeArticle[j].bidding_Article.length;k++)
                              //     {
                              //         if(user1[a].writeArticle[j].bidding_Article[k].artistId==user1._id&&user1[a].writeArticle[j].bidding_Article[k].status=='')
                              //         {
                              //             working[i]=working[i]+1;
                              //         }
                              //     }
                              // }

                          }


                         
                      }

                      for(var i=0;i<biddingArticle.length;i++)
                          {
                              for(var j=0;j<user1.length;j++)
                              {
                                  for(var k=0;k<user1[j].writeArticle.length;k++)
                                  {
                                      for(var l=0;l<user1[j].writeArticle[k].bidding_Article.length;l++)
                                      {
                                          if(user1[j].writeArticle[k].bidding_Article[l].artistId==biddingArticle[i].artistId&&user1[j].writeArticle[k].bidding_Article[l].status=='')
                                          {
                                              working[i]=working[i]+1;
                                              //console.log(user1[j].writeArticle[k].bidding_Article[l].artistEmail)
                                          }
                                          if(user1[j].writeArticle[k].bidding_Article[l].artistId==biddingArticle[i].artistId&&user1[j].writeArticle[k].bidding_Article[l].status=='Accepted')
                                          {
                                              contributed[i]=contributed[i]+1;
                                             // console.log(user1[j].writeArticle[k].bidding_Article[l].artistEmail)
                                          }
                                      }
                                  }
                              }

                          }
                       res.render('detailsBids',{
                          biddingArticle:biddingArticle,
                          article:article,
                          artist:artist,
                          working:working,
                          contributed:contributed,
                        })
                  }
                })
     
    }
  })
}
exports.ViewAuthorSelection=function(req,res){
  var author=req.param('author');
  var articleid=req.param('article');
  var article;
  User.findOne({_id:author}).exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.writeArticle.length;i++){
            if(user.writeArticle[i]._id==articleid)
            {
                article=user.writeArticle[i];
            }
        }
        res.render('ViewAuthorSelection',{
          article:article,
        })
    }
  })
}
exports.acceptArtistImage=function(req,res){

  var email;
    console.log(req.body);
    
    for(var i=0;i<req.user.writeArticle.length;i++)
    {
        if(req.user.writeArticle[i]._id == req.body.index)
        {

          console.log(req.user.writeArticle[i].title);
          req.user.writeArticle[i].status='Received Author’s Response';
          if(req.user.writeArticle[i].artistImage1&&req.user.writeArticle[i].artistImage1!='')
            req.user.writeArticle[i].articleImage= req.user.writeArticle[i].artistImage1;
          if(req.user.writeArticle[i].artistImage2&&req.user.writeArticle[i].artistImage2!='')
            req.user.writeArticle[i].img2= req.user.writeArticle[i].artistImage2;
          if(req.user.writeArticle[i].artistImage3&&req.user.writeArticle[i].artistImage3!='')
            req.user.writeArticle[i].img3= req.user.writeArticle[i].artistImage3;
          if(req.user.writeArticle[i].artistImage4&&req.user.writeArticle[i].artistImage4!='')
            req.user.writeArticle[i].img4= req.user.writeArticle[i].artistImage4;
          if(req.user.writeArticle[i].artistImage5&&req.user.writeArticle[i].artistImage5!='')
            req.user.writeArticle[i].img5= req.user.writeArticle[i].artistImage5;
          if(req.user.writeArticle[i].artistImage6&&req.user.writeArticle[i].artistImage6!='')
            req.user.writeArticle[i].img6= req.user.writeArticle[i].artistImage6;
          if(req.user.writeArticle[i].artistImage7&&req.user.writeArticle[i].artistImage7!='')
            req.user.writeArticle[i].img7= req.user.writeArticle[i].artistImage7;
          if(req.user.writeArticle[i].artistImage8&&req.user.writeArticle[i].artistImage8!='')
            req.user.writeArticle[i].img8= req.user.writeArticle[i].artistImage8;
          if(req.user.writeArticle[i].artistImage9&&req.user.writeArticle[i].artistImage9!='')
            req.user.writeArticle[i].img9= req.user.writeArticle[i].artistImage9;
          if(req.user.writeArticle[i].artistImage10&&req.user.writeArticle[i].artistImage10!='')
            req.user.writeArticle[i].img10= req.user.writeArticle[i].artistImage10;
          if(req.user.writeArticle[i].artistImage11&&req.user.writeArticle[i].artistImage11!='')
            req.user.writeArticle[i].img11= req.user.writeArticle[i].artistImage11;
          if(req.user.writeArticle[i].artistImage12&&req.user.writeArticle[i].artistImage12!='')
            req.user.writeArticle[i].img12= req.user.writeArticle[i].artistImage12;
          if(req.user.writeArticle[i].artistImage13&&req.user.writeArticle[i].artistImage13!='')
            req.user.writeArticle[i].img13= req.user.writeArticle[i].artistImage13;
          if(req.user.writeArticle[i].artistImage14&&req.user.writeArticle[i].artistImage14!='')
            req.user.writeArticle[i].img14= req.user.writeArticle[i].artistImage14;
          if(req.user.writeArticle[i].artistImage15&&req.user.writeArticle[i].artistImage15!='')
            req.user.writeArticle[i].img15= req.user.writeArticle[i].artistImage15;
          if(req.user.writeArticle[i].artistImage16&&req.user.writeArticle[i].artistImage16!='')
            req.user.writeArticle[i].img16= req.user.writeArticle[i].artistImage16;
          req.user.save(function(err){
              if(err)
                console.log('Error');
              else
              {

                  console.log('Done');
                  res.send('Done');

                  

              }

          });
          
        }

    }

    

}

exports.rejectArtistImage=function(req,res){
    console.log(req.body);
    
    for(var i=0;i<req.user.writeArticle.length;i++)
    {
        if(req.user.writeArticle[i]._id == req.body.index)
        {

          console.log(req.user.writeArticle[i].title);
          req.user.writeArticle[i].status='Received Author’s Response';
         

          req.user.save(function(err){
              if(err)
                console.log('Error');
              else
              {

                  console.log('Done');
                  res.send('Done');

              }

          });
          
        }

    }

}


exports.reviewer=function(req,res)
{

    var counter=0;
    var article_user=[];

    User.find().exec(function(err,user){

        for(var i=0;i<user.length;i++)
        {
            if(user[i].reviewerType == 'Reviewer')
            {
                article_user[counter]=user[i].name;
                counter++;
            }

        }


        res.render('reviewer',{
          name: article_user,
          no:counter,

        });        

  });



}
exports.userArticle=function(req,res){
 
  var author_approval=0;
  var draft_article=0;
  var waiting=0;
  var numberacceptedArtist=0;
  for(var i=0;i<req.user.writeArticle.length;i++)
  {   
      if(req.user.writeArticle[i].status == 'Request Authors Approval')
      {

          author_approval=author_approval+1;
      }
      if(req.user.writeArticle[i].status == 'draft')
      {

          draft_article++;
          console.log('Draft show');
      }
      if(req.user.writeArticle[i].status == 'waiting')
      {

          waiting++;
          console.log('Draft show');
      }
      // if(req.user.acceptedArtist)
      // {
      //     numberacceptedArtist=req.user.acceptedArtist.length;
      // }
  }
// console.log('user')
// console.log(req.user)

  res.render('userArticle',{
    user: req.user,
    author_approval:author_approval,
    draft:draft_article,
    waiting:waiting,
    //numberacceptedArtist:numberacceptedArtist,
  });
 
}
exports.acceptedArticleForBid=function(req,res){
  var acceptedArticleForBid=[];

  var count=0;
  User.find().exec(function(err,user){
    if(err)
      console.log(err)
    else
    {
        for(var i=0;i<user.length;i++)
        {
            for(var j=0;j<user[i].writeArticle.length;j++)
            { 
                for(var k=0;k<req.user.acceptedArtist.length;k++)
                {
                 // console.log(user[i].writeArticle[j]._id+ "  "+req.user.acceptedArtist[k].articleId)
                    if(user[i].writeArticle[j]._id==req.user.acceptedArtist[k].articleId)
                    {
                      acceptedArticleForBid[count]=user[i].writeArticle[j];
                      count++;
                    }
                }
            }
        }
        res.render('acceptedArticleForBid',{
          acceptedArticleForBid:acceptedArticleForBid,
          user:req.user,
        })
    }
  })
}
exports.session = function (req, res) {
  console.log('Session:');
  console.log(req.body);
 /* var url=req.session.redirectUrl || '/';
  req.session.redirectUrl=null;
  console.log(req.session.redirectUrl);
  console.log(url);
  res.redirect(url);  */
  
}

exports.home=function(req,res){

    if(req.user.userType == 'Author')
    {
        if(req.user.activation == 'Active')
            res.redirect('/');
        else
            res.redirect('/signin#loginError');

    }
    else
    {

      res.redirect('/admin');
    }
    


}

exports.facebookSignup = function(req,res){
  
  res.redirect('/auth/facebook');


}
