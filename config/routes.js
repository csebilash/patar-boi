
var async = require('async')



module.exports = function (app, passport, auth) {

var index = require('../app/controllers/index');
var users = require('../app/controllers/users');


    
app.get('/signin', users.signin);
app.get('/signup', users.signup);
app.get('/admin',auth.requiresLogin, users.admin);
app.get('/rejectArticle',auth.requiresLogin,users.rejectArticle);
app.post('/rejectReviewArticle',auth.requiresLogin,users.rejectReviewArticle);
app.get('/changeRequest',auth.requiresLogin,users.changeRequest);
app.get('/reviewarticle',auth.requiresLogin,users.reviewarticle);
app.post('/submitChangeRequest',auth.requiresLogin,users.submitChangeRequest);
app.get('/changeArticle',auth.requiresLogin,users.changeArticle);
app.post('/changecontentsubmit',auth.requiresLogin,users.changecontentsubmit);
app.post('/submitReason',auth.requiresLogin,users.submitReason);
app.get('/artist',auth.requiresLogin,users.artist);
app.get('/artistarticle',auth.requiresLogin,users.artistarticle);
app.get('/artwork',auth.requiresLogin,users.artwork);
app.post('/searchArticle',auth.requiresLogin,users.searchArticle);
app.post('/searchArticleByArticle',auth.requiresLogin, users.searchArticleByArticle);
app.post('/searchArticleProductionCost',auth.requiresLogin,users.searchArticleProductionCost);
app.post('/manageArticle/deleteReviewer',auth.requiresLogin,users.deleteReviewer);
app.post('/admin/RemoveArtist',auth.requiresLogin,users.RemoveArtist);
app.post('/searchUser',auth.requiresLogin,users.searchUser);
app.get('/manageArticle/skipReviewer',auth.requiresLogin,users.skipReviewer)
app.get('/writeArticle',auth.requiresLogin, users.writeArticle);
app.get('/userArticle',auth.requiresLogin,users.userArticle);
app.get('/signout',users.signout);
app.post('/users/article/delete',auth.requiresLogin,users.articleDelete);
app.post('/deleteSearchedArticle',auth.requiresLogin,users.deleteSearchedArticle)
app.post('/user/changePassword',users.changePassword);
app.post('/users/updateProfile',auth.requiresLogin,users.updateProfile);
app.post('/users/registration', users.registration);
app.post('/reviewer/articleAccept',auth.requiresLogin,users.reviewerArticleAccept);
app.post('/reviewer/savedArticleAccept',auth.requiresLogin,users.savedArticleAccept);
app.get('/reviewSave',auth.requiresLogin,users.reviewSave);
app.get('/previouslyreviewed',auth.requiresLogin,users.previouslyreviewed);
app.post('/user/updateArticle',auth.requiresLogin,users.updateArticle);
app.post('/users/article/update',auth.requiresLogin,users.articleUpdate);
app.post('/user/article',auth.requiresLogin,users.article);  // article with
app.post('/users/acceptArtistImage',auth.requiresLogin,users.acceptArtistImage); // accept artist image
app.post('/users/rejectArtistImage',auth.requiresLogin,users.rejectArtistImage); // reject
app.post('/users/profilePicture',auth.requiresLogin,users.profilePicture); // for profile picture upload
app.post('/users/articleImage',auth.requiresLogin,users.articleImage);
app.post('/users/newArtistImage',auth.requiresLogin,users.newArtistImage);
app.post('/admin/Request_to_bid',auth.requiresLogin,users.Request_to_bid);
app.get('/viewBids',auth.requiresLogin,users.viewBids);
app.get('/viewArtistArtWork',auth.requiresLogin,users.viewArtistArtWork)
app.get('/bid_article',auth.requiresLogin,users.bid_article);
app.get('/viewBidding_Article',auth.requiresLogin,users.viewBidding_Article);
app.post('/artist/newImage',auth.requiresLogin,users.newImage);
app.post('/manageMember/addtype',auth.requiresLogin,users.addtype);
app.post('/admin/invite',auth.requiresLogin,users.invite);
app.post('/admin/OurUser',auth.requiresLogin,users.OurUser);
app.post('/acceptArtistArticle',auth.requiresLogin,users.acceptArtistArticle);
app.post('/submitArtistImage',auth.requiresLogin,users.submitArtistImage);
app.post('/selectionSubmit',auth.requiresLogin,users.selectionSubmit);
app.get('/admin/skipArtist',auth.requiresLogin,users.skipArtist)
app.get('/showartistarticle',auth.requiresLogin,users.showartistarticle);// artist article show in author
 // for add type in author
app.get('/manageMembers',auth.requiresLogin,users.manageMembers);
app.get('/selectArtWork',auth.requiresLogin,users.selectArtWork)
app.post('/searchArticleByArtist',auth.requiresLogin,users.searchArticleByArtist);
app.post('/selectionArtWorkSubmit',auth.requiresLogin,users.selectionArtWorkSubmit);
app.get('/biddingArticle',auth.requiresLogin,users.biddingArticle);
app.get('/viewSingleBiddingArticle',auth.requiresLogin,users.viewSingleBiddingArticle);
app.get('/acceptBidding',auth.requiresLogin,users.acceptBidding);
app.get('/acceptedArticleForBid',auth.requiresLogin,users.acceptedArticleForBid);
app.get('/artistArticleForbid',auth.requiresLogin,users.artistArticleForbid);
app.get('/editcontents',auth.requiresLogin,users.editcontents);
app.post('/editcontentsubmit',auth.requiresLogin,users.editcontentsubmit);
app.get('/author/previousarticle',auth.requiresLogin,users.previousarticle);
app.get('/showreviedArticle',auth.requiresLogin,users.showreviedArticle);
app.get('/review',auth.requiresLogin,users.review);
app.post('/bidding',auth.requiresLogin,users.bidding);
app.post('/submitDraft',auth.requiresLogin,users.submitDraft);
app.get('/author/editarticle',auth.requiresLogin,users.editarticle);
app.post('/article/rating',auth.requiresLogin, users.articlerating)
app.get('/article',users.singlearticle);  // single article
app.get('/latestarticle',users.latestarticle);
app.get('/reviewcomplete',auth.requiresLogin,users.reviewcomplete);  /// not work yet
app.get('/completereview',auth.requiresLogin,users.completereview); 
app.get('/submitted',auth.requiresLogin,users.submitted);
app.get('/deleteComment',auth.requiresLogin,users.deleteComment);
app.get('/acceptArticle',auth.requiresLogin,users.acceptArticle);
app.get('/ViewAuthorSelection',auth.requiresLogin,users.ViewAuthorSelection);
app.get('/defineAsComplete',auth.requiresLogin,users.defineAsComplete);
//app.post('/tagsearch',users.tagsearch); // tag search
app.post('/editLabelCategory',auth.requiresLogin,users.editLabelCategory)
app.get('/athorapproval',auth.requiresLogin,users.athorapproval);
app.get('/requestartist',auth.requiresLogin,users.requestartist);
app.get('/manageArticle',auth.requiresLogin,auth.requiresLogin,users.manageArticles);
app.get('/managewebsite',auth.requiresLogin,users.managewebsite);
app.get('/viewdonation',auth.requiresLogin,users.viewdonation);
app.get('/viewdistribution',auth.requiresLogin,users.viewdistribution);
app.get('/writeNewArticle',auth.requiresLogin,users.writeNewArticle); // write article page
app.get('/assignartist',auth.requiresLogin,users.assignartist);
app.get('/assignreviewer',auth.requiresLogin,users.assignreviewer);
app.get('/amaderkotha',users.amaderkotha);
app.get('/donation',users.donation);
app.get('/donors',users.donors);
app.post('/donateAmount',users.donateAmount);

app.get('/authors',users.showAuhtor);
app.get('/reviewer',users.reviewer);
app.post('/areYouArtist',auth.requiresLogin,users.areYouArtist);
app.post('/congrtzArtist',auth.requiresLogin,users.congrtzArtist);
app.post('/congrtzAuthor',auth.requiresLogin,users.congrtzAuthor);
app.post('/suggestion',auth.requiresLogin,users.suggestion);
app.get('/managecost',auth.requiresLogin,users.managecost);
app.post('/adminmanagecost',auth.requiresLogin,users.adminManagecost);
app.post('/adminpassword',auth.requiresLogin,users.adminpassword);
app.get('/guideline',auth.requiresLogin,users.guideline);
app.post('/contact',auth.requiresLogin,users.contact);
app.get('/previous',auth.requiresLogin,users.previousArticleshow);
app.get('/allauthors',auth.requiresLogin,users.allauthors);
app.get('/addSound',auth.requiresLogin,users.addSound);
app.get('/uploadFiles',auth.requiresLogin,users.uploadFiles)
app.get('/reviewerpanel',auth.requiresLogin,users.reviewerpanel);
app.post('/addsoundSubmit',auth.requiresLogin,users.addsoundSubmit);
app.post('/editsubmit',auth.requiresLogin,users.editsubmit);
app.post('/admin/managewebsite-amaderkotha',auth.requiresLogin,users.manageamaderkotha);
app.post('/admin/managewebsite-writeArticle',auth.requiresLogin,users.manageWriteArticle);
app.post('/admin/managewebsite-donation',auth.requiresLogin,users.manageDonation);
app.post('/admin/managewebsite-guideAuthor',auth.requiresLogin,users.manageguideAuthor);
app.post('/manageArticle/selectReviewer',auth.requiresLogin,users.selectReviewer);
app.post('/manageArticle/accept',auth.requiresLogin,users.adminArticleAccept);
app.get('/draft',auth.requiresLogin,users.draft);
app.get('/decide2review',auth.requiresLogin,users.decide2review);
app.post('/reviewarticlestatuschange',auth.requiresLogin,users.reviewarticlestatuschange);
app.post('/admin/assignArtist',auth.requiresLogin,users.assign_Artist);
app.get('/currentlyworkingartist',auth.requiresLogin,users.currentlyworkingartist);
app.get('/waiting_for_acceptence',auth.requiresLogin,users.waiting_for_acceptence);
app.get('/open_for_bid',auth.requiresLogin,users.open_for_bid);
app.get('/open_for_rebid',auth.requiresLogin,users.open_for_rebid);
app.post('/manageArticle/viewReview',auth.requiresLogin,users.viewReview);
app.post('/manageArticle/ackAuthour',auth.requiresLogin,users.ackAuthour);
app.post('/manageArticle/saveCredit',auth.requiresLogin,users.saveCredit);
app.get('/reviewerDetails',auth.requiresLogin,users.reviewerDetails);
app.get('/previouslyworkedartist',auth.requiresLogin,users.previouslyworkedartist);
app.post('/admin/requestAuthor',auth.requiresLogin,users.requestAuthor);
app.get('/requestAuthorsApproval',auth.requiresLogin,users.requestAuthorsApproval);
app.post('/thanksEmail',auth.requiresLogin,users.thanksEmail);
app.post('/remindArtist',auth.requiresLogin,users.remindArtist);
app.post('/thank_Bid',auth.requiresLogin,users.thank_Bid);
app.get('/requestForBid',auth.requiresLogin,users.requestForBid);
app.post('/thanks_submittedArticle',auth.requiresLogin,users.thanks_submittedArticle);
app.post('/email_rejectedArticle',auth.requiresLogin,users.email_rejectedArticle);
app.post('/remindReviewer',auth.requiresLogin,users.remindReviewer);
app.post('/thanksReviewer',auth.requiresLogin,users.thanksReviewer);
app.post('/remindArtistArtWork',auth.requiresLogin,users.remindArtistArtWork);
app.post('/requestArtistArtWork',auth.requiresLogin,users.requestArtistArtWork);

app.get('/viewArtWork',auth.requiresLogin,users.viewArtWork);
app.get('/myWallet',auth.requiresLogin,users.myWallet);
app.get('/reviewsinglearticle',auth.requiresLogin,users.reviewsinglearticle);
app.get('/viewPreReviwersinglearticle',auth.requiresLogin,users.viewPreReviwersinglearticle);
app.get('/viewReviwersinglearticle',auth.requiresLogin,users.viewReviwersinglearticle);
app.post('/manageArticle/reject',auth.requiresLogin,users.adminArticleReject);
app.post('/admin/managewebsite',auth.requiresLogin,users.adminManageWebsite);
app.get('/authorapprovalarticle',auth.requiresLogin,users.authorapprovalarticle);
app.get('/viewarticle',auth.requiresLogin,users.viewarticle);
app.get('/viewChangedarticle',auth.requiresLogin,users.viewChangedarticle);
app.get('/recoverPassword/:userId',users.recoverPassword);
app.post('/users/resetpassword',users.resetpassword);
app.get('/activation/:user_id',users.activation);
app.get('/changeUserPassword',users.changeUserPassword);
app.post('/users/session', passport.authenticate('local', {failureRedirect: '/signin#loginError', failureFlash: 'Invalid email or password.'}), users.home);
app.post('/removeUserType',auth.requiresLogin,users.removeUserType);
app.post('/commentSubmit',auth.requiresLogin,users.commentSubmit);
app.post('/adminproductionDetails',auth.requiresLogin,users.productionDetails);
app.post('/admindistributionDetails',auth.requiresLogin,users.admindistributionDetails);

app.get('/manageArticle/uploadPdf',auth.requiresLogin,users.uploadPdf);
app.get('/manageArticle/uploadppt',auth.requiresLogin,users.uploadppt);
app.get('/manageArticle/uploadletter',auth.requiresLogin,users.uploadletter);
app.post('/uploadPdf',auth.requiresLogin,users.uploadPdfSubmit);
app.post('/uploadppt',auth.requiresLogin,users.uploadpptSubmit);
app.post('/uploadletter',auth.requiresLogin,users.uploadletterSubmit);
app.get('/users/updateUserProfile',auth.requiresLogin,users.updateUserProfile);
app.get('/users/viewProfile',auth.requiresLogin,users.viewProfile)
app.get('/viewSearchuserProfile',auth.requiresLogin,users.viewSearchuserProfile);
app.get('/users/facebookSignup',users.facebookSignup);
app.get('/auth/facebook', passport.authenticate('facebook', { scope: [ 'email', 'user_about_me', 'read_stream', 'publish_actions'], failureRedirect: '/signup#signupError' }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/signup#signupError' }), users.authCallback);
app.get('/', index.render);



}
