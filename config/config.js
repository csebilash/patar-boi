
var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')

module.exports = {
  development: {
    db: 'mongodb://localhost/patar-boi',
    root: rootPath,
    app: {
      name: 'patar boi'
    },
    facebook: {
      clientID: "366830853419313",
      clientSecret: "215cb3e51aed7e9b64a81231a69763e4",
      callbackURL: "http://localhost/auth/facebook/callback"
    }
  
  },


  production: {
    db: 'mongodb://localhost/patar-boi',
    root: rootPath,
    app: {
      name: 'patar boi'
    },
    facebook: {
      clientID: "290578804422991",
      clientSecret: "6dbc3aca2824a29d1bd1f38caf5cd9be",
      callbackURL: "http://162.243.133.214/auth/facebook/callback"
    }
  
  }
}
